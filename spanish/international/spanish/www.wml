#use wml::debian::template title="Traducciones del servidor web"


<p>El servidor web es parte de la imagen pública de Debian. Como tal, es
importante que los visitantes puedan tener acceso en su idioma
natal. <a href="#participar">Participar</a> en su traducción es sencillo, no es
necesario realizar tanto trabajo como con un documento del proyecto de
documentación, y estarás ayudando <em>mucho</em> al proyecto
Debian. Las páginas del servidor web de Debian crecen y se actualizan
constantemente, siendo necesaria la ayuda de muchos voluntarios para
poder mantener una versión en español.</p>

<p> Puedes consultar, si lo deseas, las <a
href="https://www.debian.org/devel/website/stats/">estadísticas
globales</a> y el <a
href="https://www.debian.org/devel/website/stats/es">estado actual
de la traducción al español</a>.</p>

<p>Para poder seguir el ritmo del documento original y determinar en
qué momento un documento traducido tiene que ser actualizado, se
utiliza un esquema en base al código «hash» de cada revisión. Las
traducciones de las páginas web deben incluir lo siguiente: <tt>#use
wml::debian::translation-check translation="xyz123abc"</tt> donde xyz123abc es el
«hash» de la revisión del fichero original (se puede obtener ejecutando
<tt>git log -n 1 fichero</tt> ¡asegúrate de mirar la versión en inglés!).</p>

<p>De esta forma las herramientas automáticas realizadas pueden:</p>

<ul>

<li>Avisar automáticamente al traductor (por correo) cuando un fichero se haya
actualizado en su versión original.</li>

<li>Avisar al lector de que la traducción está desactualizada en la propia página, para que acuda a la fuente original si lo necesita.</li>

</ul> 

<p>Para más información consulte <a
href="$(HOME)/devel/website/uptodate">las páginas de actualización del
servidor de la web</a>.</p>

<h2><a name="participar">¿Cómo participar?</a></h2>

<p>Si quieres participar sigue los siguientes pasos:
</p>

<ol>

<li>Subscríbete, si no lo has hecho ya, a
<a href="mailto:debian-l10n-spanish@lists.debian.org">debian-l10n-spanish</a></li>

<li>Avisa a la lista de tu intención de participar en el
proyecto de traducción de la web. Si quieres encargate de una sección específica,
indícalo también expresamente.</li>


<li>Lee las <a href="notas">normas y notas de traducción</a>, y
familiarízate con las herramientas con las que se construye el sitio web. Están
descritas en las <a href="$(HOME)/devel/website/">páginas de desarrollo
del sitio web</a>. En el caso de querer dedicarte de forma habitual
a la traducción es de gran utilidad (y hasta casi imprescindible) conocer y
manejar las tecnologías y herramientas relacionadas con Git (sistema de control
de versiones) y WML (metalenguaje que se utiliza para la construcción del
sitio web).</li>

<li>Mira en <a href="$(HOME)/devel/website/stats/es">estadísticas</a>
las páginas que necesitan ser traducidas o actualizadas
(hay enlaces directos para descargarlas e indicaciones de las órdenes git a utilizar para ver las diferencias en
caso de actualización).</li>

<li>Manda un mensaje a <a
href="mailto:debian-l10n-spanish@lists.debian.org">debian-l10n-spanish</a>
para decir que te vas a encargar de un determinado documento (para no
solapar esfuerzo y evitar que dos personas trabajen en el mismo).
Este mensaje debe seguir el formato utilizado por el <a
href="robot">robot de gestión de traducciones</a>.</li>

<li>En caso de que vayas a traducir una página no traducida,
se recomienda utilizar
la herramienta <tt>copypage.pl</tt> para generar la primera versión de la página
a traducir (la herramienta también detecta si hay versiones antiguas disponibles).</li>

<li>Traduce el documento.</li>

<li>Envíalo a la lista de correo para su revisión.</li>

<li>Si no dispones de acceso directo al sistema de control de versiones, 
una vez revisado por la lista solicita al coordinador de la traducción de la
web que lo incluya en el servidor (si no lo ha incluido aún).</li>

</ol>

<p>No existe una prioridad definida para traducir nuevas páginas. Cada persona
que trabaja en la traducción se encarga de una parte, bien porque le gusta,
porque la conoce más o por cualquier otra razón personal.</p>

<p>Sin embargo, sí que es importante, antes de ponerse a traducir nuevos
contenidos, ayudar a que las <strong>ya</strong> traducidas se mantengan
actualizadas. Existen mecanismos para revisar el nivel de
desactualización.</p>

<p>Si estás dispuesto a traducir los ficheros del servidor de una
forma más asidua, consulta la información <a
href="$(HOME)/devel/website/">destinada a desarrolladores del servidor
web</a> para ver qué necesitas hacer.  Deberás, entre otras cosas,
crear una cuenta en <a href="https://salsa.debian.org">Salsa</a>
(el sistema de control de versiones de Debian), descargar los
ficheros originales y el estado de la traducción actual y enviar tus
cambios a través de Git. En cualquier caso, habla con el coordinador y
con los demás traductores dentro de la lista de distribución.</p>

<p>Las traducciones  se hacen sobre ficheros WML (<em>web site
metalanguage</em>). Recuerda, no debes trabajar sobre ficheros HTML. Los puedes
descargar directamente, también, desde la página de 
<a href="$(HOME)/devel/website/stats/es">estadísticas de la traducción</a>,
así como ver las diferencias en caso de actualización.  Hay también una
página disponible con el <a href="$(HOME)/devel/website/stats/">ranking</a> de
los distintos idiomas con intención de que sirva como una forma de competencia
«sana».</p>

<h2><a name="colaboradores">Colaboradores</a></h2>

<p>La persona que coordina actualmente el esfuerzo de traducción de la web
es <a href="https://wiki.debian.org/LauraArjona">Laura Arjona Reina</a>.</p>

<p>Colaboran de forma continuada en la traducción de estos contenidos, entre otras personas:</p>

<ul>
<li>Laura Arjona Reina</li>
<li>Javier Amor</li>
<li>Alfredo Quintero</li>
</ul>

<p>Han colaborado en el servidor en el pasado, entre otras, las siguientes personas:</p>

<ul>
<li>Javier Fernández-Sanguino: traducción de numerosas secciones y páginas individuales de la web y coordinador previo.</li>
<li>Diddier Hilarion.</li>
<li>Ignacio García Fernández: responsable de la traducción de <tt>News/weekly</tt> (las <em>Noticias del Proyecto Debian</em>).</li>
<li>Ricardo Javier Cárdenes Medina.</li>
<li>Fernando Cerezal.</li>
<li>David Moreno Garza.</li>
<li>David Martínez Moreno.</li>
<li>Juan Manuel García Molina: traducción de <tt>security/</tt>.</li>
<li>Ugo Enrico Albarello &lt;ugo_albarello_AT_correo.javeriana.edu.co&gt;: traducción de <tt>/news</tt> e <tt>/intro</tt>.</li>
<li>Jesús M. González-Barahona &lt;jgb_AT_debian.org&gt;: traducción de /News/weekly durante mucho tiempo y coordinador previo.</li>
<li>Gustavo_AT_edei.es: traducción de <tt>logos/</tt>, <tt>doc/</tt> y <tt>2.0/</tt>.</li>
<li>Jose Marin &lt;jose_AT_ma.hw.ac.uk&gt;: traducción de <tt>/distrib</tt> y <tt>MailingLists/</tt>.</li>
<li>Saioa Perurena &lt;saioa_AT_jet.es&gt;: traducción de <tt>/security</tt>.</li>
<li>Aurelio aurelio_AT_bbvnet.com&gt;: traducción de <tt>/security</tt>.</li>
<li>Jose Rodríguez &lt;boriel_AT_arrakis.es&gt;: coordinación de la traducción de <tt>/ports</tt>.</li>
<li>Lucas Gonzalez Santa Cruz &lt;lucas_AT_dgsp.scstf.rcanaria.es&gt;: traducción de <tt>/vote</tt>.</li>
<li>Pedro Graci Fajardo &lt;pedro_AT_redkbs.com&gt;: traducción de HOWTO_work_on_website (28 diciembre 1998).</li>
<li>thot_AT_bbvnet.com: encargada traducción de <tt>/consultants</tt> (27 febrero 1999).</li>
<li>Juan Gascón &lt;jgascon_AT_mundivia.es&gt;: encargada traducción de <tt>/security/1999</tt> (enviado 1 mayo 1999).</li>
<li>Sergio Rael Gutierrez &lt;sergio_AT_dante.dhis.org&gt;: encargada traducción de <tt>/devel/constitution</tt> (1 mayo 1999).</li>
<li>Alberto Serrano &lt;apserrano_AT_bbvnet.com&gt;: traducción de <tt>/News/weekly</tt> (hasta 31 marzo 1999).</li>
<li>Hector García &lt;hector_AT_scout-es.org&gt;: traducción de <tt>/news/weekly</tt> (también).</li>
</ul>
