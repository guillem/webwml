#use wml::debian::translation-check translation="4c0e547d89fc6a0f2307d782bbce0a6ac7d74bc2"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.4</define-tag>
<define-tag release_date>2022-07-09</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la cuarta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apache2 "Nueva versión «estable» del proyecto original; corrige problema de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2022-26377], problemas de lectura fuera de límites [CVE-2022-28330 CVE-2022-28614 CVE-2022-28615], problemas de denegación de servicio [CVE-2022-29404 CVE-2022-30522], posible problema de lectura fuera de límites [CVE-2022-30556] y posible problema de elusión de autenticación por IP [CVE-2022-31813]">
<correction base-files "Actualiza /etc/debian_version para la versión 11.4">
<correction bash "Corrige desbordamiento de memoria de un byte en lectura, provocando caracteres multibyte corruptos en sustituciones de órdenes">
<correction clamav "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction clementine "Añade dependencia con libqt5sql5-sqlite, que faltaba">
<correction composer "Corrige problema de inyección de código [CVE-2022-24828]; actualiza patrón de tokens de GitHub">
<correction cyrus-imapd "Asegura que todos los buzones tienen un campo <q>uniqueid</q>, corrigiendo las actualizaciones a la versión 3.6">
<correction dbus-broker "Corrige problema de desbordamiento de memoria [CVE-2022-31212]">
<correction debian-edu-config "Acepta correo procedente de la red local enviado a root@&lt;mynetwork-names&gt;; crea principales Kerberos de máquina («host») y de servicio solo si no existen todavía; asegura que libsss-sudo está instalado en las estaciones de trabajo itinerantes («Roaming Workstations»); corrige nomenclatura y visibilidad de colas de impresión; soporta krb5i en estaciones de trabajo sin disco («Diskless Workstations»); squid: prefiere búsquedas DNSv4 a las DNSv6">
<correction debian-installer "Recompilado contra proposed-updates; incrementa la ABI del núcleo Linux a la 16; restaura algunos dispositivos («targets») armel para netboot (openrd)">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates; incrementa la ABI del núcleo Linux a la 16; restaura algunos dispositivos («targets») armel para netboot (openrd)">
<correction distro-info-data "Añade Ubuntu 22.10, Kinetic Kudu">
<correction docker.io "Configura docker.service para que arranque después de containerd.service y así corregir la parada («shutdown») de contenedores; pasa explícitamente la ruta del socket de containerd a dockerd para garantizar que este no arranque containerd">
<correction dpkg "dpkg-deb: corrige condiciones de fin de fichero inesperadas al extraer ficheros .deb; libdpkg: no restringe los campos virtuales source:* a paquetes instalados; Dpkg::Source::Package::V2: corrige siempre los permisos de los ficheros distribuidos («tarballs») por el proyecto original (regresión introducida por DSA-5147-1)">
<correction freetype "Corrige problema de desbordamiento de memoria [CVE-2022-27404]; corrige caídas [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Corrige problemas de desbordamiento de memoria [CVE-2022-25308 CVE-2022-25309]; corrige caída [CVE-2022-25310]">
<correction ganeti "Nueva versión del proyecto original; corrige varios problemas con actualizaciones; corrige migración en vivo con QEMU 4 y con <q>security_model</q> <q>user</q> o <q>pool</q>">
<correction geeqie "Corrige Ctrl click en el interior de una selección de bloque">
<correction gnutls28 "Corrige cálculo erróneo de SHA384 en SSSE3; corrige problema de desreferencia de puntero nulo [CVE-2021-4209]">
<correction golang-github-russellhaering-goxmldsig "Corrige desreferencia de puntero nulo provocada por firmas XML manipuladas [CVE-2020-7711]">
<correction grunt "Corrige problema de escalado de directorios [CVE-2022-0436]">
<correction hdmi2usb-mode-switch "udev: añade un sufijo a ficheros de dispositivo /dev/video para desambiguarlos; mueve reglas de udev a la prioridad 70, para que se apliquen después de las de 60-persistent-v4l.rules">
<correction hexchat "Añade dependencia con python3-cffi-backend, que faltaba">
<correction htmldoc "Corrige bucle infinito [CVE-2022-24191], problemas de desbordamiento de entero [CVE-2022-27114] y problema de desbordamiento de memoria dinámica («heap») [CVE-2022-28085]">
<correction knot-resolver "Corrige posible fallo de aserción en caso extremo de NSEC3 [CVE-2021-40083]">
<correction libapache2-mod-auth-openidc "Nueva versión «estable» del proyecto original; corrige problema de redirección abierta [CVE-2021-39191]; corrige caída durante reload / restart">
<correction libintl-perl "Instala efectivamente gettext_xs.pm">
<correction libsdl2 "Evita lectura fuera de límites al cargar fichero BMP mal construido [CVE-2021-33657] y durante la conversión de YUV a RGB">
<correction libtgowt "Nueva versión «estable» del proyecto original, para soportar versiones más recientes de telegram-desktop">
<correction linux "Nueva versión «estable» del proyecto original; incrementa la ABI a la 16">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 16">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 16">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 16">
<correction logrotate "Omite el bloqueo si el fichero de estado tiene permisos universales de lectura [CVE-2022-1348]; análisis sintáctico de la configuración más estricto para evitar el análisis sintáctico de ficheros ajenos, como volcados de memoria">
<correction lxc "Actualiza el servidor de claves GPG por omisión, corrigiendo la creación de contenedores cuando se utiliza la plantilla <q>download</q>">
<correction minidlna "Valida peticiones HTTP para proteger frente a ataques de revinculación de DNS [CVE-2022-26505]">
<correction mutt "Corrige problema de desbordamiento de memoria en uudecode [CVE-2022-1328]">
<correction nano "Varias correcciones de fallos, incluyendo algunas para caídas">
<correction needrestart "Hace que la detección de cgroups para servicios y sesiones de usuario tenga en cuenta la v2 de cgroup">
<correction network-manager "Nueva versión «estable» del proyecto original">
<correction nginx "Corrige caída cuando libnginx-mod-http-lua está cargado y se usa un init_worker_by_lua*; mitiga ataque de confusión de contenido de protocolo en la capa de aplicación en el módulo Mail [CVE-2021-3618]">
<correction node-ejs "Corrige problema de inyección de plantillas en el lado servidor [CVE-2022-29078]">
<correction node-eventsource "Elimina cabeceras sensibles en redirecciones a sitios con diferente origen [CVE-2022-1650]">
<correction node-got "No permite redirecciones a sockets Unix [CVE-2022-33987]">
<correction node-mermaid "Corrige problemas de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-23648 CVE-2021-43861]">
<correction node-minimist "Corrige problema de contaminación de prototipo [CVE-2021-44906]">
<correction node-moment "Corrige problema de escalado de directorios [CVE-2022-24785]">
<correction node-node-forge "Corrige problemas de verificación de firmas [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-raw-body "Corrige potencial problema de denegación de servicio en node-express, mediante el uso de node-iconv-lite en lugar de node-iconv">
<correction node-sqlite3 "Corrige problema de denegación de servicio [CVE-2022-21227]">
<correction node-url-parse "Corrige problemas de elusión de autenticación [CVE-2022-0686 CVE-2022-0691]">
<correction nvidia-cuda-toolkit "Usa snapshots de OpenJDK8 para amd64 y para ppc64el; comprueba que el binario java sea utilizable; nsight-compute: mueve la carpeta 'sections' a una ubicación multiarquitectura; corrige la ordenación de las versiones de nvidia-openjdk-8-jre">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original; cambia al árbol 470 del proyecto original; corrige problemas de denegación de servicio [CVE-2022-21813 CVE-2022-21814]; corrige problema de escritura fuera de límites [CVE-2022-28181], problema de lectura fuera de límites [CVE-2022-28183] y problemas de denegación de servicio [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión del proyecto original; corrige problemas de escritura fuera de límites [CVE-2022-28181 CVE-2022-28185]">
<correction nvidia-graphics-drivers-tesla-418 "Nueva versión «estable» del proyecto original">
<correction nvidia-graphics-drivers-tesla-450 "Nueva versión «estable» del proyecto original; corrige problemas de escritura fuera de límites [CVE-2022-28181 CVE-2022-28185] y problema de denegación de servicio [CVE-2022-28192]">
<correction nvidia-graphics-drivers-tesla-460 "Nueva versión «estable» del proyecto original">
<correction nvidia-graphics-drivers-tesla-470 "Nuevo paquete, cambiando el soporte de Tesla al árbol 470 del proyecto original; corrige problema de escritura fuera de límites [CVE-2022-28181], problema de lectura fuera de límites [CVE-2022-28183] y problemas de denegación de servicio [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-persistenced "Nueva versión del proyecto original; cambia al árbol 470 del proyecto original">
<correction nvidia-settings "Nueva versión del proyecto original; cambia al árbol 470 del proyecto original">
<correction nvidia-settings-tesla-470 "Nuevo paquete, cambiando el soporte de Tesla al árbol 470 del proyecto original">
<correction nvidia-xconfig "Nueva versión del proyecto original">
<correction openssh "seccomp: añade llamada al sistema pselect6_time64 en arquitecturas de 32 bits">
<correction orca "Corrige uso con webkitgtk 2.36">
<correction php-guzzlehttp-psr7 "Corrige análisis sintáctico de cabeceras incorrecto [CVE-2022-24775]">
<correction phpmyadmin "Corrige algunas consultas SQL que provocan un error de servidor">
<correction postfix "Nueva versión «estable» del proyecto original; respeta en postinst el valor de default_transport establecido por el usuario; if-up.d: no emite error si postfix no puede enviar correo todavía">
<correction procmail "Corrige desreferencia de puntero nulo">
<correction python-scrapy "No envía datos de autenticación con todas las solicitudes [CVE-2021-41125]; no expone cookies de dominio cruzado al redireccionar [CVE-2022-0577]">
<correction ruby-net-ssh "Corrige autenticación contra sistemas que usan OpenSSH 8.8">
<correction runc "Respeta defaultErrnoRet de seccomp; no establece capacidades heredables [CVE-2022-29162]">
<correction samba "Corrige fallo en el arranque de winbind cuando se usa <q>allow trusted domains = no</q>; corrige autenticación de MIT Kerberos; corrige problema de escape de recurso compartido por condición de carrera en mkdir [CVE-2021-43566]; corrige posible problema de corrupción grave de datos debida a envenenamiento de caché en clientes Windows; corrige instalación en sistemas no systemd">
<correction tcpdump "Actualiza perfil de AppArmor para permitir acceso a ficheros *.cap y gestiona sufijos numéricos en nombres de fichero añadidos con -W">
<correction telegram-desktop "Nueva versión «estable» del proyecto original, que vuelve a hacerlo funcional">
<correction tigervnc "Corrige arranque del escritorio GNOME cuando se utiliza tigervncserver@.service; corrige visualización del color cuando vncviewer y el servidor X11 usan ordenaciones de bytes («endianness») distintas">
<correction twisted "Corrige problema de revelación de información con redireccionamientos entre dominios («cross-domain») [CVE-2022-21712], problema de denegación de servicio durante los «handshakes» de SSH [CVE-2022-21716] y problemas de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2022-24801]">
<correction tzdata "Actualiza datos de zona horaria para Palestina; actualiza lista de segundos intercalares">
<correction ublock-origin "Nueva versión «estable» del proyecto original">
<correction unrar-nonfree "Corrige problema de escalado de directorios [CVE-2022-30333]">
<correction usb.ids "Nueva versión del proyecto original; actualiza los datos incluidos">
<correction wireless-regdb "Nueva versión del proyecto original; borra desvío («diversion») añadido por el instalador, asegurando que se utilizan los ficheros distribuidos con el paquete">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4999 asterisk>
<dsa 2021 5026 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5107 php-twig>
<dsa 2022 5108 tiff>
<dsa 2022 5110 chromium>
<dsa 2022 5111 zlib>
<dsa 2022 5112 chromium>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5114 chromium>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5116 wpewebkit>
<dsa 2022 5117 xen>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5120 chromium>
<dsa 2022 5121 chromium>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5124 ffmpeg>
<dsa 2022 5125 chromium>
<dsa 2022 5127 linux-signed-amd64>
<dsa 2022 5127 linux-signed-arm64>
<dsa 2022 5127 linux-signed-i386>
<dsa 2022 5127 linux>
<dsa 2022 5128 openjdk-17>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5130 dpdk>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5133 qemu>
<dsa 2022 5134 chromium>
<dsa 2022 5136 postgresql-13>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5148 chromium>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5155 wpewebkit>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5161 linux-signed-amd64>
<dsa 2022 5161 linux-signed-arm64>
<dsa 2022 5161 linux-signed-i386>
<dsa 2022 5161 linux>
<dsa 2022 5162 containerd>
<dsa 2022 5163 chromium>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5166 slurm-wlm>
<dsa 2022 5167 firejail>
<dsa 2022 5168 chromium>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5174 gnupg2>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction elog "Sin desarrollo activo; problemas de seguridad">
<correction python-hbmqtt "Sin desarrollo activo y roto">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
