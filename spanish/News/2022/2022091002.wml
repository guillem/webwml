#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.5</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la quinta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction avahi "Corrige visualización de las URL que contienen '&amp;' en avahi-discover; no inhabilita la limpieza de timeouts al limpiar relojes («watch»); corrige caídas por punteros NULL al intentar resolver nombres de máquina con formato erróneo [CVE-2021-3502]">
<correction base-files "Actualiza /etc/debian_version para la versión 11.5">
<correction cargo-mozilla "Nuevo paquete fuente para soportar la compilación de versiones más recientes de firefox-esr y de thunderbird">
<correction clamav "Nueva versión «estable» del proyecto original">
<correction commons-daemon "Corrige la detección de la JVM">
<correction curl "Rechaza cookies con <q>control bytes</q> («bytes de control») [CVE-2022-35252]">
<correction dbus-broker "Corrige fallo de aserción al desconectar grupos de pares; corrige fuga de memoria; corrige desreferencia de puntero nulo [CVE-2022-31213]">
<correction debian-installer "Recompilado contra proposed-updates; incrementa la ABI del núcleo Linux a la 5.10.0-18">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates; incrementa la ABI del núcleo Linux a la 5.10.0-18">
<correction debian-security-support "Actualiza el estado de soporte de varios paquetes">
<correction debootstrap "Asegura que se pueden seguir creando chroots con /usr no fusionado para versiones anteriores y para chroots de buildd">
<correction dlt-daemon "Corrige problema de «doble liberación» [CVE-2022-31291]">
<correction dnsproxy "Escucha por omisión en localhost, en lugar de escuchar en la posiblemente no disponible dirección 192.168.168.1">
<correction dovecot "Corrige posibles problemas de seguridad cuando existen dos entradas de configuración de passdb con los mismos valores de controlador («driver») y parámetros («args») [CVE-2022-30550]">
<correction dpkg "Corrige tratamiento de removal-on-upgrade («borrar al actualizar») fichero de configuración y fuga de memoria en el tratamiento de remove-on-upgrade; Dpkg::Shlibs::Objdump: corrige apply_relocations para que funcione con símbolos con número de versión; añade soporte para las CPU ARCv2; varias actualizaciones y correcciones de dpkg-fsys-usrunmess">
<correction fig2dev "Corrige problema de «doble liberación» [CVE-2021-37529] y problema de denegación de servicio [CVE-2021-37530]; corrige posicionamiento de imágenes eps embebidas">
<correction foxtrotgps "Corrige caída asegurando que las referencias a los hilos son eliminadas siempre">
<correction gif2apng "Corrige desbordamientos de memoria dinámica («heap») [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Corrige un desbordamiento de memoria por uno («off-by-one») en getcwd() [CVE-2021-3999]; corrige varios desbordamientos en funciones de caracteres anchos; añade algunas funciones de cadenas de caracteres EVEX optimizadas para corregir un problema de rendimiento (hasta del 40%) con procesadores Skylake-X; hace grantpt utilizable después de una bifurcación («fork») multihilo; asegura que está habilitada la protección de las vtables de libio">
<correction golang-github-pkg-term "Corrige compilación con núcleos Linux más recientes">
<correction gri "Usa <q>ps2pdf</q> en lugar de <q>convert</q> para convertir de PS a PDF">
<correction grub-efi-amd64-signed "Nueva versión del proyecto original">
<correction grub-efi-arm64-signed "Nueva versión del proyecto original">
<correction grub-efi-ia32-signed "Nueva versión del proyecto original">
<correction grub2 "Nueva versión del proyecto original">
<correction http-parser "Desactiva F_CHUNKED en nuevos campos de cabecera Transfer-Encoding, corrigiendo posible problema de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2020-8287]">
<correction ifenslave "Corrige configuraciones de interfaces vinculadas">
<correction inetutils "Corrige problema de desbordamiento de memoria [CVE-2019-0053], problema de agotamiento de la pila, tratamiento de respuestas FTP PASV [CVE-2021-40491] y problema de denegación de servicio [CVE-2022-39028]">
<correction knot "Corrige el paso de IXFR a AXFR con dnsmasq">
<correction krb5 "Usa SHA256 como Pkinit CMS Digest">
<correction libayatana-appindicator "Proporciona compatibilidad con software que depende de libappindicator">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libhttp-daemon-perl "Mejora el tratamiento de la cabecera Content-Length [CVE-2022-31081]">
<correction libreoffice "Soporta EUR en la configuración regional («locale») .hr; añade el tipo de cambio HRK&lt;-&gt;EUR a Calc y al asistente Euro («Euro Wizard»); correcciones de seguridad [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307]; corrige cuelgue al acceder a libretas de direcciones de Evolution">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction llvm-toolchain-13 "Nuevo paquete fuente para soportar la compilación de versiones más recientes de firefox-esr y de thunderbird">
<correction lwip "Corrige problemas de desbordamiento de memoria [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "Nueva versión del proyecto original, para permitir gestión de SBAT">
<correction node-log4js "No crea por omisión ficheros con permisos universales de lectura [CVE-2022-21704]">
<correction node-moment "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2022-31129]">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "Nueva versión del proyecto original; corrige compilación cruzada">
<correction nvidia-settings-tesla-470 "Nueva versión del proyecto original; corrige compilación cruzada">
<correction pcre2 "Corrige problemas de lectura fuera de límites [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "No permite que los scripts de extensión reemplacen objetos salvo que estos pertenezcan ya a la extensión [CVE-2022-2625]">
<correction publicsuffix "Actualiza los datos incluidos">
<correction rocksdb "Corrige instrucción ilegal en arm64">
<correction sbuild "Buildd::Mail: soporta cabecera Subject: con formato MIME, copia también la cabecera Content-Type: al reenviar correos">
<correction systemd "Desecha la copia de linux/if_arp.h incluida, corrigiendo fallos de compilación con cabeceras de núcleos más recientes; soporta la detección de huéspedes Hyper-V en ARM64; detecta instancia OpenStack como KVM en arm">
<correction twitter-bootstrap4 "Instala realmente ficheros CSS map">
<correction tzdata "Actualiza datos de zona horaria para Irán y para Chile">
<correction xtables-addons "Soporta tanto versiones antiguas como nuevas de security_skb_classify_flow()">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction evenement "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-cocur-slugify "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-defuse-php-encryption "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-dflydev-fig-cookies "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-embed "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-fabiang-sasl "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-markdown "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-raintpl "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-react-child-process "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-react-http "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-respect-validation "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction php-robmorgan-phinx "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction ratchet-pawl "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction ratchet-rfc6455 "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction ratchetphp "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-cache "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-dns "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-event-loop "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-promise-stream "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-promise-timer "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-socket "Sin desarrollo activo; necesario solo para movim, ya eliminado">
<correction reactphp-stream "Sin desarrollo activo; necesario solo para movim, ya eliminado">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


