# translation of cdimage.hy.po to Armenian
# Armen Dilanyan <armyan861@gmail.com>, 2019.
# Arman High <mrarmanhigh@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-03-25 19:20+0600\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: hy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:12
msgid "      Key fingerprint"
msgstr "      Հիմնական մատնահետք"

#: ../../english/devel/debian-installer/images.data:89
msgid "ISO images"
msgstr "ISO պատկերներ"

#: ../../english/devel/debian-installer/images.data:90
msgid "Jigdo files"
msgstr "Jigdo նիշքեր"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />ՀՏՀ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Ներբեռնել Jigdo-ի միջոցով"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Ներբեռնել HTTP/FTP-ի միջոցով"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Գնել CD-ներ կամ DVD-ներ"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Ցանցային տեղադրում"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id = \"dc_download\" />Ներբեռնել"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id = \"dc_misc\" />Խառնաբնույթ"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id = \"dc_artwork\" />Արվեստի գործեր"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id = \"dc_mirroring\" />Հայելիներ"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync հայելիներ"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id = \"dc_verify\" />Ստուգել"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id = \"dc_torrent\" />Ներբեռնել Torrent-ի միջոցով"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "Debian CD-ի թիմը"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr "Debian-ը CD-ի վրա"

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />ՀՏՀ"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "գնել"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr "Ցանցային տեղադրում"

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />խառնաբնույթ"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"Անգլալեզու <a href=\"/MailingLists/disclaimer\">հրապարակային փոստային ցանկ</"
"a> CD-ների/DVD-ների համար՝"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id = \"dc_relinfo\" />Տեղեկատվություն թողարկման պատկերի մասին"
