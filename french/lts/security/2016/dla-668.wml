#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans libass, une
bibliothèque pour manipuler le format de fichier de sous-titres SubStation
Alpha (SSA). Le projet « Common Vulnerabilities and Exposures » (CVE)
identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7969">CVE-2016-7969</a>

<p>L'égalisation des coupures de ligne en mode 0/3 pourrait avoir pour
conséquence, dans des cas particuliers, des lectures illégales lors de
la présentation et la mise en forme de texte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7972">CVE-2016-7972</a>

<p>Problème de réallocation de mémoire dans la fonction « shaper » qui mène
à un comportement non défini.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.10.0-3+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libass.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-668.data"
# $Id: $
