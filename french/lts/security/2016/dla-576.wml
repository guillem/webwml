#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités d'utilisation de mémoire après libération ont été
découvertes dans DBD::mysql, un pilote Perl DBI pour le serveur de base de
données. Un attaquant distant peut tirer avantage de ces défauts pour
provoquer un déni de service à l'encontre d'une application utilisant
DBD::mysql (plantage de l'application), ou éventuellement pour exécuter du
code arbitraire avec les droits de l'utilisateur exécutant l'application.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.021-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libdbd-mysql-perl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-576.data"
# $Id: $
