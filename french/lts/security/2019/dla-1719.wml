#use wml::debian::translation-check translation="68a214499ef415a6f6eb6218322fb0a6e0760760" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de déni de service dans la bibliothèque d’image
JPEG, libjpeg-turbo, optimisée pour le CPU. Une lecture hors limites de tampon
basé sur le tas pourrait être déclenchée par un fichier bitmap (BMP) contrefait
pour l'occasion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14498">CVE-2018-14498</a>

<p>get_8bit_row dans rdbmp.c dans libjpeg-turbo jusqu’à 1.5.90 et MozJPEG
jusqu’à 3.3.1 permettaient à des attaquants de provoquer un déni de service (une
lecture hors limites de tampon basé sur le tas et plantage d'application)
à l'aide d'un BMP 8 bits contrefait dans lequel un ou plusieurs indices de couleur
sont en dehors des entrées de numéros de palette.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:1.3.1-12+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libjpeg-turbo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1719.data"
# $Id: $
