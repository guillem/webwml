#use wml::debian::translation-check translation="fde938776706efcf28e8464baed268b4223022a0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes mineurs ont été corrigés dans mupdf, un visionneur léger
de PDF adapté à l’affichage de graphismes anticrénelés et de haute qualité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5686">CVE-2018-5686</a>

<p>Dans MuPDF, il existait une vulnérabilité de boucle infinie et de plantage
d’application dans la fonction pdf_parse_array (pdf/pdf-parse.c) à cause d’EOF
n’ayant pas été pris en compte. Des attaquants distants pourraient exploiter
cette vulnérabilité pour provoquer un déni de service à l'aide d'un fichier
PDF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6130">CVE-2019-6130</a>

<p>MuPDF possède un SEGV dans la fonction fz_load_page du fichier
fitz/document.c, comme prouvé par mutool. Cela concernait le mauvais traitement
du numéro de page dans cbz/mucbz.c, cbz/muimg.c et svg/svg-doc.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6192">CVE-2018-6192</a>

<p>Dans MuPDF, la fonction pdf_read_new_xref dans pdf/pdf-xref.c permettait
à des attaquants distants de provoquer un déni de service (violation de
segmentation et plantage d'application) à l'aide d'un fichier PDF contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.5-1+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets mupdf.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1838.data"
# $Id: $
