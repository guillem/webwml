#use wml::debian::translation-check translation="391103ab2e705e2bc9f769600c66c87a4389d634" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>CKEditor, un éditeur de texte HTML WYSIWYG libre avec prise en charge du
<q>rich content</q>, qui peut être intégré dans des pages web, contient les
deux vulnérabilités suivantes :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33829">CVE-2021-33829</a>

<p>Une vulnérabilité de script intersite (XSS) dans le processeur de
données HTML dans CKEditor 4 permet à des attaquants distants d'injecter du
code JavaScript exécutable à l'aide d'un commentaire contrefait parce que
<q>--!></q> est mal géré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37695">CVE-2021-37695</a>

<p>Une possible vulnérabilité a été découverte dans le paquet Fake Objects
de CKEditor 4. La vulnérabilité permettait d'injecter du HTML mal formé de
Fake Objects, qui pourrait avoir pour conséquence l'exécution de code
JavaScript.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.5.7+dfsg-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ckeditor.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ckeditor, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ckeditor">\
https://security-tracker.debian.org/tracker/ckeditor</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2813.data"
# $Id: $
