#use wml::debian::translation-check translation="f1c031f3c7d72bd31a1d99c4b2bb5a3b420c4ad7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8695">CVE-2020-8695</a>

<p>Un écart observable dans l’interface RAPL de certains processeurs d’Intel®
pourrait potentiellement permettre à un utilisateur privilégié de rendre
possible une divulgation d’informations à l’aide d’un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8696">CVE-2020-8696</a>

<p>Une suppression incorrecte d’informations sensibles avant stockage ou
transfert dans certains processeurs d’Intel® peut potentiellement permettre à un
utilisateur authentifié de rendre possible une divulgation d’informations
à l’aide d’un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8698">CVE-2020-8698</a>

<p>Une isolation insuffisante de ressources partagées dans certains processeurs
d’Intel® peut potentiellement permettre à un utilisateur authentifié de rendre
possible une divulgation d’informations à l’aide d’un accès local.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.20201118.1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2546.data"
# $Id: $
