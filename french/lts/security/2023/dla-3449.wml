#use wml::debian::translation-check translation="d675d2c5767201c05295391bab6f3e142739d08e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL, une boîte à outils
SSL (Secure Sockets Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0464">CVE-2023-0464</a>

<p>David Benjamin a signalé un défaut relatif à la vérification de chaines de
certificats X.509 qui incluaient des contraintes de politique, ce qui pouvait
aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0465">CVE-2023-0465</a>

<p>David Benjamin a signalé que des politiques de certificat non autorisé dans
les certificats d'entité finale étaient ignorées silencieusement. Un CA malveillant
pouvait exploiter ce défaut pour délibérément déclarer des politiques de
certificats non valables ou incorrectes afin de contourner complètement la
vérification de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0466">CVE-2023-0466</a>

<p>David Benjamin a découvert que l’implémentation de la fonction
X509_VERIFY_PARAM_add0_policy() n’activait pas la vérification, ce qui permettait
aux certificats avec des politiques non valables ou incorrectes de passer la
vérification de certificat (contrairement à la documentation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2650">CVE-2023-2650</a>

<p>Il a été découvert que le traitement d’identifiants ou de données d’objet
ASN.1 mal formés pouvait aboutir à un déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.1.1n-0+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3449.data"
# $Id: $
