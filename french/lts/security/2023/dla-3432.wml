#use wml::debian::translation-check translation="9727cdf04dc39aaa92d07c1bef798dee4fa016e3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Python, un langage
interactif de haut niveau orienté objet. Un attaquant pouvait provoquer une
injection de commande, un déni de service (DoS), une dissimulation de requête
ou un balayage de ports.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-20107">CVE-2015-20107</a>

<p>Le module mailcap n’ajoutait pas de caractères d’échappement dans les
commandes dans le fichier mailcap du système. Cela pouvait permettre à des
attaquants d’injecter des commandes d’interpréteur dans des applications qui
appelaient mailcap.findmatch avec des entrées non fiables (manque de validation
de noms de fichier ou d’arguments fournis par l’utilisateur.).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20907">CVE-2019-20907</a>

<p>Dans Lib/tarfile.py, un attaquant était capable d’élaborer une archive TAR
conduisant à une boucle infinie quand elle était ouverte par tarfile.open,
à cause d’une validation d’en-tête _proc_pax lacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8492">CVE-2020-8492</a>

<p>Python permettait à un serveur HTTP de conduire une attaque par déni de
service à l’aide d’une expression rationnelle (ReDoS) à l’encontre d’un client
à cause d’un retour sur trace désastreux de
urllib.request.AbstractBasicAuthHandler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26116">CVE-2020-26116</a>

<p>http.client permettait une injection CRLF si un attaquant contrôlait la
méthode de requête HTTP, comme le montre l’insertion de caractères de contrôle
CR et LF dans le premier argument de HTTPConnection.request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3177">CVE-2021-3177</a>

<p>Python avait un dépassement de tampon dans PyCArg_repr dans
_ctypes/callproc.c, qui pouvait conduire à une exécution de code à distance dans
certaines applications Python qui acceptaient des nombres avec virgule flottante
comme entrée non fiable, comme le montre un argument 1e300 pour
c_double.from_param. Cela se produisait parce sprintf était utilisé de manière
non sûre.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3733">CVE-2021-3733</a>

<p>Un défaut existait dans la classe AbstractBasicAuthHandler de urllib. Un
attaquant qui contrôlait un serveur HTTP malveillant qui avait un client HTTP
(tel qu’un navigateur web) connecté, pouvait déclencher un déni de service par
expression rationnelle (ReDOS) durant une authentification de requête avec une
charge utile contrefaite pour l'occasion qui était envoyée par le serveur au
client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3737">CVE-2021-3737</a>

<p>Une réponse HTTP improprement gérée dans le code de client HTTP de python
pouvait permettre à un attaquant distant, qui contrôlait un serveur HTTP, de
faire entrer le script client dans une boucle infinie, dévorant du temps CPU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4189">CVE-2021-4189</a>

<p>La bibliothèque cliente FTP (File protocole Transfer) dans le mode PASV
(passif) acceptait l’hôte par défaut de la réponse PASV. Ce défaut permettait
à un attaquant de mettre en place un serveur FTP malveillant qui pouvait amener
des clients FTP de se reconnecter à une adresse IP et un port donnés. Cette
vulnérabilité pouvait conduire à un balayage de ports FTP du client. Pour les
rares utilisateurs qui voudraient le comportement précédent, définissez un
attribut <q>trust_server_pasv_ipv4_address</q> dans votre instance
<q>ftplib.FTP</q> à <q>True</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45061">CVE-2022-45061</a>

<p>Un algorithme quadratique non nécessaire existait dans un chemin lors du
traitement de certaines entrées dans le décodeur IDNA (RFC 3490), qui, si un nom
élaboré et d’une longueur déraisonnable était présenté au décodeur, pouvait
conduire à un déni de service du CPU.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.7.16-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python2.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python2.7,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python2.7">\
https://security-tracker.debian.org/tracker/python2.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3432.data"
# $Id: $
