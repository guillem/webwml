#use wml::debian::translation-check translation="55df9dedff7bd99d6ff2d5a0c1591d8614cc406c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans Asterisk, un autocommutateur téléphonique
privé au code source ouvert. Une vulnérabilité de dépassement de tampon
affectait les utilisateurs qui utilisaient le résolveur DNS PJSIP. Cette
vulnérabilité est relative au
<a href="https://security-tracker.debian.org/tracker/CVE-2022-24793">CVE-2022-24793</a>.
La différence est que ce problème est dans l’analyse de l’enregistrement de
requête <q>parse_query()</q> tandis que ce problème dans
<a href="https://security-tracker.debian.org/tracker/CVE-2022-24793">CVE-2022-24793</a>
est dans <q>parse_rr()</q>. Un contournement est de désactiver la résolution
DNS dans la configuration de PJSIP (en réglant <q>nameserver_count</q>
à zéro) ou d’utiliser à la place une implémentation externe de résolveur.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:16.28.0~dfsg-0+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asterisk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3394.data"
# $Id: $
