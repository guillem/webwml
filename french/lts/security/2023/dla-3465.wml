#use wml::debian::translation-check translation="1a65e2276c49127b7ccb9b88de2312d9172ca393" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité de dépassement de tampon de tas a été découverte dans le
code d’analyse de chunk HTTP de minidlna, un serveur léger DLNA/UPnP-AV. Elle
pouvait permettre l’exécution de code arbitraire.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.2.1+dfsg-2+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets minidlna.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de minidlna,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/minidlna">\
https://security-tracker.debian.org/tracker/minidlna</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3465.data"
# $Id: $
