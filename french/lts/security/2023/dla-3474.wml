#use wml::debian::translation-check translation="abd7fb1613d888aa6799c40016fd5ad9b26c2815" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un dépassement de tampon dans format_timespan() a été corrigé dans systemd,
le système d’initialisation par défaut dans Debian.

De plus, des corrections pour l’obtention des propriétés OnExternalPower à
l’aide de D-Bus et pour une fuite de mémoire lors du rechargement du démon sont
aussi incluses.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 241-7~deb10u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de systemd,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/systemd">\
https://security-tracker.debian.org/tracker/systemd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3474.data"
# $Id: $
