#use wml::debian::translation-check translation="4aec9d92dcfd593e82ecfdb7e3bf23405c0ec417" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été trouvées dans frr, la suite de
protocoles Internet de FRRouting. Des paquets BGP (Border Protocol Gateway)
construits de manière malveillante ou des attributs corrompus de tunnel
pouvaient provoquer un déni de service (plantage d'application) exploitable par
un attaquant distant.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7.5.1-1.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets frr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de frr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/frr">\
https://security-tracker.debian.org/tracker/frr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3573.data"
# $Id: $
