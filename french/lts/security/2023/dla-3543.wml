#use wml::debian::translation-check translation="96e9d1cd886184fecf6bd72f9bd7541b8de1b6ca" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut spécifique lors du traitement de volumes de sauvegarde existait
dans RAR, un programme d’archive pour des fichiers rar. Il permettait à des
attaquants distants d’exécuter du code arbitraire sur les installations
affectées. Une interaction utilisateur était requise pour exploiter cette
vulnérabilité. La cible devait visiter une page malveillante ou ouvrir un
fichier rar malveillant.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:6.23-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rar.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rar,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/rar">\
https://security-tracker.debian.org/tracker/rar</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3543.data"
# $Id: $
