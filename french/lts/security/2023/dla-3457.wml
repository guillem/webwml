#use wml::debian::translation-check translation="0c91772d6fd8e1019aa0374310f4e4a6e0cfa09a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>MaraDNS est un serveur DNS, petit, léger et multiplateforme, au code source
ouvert.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30256">CVE-2022-30256</a>

<p>Un nom de domaine révoqué (appelé nom de domaine <q>Ghost</q>) pouvait
toujours être résolu pendant longtemps en restant dans le cache plus longtemps
que ce que permet max_ttl. Les noms de domaine <q>Ghost</q> incluent des
domaines expirés et des domaines malveillants retirés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31137">CVE-2023-31137</a>

<p>Le serveur faisant autorité dans MaraDNS avait un problème par lequel il
était possible de terminer à distance le processus MaraDNS avec un paquet
spécialement contrefait (appelé <q>paquet de mort</q>).</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.0.13-1.2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets maradns.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de maradns,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/maradns">\
https://security-tracker.debian.org/tracker/maradns</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3457.data"
# $Id: $
