#use wml::debian::translation-check translation="8cf26b59e2238ab325eec19034162d5dcd437c2b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans les nettoyeurs HTML de
Rails, une bibliothèque de nettoyage HTML pour les applications Ruby on Rails.
Un attaquant pouvait lancer des attaques par script intersite (XSS) et de déni
de service (DoS) à l’aide de documents HTML/XML contrefaits.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23517">CVE-2022-23517</a>

<p>Certain configurations utilisent une expression rationnelle inefficace qui
était susceptible d’un retour sur trace excessif lors du nettoyage de certains
attributs SVG. Cela pouvait conduire à un déni de service à travers une
consommation de ressources du CPU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23518">CVE-2022-23518</a>

<p>Script intersite à l’aide d’URI de données lorsqu’utilisé en combinaison avec
Loofah >= 2.1.0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23519">CVE-2022-23519</a>

<p>Une vulnérabilité de script intersite (XSS) avec certaines configurations de
Rails::Html::Sanitizer pouvait permettre à un attaquant d’injecter du contenu
si le développeur de l’application avait outrepassé les étiquettes autorisées du
nettoyeur dans l’une des façons suivantes : autoriser à la fois des éléments
<q>math</q> et <q>style</q> ou autoriser à la fois des éléments <q>svg</q> et
<q>style</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23520">CVE-2022-23520</a>

<p>Vulnérabilité de script intersite (XSS) avec certaines configurations de
Rails::Html::Sanitizer due à un correctif incomplet du
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32209">CVE-2022-32209</a>.
Rails::Html::Sanitizer pouvait permettre à un attaquant d’injecter du contenu
si le développeur de l’application avait outrepassé les étiquettes de nettoyage
pour permettre à la fois les éléments <q>select</q> et <q>style</q>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.0.4-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rails-html-sanitizer.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rails-html-sanitizer,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer">\
https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3566.data"
# $Id: $
