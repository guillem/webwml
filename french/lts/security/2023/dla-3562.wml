#use wml::debian::translation-check translation="02611d118b00d93f7468d03c5b354585599e35dc" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été identifiée dans Orthanc, un serveur DICOM
utilisé pour l’imagerie médicale, par laquelle des utilisateurs d’API
authentifiés avaient la capacité d’écraser des fichiers arbitraires et, dans
certaines configurations, exécuter du code non autorisé.</p>

<p>Cette mise à jour corrige le problème en rétroportant un mécanisme de
protection : l’option RestApiWriteToFileSystemEnabled est désormais incluse et
est réglée à <q>true</q> par défaut dans le fichier de configuration
/etc/orthanc/orthanc.json. Les utilisateurs voulant revenir au comportement
précédent peuvent régler manuellement cette option à  <q>true</q>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.5.6+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets orthanc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de orthanc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/orthanc">\
https://security-tracker.debian.org/tracker/orthanc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3562.data"
# $Id: $
