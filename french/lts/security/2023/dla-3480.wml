#use wml::debian::translation-check translation="f5ff7bac50f078b247d9e68f9dc519745273a125" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de déni de service par expression rationnelle (ReDoS) a été
découvert dans la fonction sanitize_html de redcloth gem. Cette vulnérabilité
permettait à des attaquants de provoquer un déni de service (DoS) à l’aide d’une
charge contrefaite.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 4.3.2-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-redcloth.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-redcloth,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-redcloth">\
https://security-tracker.debian.org/tracker/ruby-redcloth</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3480.data"
# $Id: $
