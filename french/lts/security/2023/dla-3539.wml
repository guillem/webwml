#use wml::debian::translation-check translation="69bedd7465a01c56e061e291391d8a77c1d75cf8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qt4-x11, une boîte à outils
de fenêtrage graphique.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3481">CVE-2021-3481</a>

<p>Lors du rendu et de l’affichage d’un fichier SVG (Scalable Vector Graphics),
ce défaut pouvait conduire à un accès non autorisé en mémoire. La plus grande
menace concernait la confidentialité des données et la disponibilité de
l’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45930">CVE-2021-45930</a>

<p>Écriture hors limites dans
<q>QtPrivate::QCommonArrayOps&lt;QPainterPath::Element&gt;::growAppend</q> (appelé depuis
QPainterPath::addPath et QPathClipper::intersect).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32573">CVE-2023-32573</a>

<p>Utilisation d’un variable non initialisée dans m_unitsPerEm.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32763">CVE-2023-32763</a>

<p>Plantage d'application dans QXmlStreamReader à l'aide d'une chaine XML
contrefaite qui déclenchait une situation dans laquelle le préfixe était
supérieur à la longueur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34410">CVE-2023-34410</a>

<p>La validation de certificat pour TLS ne considérait pas toujours si la racine
d’une chaine était un certificat CA autorisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-37369">CVE-2023-37369</a>

<p>Un plantage d'application était possible dans QXmlStreamReader à l'aide d'une
chaine XML contrefaite qui déclenchait une situation dans laquelle le préfixe était
supérieur à la longueur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38197">CVE-2023-38197</a>

<p>Boucle infinie dans l’expansion récursive d’entité.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 4:4.8.7+dfsg-18+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qt4-x11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qt4-x11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qt4-x11">\
https://security-tracker.debian.org/tracker/qt4-x11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3539.data"
# $Id: $
