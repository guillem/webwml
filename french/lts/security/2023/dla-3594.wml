#use wml::debian::translation-check translation="59f97a38f394a8c4692cf28646c014c70e1c4d62" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été découverts dans cups, le système d'impression pour
UNIX™.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4504">CVE-2023-4504</a>

<p>À cause de vérifications absentes de limites, un dépassement de tampon de tas
et une exécution de code étaient possibles en utilisant des documents
PostScript contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32360">CVE-2023-32360</a>

<p>Des utilisateurs non autorisés pouvaient récupérer des documents imprimés
récemment.</p>

<p>Puisque c’est un correctif de configuration, il se peut qu’il ne soit
pas appliqué lors d’une mise à jour du paquet. Veuillez revérifier que votre
fichier /etc/cups/cupds.conf limite l’accès CUPS-Get-Document avec quelque chose
comme ce qui suit :</p>
<ul>
<li>  &lt;Limit CUPS-Get-Document&gt;</li>
<li>   AuthType Default</li>
<li>  Require utilisateur @OWNER @SYSTEM</li>
<li>  Order deny,allow</li>
<li>  &lt;/Limit&gt;</li>
</ul>
<p>(La ligne importante dans cette section est <q>AuthType Default</q>)</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.2.10-6+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cups.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cups,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cups">\
https://security-tracker.debian.org/tracker/cups</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3594.data"
# $Id: $
