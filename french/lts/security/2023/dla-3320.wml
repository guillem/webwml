#use wml::debian::translation-check translation="5d7fdc280e863be469c4a141eef0808f9d01495e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La vulnérabilité suivante a été découverte dans le moteur web WebKitGTK.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23529">CVE-2023-23529</a>

<p>Un chercheur anonyme a découvert que le traitement d’un contenu web
malveillant pouvait conduire à l’exécution de code arbitraire. Apple est averti
que ce problème a été largement exploité.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.38.5-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3320.data"
# $Id: $
