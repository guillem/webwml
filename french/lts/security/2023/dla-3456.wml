#use wml::debian::translation-check translation="c5eda3b0c3a68a5a0b89ba3cbb136616fd0eed7c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Requests, une bibliothèque HTTP de Python, oubliait les en-têtes
Proxy-Authorization vers les serveurs de destination lors d’une redirection vers
un terminal HTTPS. Pour les connexions HTTP envoyées à travers le tunnel, le
mandataire identifiait l’en-tête dans la requête elle-même et la supprimait
avant de la rediriger vers le serveur de destination. Cependant, lors d’un envoi
à travers HTTPS, l’en-tête <q>Proxy-Authorization</q> doit être envoyer dans la
requête CONNECT car le mandataire n’a aucune visibilité dans la requête
<q>tunnelisée</q>. Cela aboutissait à ce que Requests transmettait les
identifiants du mandataire au serveur de destination involontairement,
permettant éventuellement à un acteur malveillant d’obtenir des informations
sensibles.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.21.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets requests.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de requests,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/requests">\
https://security-tracker.debian.org/tracker/requests</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3456.data"
# $Id: $
