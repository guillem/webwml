#use wml::debian::translation-check translation="1d7c6277bd0111e239bd45d329f5511bcef8154c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de dépassement de tampon ont été trouvées
dans libvpx, une bibliothèque multimédia pour les codecs vidéo VP8 et VP9, qui
pouvaient aboutir à l’exécution de code arbitraire si un flux média VP8 ou VP9
contrefait pour l'occasion était traité.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.7.0-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvpx.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libvpx,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libvpx">\
https://security-tracker.debian.org/tracker/libvpx</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3598.data"
# $Id: $
