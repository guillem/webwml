#use wml::debian::translation-check translation="07967a998711f2b3609cd00a3c847cd2ba1062cf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de déni de
service dans in Django, un cadriciel populaire de développement web.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41164">CVE-2023-41164</a></li>

<p>L’amont a signalé qu’il existait une vulnérabilité potentielle dans
<code>django.utils.encoding.uri_to_iri()</code>. Cette méthode était sujette
à une attaque potentielle par déni de service à l’aide de certaines entrées
ayant un très grand nombre de caractères Unicode.</p>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:1.11.29-1+deb10u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3558.data"
# $Id: $
