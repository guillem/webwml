#use wml::debian::translation-check translation="abdedbee955c7d29c5ae8ab13e96eb9eda0ca829" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans gerbv, un visualisateur pour
le format Gerber pour la conception de circuits imprimés (PCB).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40393">CVE-2021-40393</a>

<p>Écriture hors limites dans les variables <q>d’aperture</q> de macros au
format RS-274X.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40394">CVE-2021-40394</a>

<p>Dépassement d'entier dans les variables <q>d’aperture</q> de primitives
de macros de contour au format RS-274X.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4508">CVE-2023-4508</a>

<p>Accès en mémoire hors limites lors du référencement de fichiers externes.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.7.0-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gerbv.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gerbv,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gerbv">\
https://security-tracker.debian.org/tracker/gerbv</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3593.data"
# $Id: $
