#use wml::debian::translation-check translation="ab04c93a4cf9eff2ad7ac326478c0d1d8d24e0ac" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans GNU cpio, un programme pour gérer
les archives de fichiers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14866">CVE-2019-14866</a>

<p>Validation impropre des fichiers d’entrée lors de la génération d’archives
tar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38185">CVE-2021-38185</a>

<p>Code arbitraire à l’aide de fichier de patron contrefait.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.12+dfsg-9+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cpio.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cpio,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cpio">\
https://security-tracker.debian.org/tracker/cpio</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3445.data"
# $Id: $
