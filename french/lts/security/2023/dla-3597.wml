#use wml::debian::translation-check translation="b6a8122fef4356d74a7a69dd35c0dec753f59dd2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans <q>Open VMware
Tools</q>. Un opérateur malveillant ayant les privilèges <q>Guest Operation</q>
dans une machine virtuelle cible pouvait élever ses privilèges si cette machine
avait un <q>Guest Alias</q> plus privilégié.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:10.3.10-1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets open-vm-tools.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de open-vm-tools,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/open-vm-tools">\
https://security-tracker.debian.org/tracker/open-vm-tools</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3597.data"
# $Id: $
