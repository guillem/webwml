#use wml::debian::translation-check translation="3901ce46ec8b677310ff46ac62b7f9b7c71282a7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans qpdf, un paquet d’outils pour
transformer et inspecter des fichiers PDF. Des fichiers contrefaits permettaient
à des attaquants distants d’exécuter du code arbitraire ou de créer des appels
récursifs pendant longtemps, ce qui causait un déni de service. De plus, un
dépassement de tampon de tas pouvait survenir quand une écriture en aval
échouait.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 8.4.0-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qpdf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qpdf,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qpdf">\
https://security-tracker.debian.org/tracker/qpdf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3548.data"
# $Id: $
