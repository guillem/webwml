#use wml::debian::translation-check translation="089826b8a40b7284f7127801c0b4c2cde8ded27b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Robert Merget, Marcus Brinkmann, Nimrod Aviram et Juraj Somorovsky ont
découvert que certaines suites cryptographiques Diffie-Hellman dans la
spécification TLS et mises en œuvre par OpenSSL contenaient un défaut. Un
attaquant distant pourrait éventuellement utiliser cela pour écouter des
communications chiffrées. Cela a été corrigé dans cette mise à jour en
désactivant les suites non sécurisées.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.0.2u-1~deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl1.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl1.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl1.0">https://security-tracker.debian.org/tracker/openssl1.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2378.data"
# $Id: $
