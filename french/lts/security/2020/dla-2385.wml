#use wml::debian::translation-check translation="a4ce829c5f13cdfda040d170cfd7fcc0b7844752" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3874">CVE-2019-3874</a>

<p>Les tampons du noyau alloués par le protocole réseau SCTP n’étaient pas
limités par le contrôleur de mémoire cgroup. Un utilisateur local pouvait
éventuellement utiliser cela pour contourner les limites de mémoire de conteneur
et provoquer un déni de service (utilisation excessive de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19448">CVE-2019-19448</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19813">CVE-2019-19813</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19816">CVE-2019-19816</a>

<p><q>Team bobfuzzer</q> a signalé des bogues dans Btrfs qui pourraient conduire
à une utilisation de mémoire après libération ou à un dépassement de tampon de
tas, et qui pourraient être déclenchés par des images contrefaites de système de
fichiers. Un utilisateur autorisé à monter et accéder à des systèmes de fichiers
arbitraires pouvait utiliser cela pour provoquer un déni de service (plantage
ou corruption de mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10781">CVE-2020-10781</a>

<p>Luca Bruno de Red Hat a découvert que le fichier de contrôle zram
/sys/class/zram-control/hot_add était lisible par tous les utilisateurs. Sur un
système avec zram activé, un utilisateur local pouvait utiliser cela pour
provoquer un déni de service (épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12888">CVE-2020-12888</a>

<p>Il a été découvert que le pilote PCIe Virtual Function I/O (vfio-pci)
permettait aux utilisateurs de désactiver un espace mémoire de périphérique
encore mappé dans un processus. Sur certaines plateformes matérielles, des
utilisateurs locaux ou des machines virtuelles clientes autorisés à accéder
à des fonctions virtuelles PCIe pouvaient utiliser cela pour provoquer un déni
de service (erreur de matériel et plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14314">CVE-2020-14314</a>

<p>Un bogue a été découvert dans le système de fichiers ext4 qui pouvait
conduire à une lecture hors limites. Un utilisateur local autorisé à monter et
accéder à des images arbitraires de système de fichiers pouvait utiliser cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14331">CVE-2020-14331</a>

<p>Un bogue a été découvert dans la fonction soft-scrollback de pilote de
console VGA qui pouvait conduire à un dépassement de tampon de tas. Sur un
système avec un noyau personnalisé ayant CONFIG_VGACON_SOFT_SCROLLBACK activé,
un utilisateur local avec accès à une console pouvait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14356">CVE-2020-14356</a>

<p>Un bogue a été découvert dans le traitement de références cgroup de socket
de sous-système cgroup. Dans certaines configurations cgroup, cela pouvait
conduire à une utilisation de mémoire après libération. Un utilisateur local
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14385">CVE-2020-14385</a>

<p>Un bogue a été découvert dans XFS qui pouvait conduire à un attribut étendu
(xattr) d’être faussement détecté comme non valable. Un utilisateur local avec
accès à un système de fichiers XFS pouvait utiliser cela pour provoquer un déni
de service (shutdown pour un système de fichiers).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14386">CVE-2020-14386</a>

<p>Or Cohen a découvert un bogue dans l’implémentation de socket de paquet
(AF_PACKET) qui pouvait conduire à un dépassement de tampon de tas. Un
utilisateur local avec la capacité CAP_NET_RAW (dans n’importe quel espace de
nommage utilisateur) pouvait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14390">CVE-2020-14390</a>

<p>Minh Yuan a découvert un bogue dans la fonction scrollback de pilote de
console de tampon de trame qui pouvait conduire à un dépassement de tampon de
tas. Sur un système utilisant les consoles de tampon de trame, un utilisateur
local avec accès à une console pouvait utiliser cela pour provoquer un déni de
service (plantage ou corruption de mémoire) ou, éventuellement, pour une
élévation des privilèges.</p>

<p>La fonction scrollback a été désactivée pour le moment, car aucun autre
correctif n’était disponible pour ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16166">CVE-2020-16166</a>

<p>Amit Klein a signalé que le générateur de nombres aléatoires utilisé par la
pile réseau pouvait n’être pas réinitialisé (changement de graine) pour de
longues périodes, rendant par exemple les allocations de numéro de port plus
prévisibles. Cela facilitait pour des attaquants distants certaines conduites
d’attaque telles que l’empoisonnement de cache DNS ou le suivi de périphérique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25212">CVE-2020-25212</a>

<p>Un bogue a été découvert dans l’implémentation de client NFSv4 qui pouvait
conduire à un dépassement de tampon de tas. Un serveur NFS malveillant pouvait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour exécuter du code arbitraire sur le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25284">CVE-2020-25284</a>

<p>Il a été découvert que le pilote de périphérique bloc Rados (rbd) permettait
aux tâches exécutées avec UID 0 d’ajouter ou retirer des périphériques rbd, même
si elles supprimaient leurs capacités. Sur un système avec le pilote rbd chargé,
cela pouvait permettre une élévation des privilèges d’un conteneur avec une
tâche exécutée en tant que superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25285">CVE-2020-25285</a>

<p>Une situation de compétition a été découverte dans les gestionnaires
de sysctl du système de fichiers hugetlb qui pouvait conduire à une corruption de pile.
Un utilisateur local avec accès en écriture sur les sysctl de grandes pages
(hugepage) pouvait utiliser cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou, éventuellement, pour une élévation des privilèges.
Par défaut, seul le superutilisateur peut faire cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25641">CVE-2020-25641</a>

<p>L’outil syzbot a trouvé un bogue dans la couche bloc qui pouvait conduire
à une boucle infinie. Un utilisateur local avec accès à un périphérique bloc
brut pouvait utiliser cela pour provoquer un déni de service (utilisation non
limitée de CPU et blocage possible du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26088">CVE-2020-26088</a>

<p>Il a été découvert que l’implémentation de socket NFC (Near Field
Communication) permettait à n’importe quel utilisateur de créer des sockets
raw. Sur un système avec une interface NFC, cela permettait à des utilisateurs
locaux de contourner la politique de sécurité réseau.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.19.146-1~deb9u1. De plus, cette mise à jour corrige les bogues
Debian <a href="https://bugs.debian.org/966846">n° 966846</a>,
<a href="https://bugs.debian.org/966917">n° 966917</a> et
<a href="https://bugs.debian.org/968567">n° 968567</a>, et inclus beaucoup
d’autres corrections de bogue des mises à jour 4.19.133 à 4.19.146, celles-ci
comprises.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2385.data"
# $Id: $
