#use wml::debian::translation-check translation="f552e8ec29e35c8c4f8d4c79a09ea7229bfcc07f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans e2fsprogs, un paquet contenant des
utilitaires pour les systèmes de fichiers ext2/ext3/ext4. Un répertoire ext4
contrefait pour l'occasion pouvait provoquer une écriture hors limites dans la
pile, aboutissant à une exécution de code. Un attaquant pouvait altérer une
partition pour déclencher cette vulnérabilité.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.43.4-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets e2fsprogs.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de e2fsprogs, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/e2fsprogs">https://security-tracker.debian.org/tracker/e2fsprogs</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2290.data"
# $Id: $
