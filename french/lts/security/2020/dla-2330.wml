#use wml::debian::translation-check translation="5d3f51fb2d1af284d1861aaf0c0e6f7eaabb3d3a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Brève description</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>

<p>Attaque par séparation de réponse dans le serveur HTTP de WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8320">CVE-2019-8320</a>

<p>Suppression de répertoire en utilisant un lien symbolique lors d’une
décompression tar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8321">CVE-2019-8321</a>

<p>Vulnérabilité d’injection de séquence d’échappement dans verbose.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8322">CVE-2019-8322</a>

<p>Vulnérabilité d’injection de séquence d’échappement dans owner de gem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8323">CVE-2019-8323</a>

<p>Vulnérabilité d’injection de séquence d’échappement dans la gestion de
réponse de l’API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8324">CVE-2019-8324</a>

<p>Installation d’un gem malveillant pouvant conduire à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8325">CVE-2019-8325</a>

<p>Vulnérabilité d’injection de séquence d’échappement lors d’erreurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16201">CVE-2019-16201</a>

<p>Vulnérabilité, à l’aide d’expression rationnelle, de déni de service de
l’authentification d’accès Digest de WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16254">CVE-2019-16254</a>

<p>Attaque par séparation de réponse HTTP dans le serveur HTTP de WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16255">CVE-2019-16255</a>

<p>Vulnérabilité d’injection de code.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.7.26-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jruby.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jruby, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jruby">https://security-tracker.debian.org/tracker/jruby</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2330.data"
# $Id: $
