#use wml::debian::translation-check translation="fd2aa6e3295b46debb225d787f43e669633484d3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le gem rubyzip, versions 1.2.1 et précédentes, contenait une vulnérabilité de
traversée de répertoire dans le composant Zip::File pouvant aboutir à l’écriture
de fichiers arbitraires dans le système de fichiers.</p>

<p>Cette attaque apparaît comme exploitable à travers un site permettant de
téléverser des fichiers .zip. Un attaquant peut téléverser un fichier malveillant
contenant des liens symboliques ou des fichiers avec des noms de chemin absolu
« ../ » pour écrire des fichiers arbitraires dans le système de fichiers.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.2.0-1.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-zip.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-zip, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-zip">https://security-tracker.debian.org/tracker/ruby-zip</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2307.data"
# $Id: $
