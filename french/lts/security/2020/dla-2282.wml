#use wml::debian::translation-check translation="72645174a66deeb4948c363e88b088964513350a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans Ruby on Rails, un cadriciel MVC
basé sur Ruby et destiné au développement d’applications web, qui pourraient
conduire à une exécution de code à distance et à l’utilisation d’entrée
utilisateur non fiable, selon l’application.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8163">CVE-2020-8163</a>

<p>Une vulnérabilité d’injection de code dans Rails pouvait permettre à un
attaquant contrôlant l’argument `locals` de l’appel `render` de réaliser une
rétro-ingénierie (RCE).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8164">CVE-2020-8164</a>

<p>Une vulnérabilité de désérialisation de données non approuvées existait dans
Rails qui pouvait permettre à un attaquant de fournir des informations qui pouvaient
être divulguées par inadvertance à partir de paramètres forts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8165">CVE-2020-8165</a>

<p>Une vulnérabilité de désérialisation de données non approuvées existait dans
Rails qui pouvait permettre à un attaquant de déconvertir des objets fournis
par l’utilisateur dans MemCacheStore et RedisCacheStore, aboutissant
éventuellement à une rétro-ingénierie.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:4.2.7.1-1+deb9u3.</>

<p>Nous vous recommandons de mettre à jour vos paquets rails.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rails, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2282.data"
# $Id: $
