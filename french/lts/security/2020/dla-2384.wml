#use wml::debian::translation-check translation="ec86b99b1d23567995bd787667d629ade1ba951a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans yaws, un serveur web HTTP 1.1 très
performant écrit en Erlang.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24379">CVE-2020-24379</a>

<p>Rejet des requêtes externes de ressource dans DAV afin d’éviter des attaques
par entité externe XML (XXE).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24916">CVE-2020-24916</a>

<p>Nettoyage de l’exécutable CGI afin d’éviter des injections de commande
à l’aide de requêtes CGI.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0.4+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets yaws.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de yaws, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/yaws">https://security-tracker.debian.org/tracker/yaws</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2384.data"
# $Id: $
