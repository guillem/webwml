#use wml::debian::translation-check translation="d4f959ecd48e95cfca3c5d3ac885a180edba9bb2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une potentielle vulnérabilité de script intersite dans
php-horde-mime-viewer, une bibliothèque de visualiseur MIME pour la
plateforme d'outil de travail en groupe Horde.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26874">CVE-2022-26874</a>

<p>lib/Horde/Mime/Viewer/Ooo.php dans Horde Mime_Viewer avant la
version 2.2.4 permet un script intersite à l'aide d'un document OpenOffice,
menant à une prise de contrôle de compte dans <q>Horde Groupware Webmail
Edition</q>. Cela survient après le rendu XSLT.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.2.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets
php-horde-mime-viewer.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3045.data"
# $Id: $
