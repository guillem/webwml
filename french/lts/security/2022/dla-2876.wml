#use wml::debian::translation-check translation="8c667e0975113ced0f019655105e5baecbc43ce2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découvertes dans Vim, une version améliorée de
l'éditeur de text Vi :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17087">CVE-2017-17087</a>

<p>fileio.c dans Vim fixe l'appartenance de groupe d'un fichier .swp au
groupe primaire de l'éditeur (qui peut être différente de l'appartenance de
groupe du fichier original). Cela permet aux utilisateurs locaux d'obtenir
des informations en exploitant une appartenance à un groupe en vigueur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20807">CVE-2019-20807</a>

<p>Les utilisateurs peuvent contourner le mode restreint rvim et exécuter
des commandes arbitraires du système d'exploitation au moyen d'interfaces
de script (par exemple, Python, Ruby ou Lua).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3778">CVE-2021-3778</a>

<p>Un dépassement de tampon de tas avec un caractère UTF-8 non valable a
été détecté dans regexp_nfa.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3796">CVE-2021-3796</a>

<p>Une erreur d'utilisation de la mémoire de tas après libération a été
détectée dans normal.c. Une exploitation réussie pourrait conduire à une
exécution de code.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2:8.0.0197-4+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vim, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2876.data"
# $Id: $
