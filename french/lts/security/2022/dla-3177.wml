#use wml::debian::translation-check translation="645af6f65572a1f557e647fa59ecac6fbb1d3a5e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait plusieurs vulnérabilités dans Django, un cadriciel populaire
de développement web basé sur Python :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28346">CVE-2022-28346</a> :
Un problème a été découvert dans les versions de Django 2.2 antérieures
à 2.2.28, 3.2 antérieures à 3.2.13 et 4.0 antérieures à 4.0.4. Les méthodes
QuerySet.annotate(), aggregate() et extra() sont sujettes à une injection
de code SQL dans les alias de colonnes à l'aide d'un dictionnaire
contrefait (avec une expansion dictionnaire) en tant qu'argument **kwargs
passés à ces méthodes.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45115">CVE-2021-45115</a> :
Un problème a été découvert dans les versions de Django 2.2 antérieures
à 2.2.26, 3.2 antérieures à 3.2.11 et 4.0 antérieures à 4.0.1.
UserAttributeSimilarityValidator était exposé à une surcharge importante
lors de l'évaluation d'un mot de passe reçu grossi de façon artificielle
par rapport aux valeurs de référence. Dans les cas où l'accès à
l'enregistrement des utilisateurs n'était pas restreint, cela offrait un
vecteur potentiel pour une attaque par déni de service.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45116">CVE-2021-45116</a> :
Un problème a été découvert dans les versions de Django 2.2 antérieures
à 2.2.26, 3.2 antérieures à 3.2.11 et 4.0 antérieures à 4.0.1. Du fait de
l'exploitation de la logique de résolution de variable du langage de
gabarits de Django, le filtre de gabarit dictsort était éventuellement
vulnérable à une divulgation d'informations ou à l'appel d'une méthode
imprévue, si une clé contrefaite de façon appropriée était passée.</li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 1:1.11.29-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3177.data"
# $Id: $
