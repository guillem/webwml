#use wml::debian::translation-check translation="37790c13cec4d2dc57cef09a571b664bfdfd3872" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Sven Klemm a découvert que certaines extensions du système de base de
données PostgreSQL pouvaient remplacer des objets qui n'appartiennent pas à
l'extension. Un attaquant pouvait exploiter cela pour exécuter des
commandes arbitraires en tant qu'un autre utilisateur.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
11.17-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3072.data"
# $Id: $
