#use wml::debian::translation-check translation="16272f118f9eba5021ebed7b84391be06cfd83e4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Nathan Davison a découvert qu'HAProxy, un serveur mandataire inverse de
répartition de charge, ne rejetait pas correctement des requêtes ou des
réponses présentant un en-tête transfer-encoding auquel manque la valeur
<q>chunked</q> ce qui pourrait faciliter une attaque par dissimulation de
requête HTTP. En outre, plusieurs défauts ont été découverts dans les
fonctions liées au DNS qui peuvent déclencher une récursion infinie menant
à un déni de service ou à une fuite d'informations due à une lecture hors
limites.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.7.5-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets haproxy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de haproxy, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/haproxy">\
https://security-tracker.debian.org/tracker/haproxy</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3034.data"
# $Id: $
