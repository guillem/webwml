#use wml::debian::translation-check translation="74a36603e9ef28016e065ee5527c6181753cd195" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Twisted, un cadriciel en Python basé sur les évènements pour construire
des applications web, est affecté par des vulnérabilités de fractionnement
de requête HTTP, et peut exposer des données sensibles en suivant des
redirections. Un attaquant peut contourner des vérifications de validation
et récupérer des identifiants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10108">CVE-2020-10108</a>

<p>Vulnérabilité de fractionnement de requête HTTP. Lorsque deux en-têtes
content-length sont fournis, le premier en-tête est ignoré. Lorsque la
seconde valeur content-length était réglée à zéro, le corps de la requête
était interprété comme une requête redirigée (pipe).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10109">CVE-2020-10109</a>

<p>Vulnérabilité de fractionnement de requête HTTP. Lorsqu’un en-tête
content-length et un encodage en bloc étaient fournis, le content-length
prévalait et la suite du corps de la requête était interprétée comme une
requête redirigée (pipe).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21712">CVE-2022-21712</a>

<p>Twisted expose les en-têtes « Cookie » et « Authorization » lorsqu'il
suit des redirections d'origines multiples. Ce problème est présent dans les
fonctions « twisted.web.RedirectAgent » et
« twisted.web.BrowserLikeRedirectAgent ».</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 16.6.0-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets twisted.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de twisted, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/twisted">\
https://security-tracker.debian.org/tracker/twisted</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2927.data"
# $Id: $
