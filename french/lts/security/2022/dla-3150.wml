#use wml::debian::translation-check translation="33334756f36a41dc9f9bbaf08a4b4c23d83dad07" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d'injection de commande a été découverte dans Rexical,
un générateur d'analyseur lexical pour le langage de programmation Ruby.
Les processus ne sont vulnérables que si la méthode non documentée
<q>Nokogiri::CSS::Tokenizer#load_file</q> est appelée avec une entrée non
sûre de l'utilisateur comme nom de fichier. Cette vulnérabilité apparaît
dans le code généré par le gem Rexical.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
1.0.5-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rexical.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rexical, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rexical">\
https://security-tracker.debian.org/tracker/rexical</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3150.data"
# $Id: $
