#use wml::debian::translation-check translation="f8f0303f3316c724e7f8afaa3d01eb3d18c67c56" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans virglrenderer,
un GPU virtuel pour la virtualisation avec KVM.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18388">CVE-2019-18388</a>

<p>Un déréférencement de pointeur NULL dans vrend_renderer.c dans virglrenderer
jusqu’à la version 0.8.0 permet aux utilisateurs du système d’exploitation
client de provoquer un déni de service à l’aide de commandes mal formées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18389">CVE-2019-18389</a>

<p>Un dépassement de tampon de tas dans la fonction
vrend_renderer_transfer_write_iov dans vrend_renderer.c dans virglrenderer
jusqu’à la version 0.8.0 permet aux utilisateurs du système d’exploitation
client de provoquer un déni de service, ou un échappement de client vers hôte de
QEMU et une exécution de code, à l’aide de commandes
VIRGL_CCMD_RESOURCE_INLINE_WRITE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18390">CVE-2019-18390</a>

<p>Une lecture hors limites dans la fonction vrend_blit_need_swizzle dans
vrend_renderer.c dans virglrenderer jusqu’à la version 0.8.0 permet aux
utilisateurs du système d’exploitation client de provoquer un déni de service à
l’aide de commandes VIRGL_CCMD_BLIT.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18391">CVE-2019-18391</a>

<p>Un dépassement de tampon de tas dans la fonction
vrend_renderer_transfer_write_iov dans vrend_renderer.c dans virglrenderer
jusqu’à la version 0.8.0 permet aux utilisateurs du système d’exploitation
client de provoquer un déni de service à l’aide de commandes
VIRGL_CCMD_RESOURCE_INLINE_WRITE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8002">CVE-2020-8002</a>

<p>Un déréférencement de pointeur NULL dans vrend_renderer.c dans virglrenderer
jusqu’à la version 0.8.1 permet à des attaquants de provoquer un déni de service
à l’aide de commandes essayant de lancer une grille sans avoir fourni auparavant
un CS (Compute Shader).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8003">CVE-2020-8003</a>

<p>Une vulnérabilité de double libération de zone de mémoire dans
vrend_renderer.c dans virglrenderer jusqu’à la version 0.8.1 permet à des
attaquants de provoquer un déni de service en provoquant un échec
d’allocation de texture, parce que vrend_renderer_resource_allocated_texture
n’est pas à l’endroit approprié pour une libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0135">CVE-2022-0135</a>

<p>Un problème d’écriture hors limites a été découvert dans le rendu VirGL
virtuel d’OpenGL (virglrenderer). Ce défaut permet à un client malveillant de
créer une ressource virgil contrefaite pour l'occasion et puis d'émettre un ioctl
VIRTGPU_EXECBUFFER, conduisant à un déni de service ou à une exécution
possible de code.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 0.7.0-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets virglrenderer.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de virglrenderer,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/virglrenderer">\
https://security-tracker.debian.org/tracker/virglrenderer</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3232.data"
# $Id: $
