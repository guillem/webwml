#use wml::debian::translation-check translation="a2cc465cc334680bfb95d13cea1bd224cb0ccb1b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans lrzip, un
programme de compression. Des pointeurs non valables, des utilisations de
mémoire après libération et des boucles infinies pouvaient permettre à des
attaquants de provoquer un déni de service ou éventuellement un autre
impact non spécifié à l'aide d'un fichier compressé contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5786">CVE-2018-5786</a>

<p>Il y avait une boucle infinie et un blocage d'application dans la
fonction get_fileinfo (lrzip.c). Des attaquants distants pouvaient
exploiter cette vulnérabilité pour provoquer un déni de service à l'aide
d'un fichier lrz contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25467">CVE-2020-25467</a>

<p>Un déréférencement de pointeur NULL a été découvert dans la fonction
lzo_decompress_buf dans stream.c qui permet à un attaquant de provoquer un
déni de service (DoS) à l'aide d'un fichier compressé contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27345">CVE-2021-27345</a>

<p>Un déréférencement de pointeur NULL a été découvert dans la fonction
ucompthread dans stream.c qui permet à des attaquants de provoquer un déni
de service (DoS) à l'aide d'un fichier compressé contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27347">CVE-2021-27347</a>

<p>Une utilisation après libération dans la fonction lzma_decompress_buf
dans stream.c de lrzip permet à des attaquants de provoquer un déni de
service (DoS) à l'aide d'un fichier compressé contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26291">CVE-2022-26291</a>

<p>Il a été découvert que lrzip contenait une utilisation de mémoire après
libération multiple de concurrence entre les fonctions
zpaq_decompress_buf() et clear_rulist(). Cette vulnérabilité permet à des
attaquants de provoquer un déni de service (DoS) à l'aide d'un fichier lrz
contrefait.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.631-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lrzip.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lrzip, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lrzip">\
https://security-tracker.debian.org/tracker/lrzip</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2981.data"
# $Id: $
