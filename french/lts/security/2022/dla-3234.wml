#use wml::debian::translation-check translation="f8d1600b7e9ee552ee38f893ee0e5fa14d69b349" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que ceux qui utilisent java.sql.Statement ou
java.sql.PreparedStatement dans hsqldb, une base de données SQL en Java, pour
traiter des entrées non fiables pourraient être vulnérables à une attaque
d’exécution de code à distance. Par défaut, il est permis d’appeler n’importe
quelle méthode statique de n’importe quelle classe Java dans le classpath,
aboutissant à une exécution de code. Le problème peut être évité en mettant à
jour vers la version 2.4.1-2+deb10u1 ou en réglant la propriété de système
<q>hsqldb.method_class_names</q> aux classes autorisées à être appelées – par
exemple, System.setProperty("hsqldb.method_class_names","abc") ou en utilisant
l’argument de Java <q>-Dhsqldb.method_class_names="abc"</q>. Depuis la
version 2.4.1-2+deb10u1, toutes les classes par défaut ne sont pas accessibles
sauf celles dans java.lang.Math, et doivent être autorisées manuellement.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 2.4.1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hsqldb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de hsqldb,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/hsqldb">\
https://security-tracker.debian.org/tracker/hsqldb</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3234.data"
# $Id: $
