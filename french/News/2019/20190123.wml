#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 9.7</define-tag>
<define-tag release_date>2019-01-23</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.7</define-tag>

<define-tag dsalink><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Le projet Debian a l'honneur d'annoncer la septième mise à jour de sa
distribution stable Debian <release> (nommée <q><codename></q>).
Cette mise à jour intègre la récente mise à jour de sécurité pour <srcpkg APT>,
afin d'assurer que les nouvelles installations de <codename> ne sont pas
vulnérables. Aucune autre mise à jour n'est incluse.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Du fait de la nature de la mise à jour incluse, il est
recommandé de suivre les instructions mentionnées dans <dsalink 2019 4371>.</p>

<p>Une liste complète des miroirs est disponible à l'adresse :</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes au paquet suivant :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction base-files "Mise à jour pour cette version">
</table>

<h2>Mise à jour de sécurité</h2>


<p>
Cette révision ajoute la mise à jour de sécurité suivante à la version
stable. L'équipe de sécurité a déjà publié une annonce pour cette
mise à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4371 apt>
</table>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
