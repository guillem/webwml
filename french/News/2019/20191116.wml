#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.2</define-tag>
<define-tag release_date>2019-11-16</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la deuxième mise à jour de sa
distribution stable Debian <release> (codename <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction aegisub "Correction de plantage lors de la sélection d'une langue à la fin de la liste <q>Spell checker language</q> ; correction de plantage lors d'un clic droit sur la zone de texte des sous-titres">
<correction akonadi "Correction de divers problèmes de plantage ou de blocage">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction capistrano "Correction d'échec pour supprimer les versions anciennes quand il y en a trop">
<correction cron "Plus d'utilisation de l'API obsolète de SELinux">
<correction cyrus-imapd "Correction de perte de données lors d'une mise à niveau à partir des versions 3.0.0 ou précédentes">
<correction debian-edu-config "Gestion des nouveaux fichiers de configuration de Firefox ESR ; ajout d'un paragraphe post-up conditionnel à l'entrée eth0 de /etc/network/interfaces">
<correction debian-installer "Correction de fontes illisibles sur les affichages HiDPI dans les images netboot démarrées avec EFI">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction distro-info-data "Ajout d'Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter "Nouvelle version amont stable ; correction de la prise en charge de sysvinit ; captation de plus d'erreurs d'encodage ASCII pour améliorer la résilience face aux mauvaises données ; correction de l'extraction de message de façon que la signature dans le filtre de courriel dans la même passe que la vérification fonctionne correctement">
<correction emacs "Mise à jour de la clé d'empaquetage d'EPLA">
<correction fence-agents "Correction de la suppression incomplète de fence_amt_ws">
<correction flatpak "Nouvelle version amont stable">
<correction flightcrew "Corrections de sécurité [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "Correction de la sélection d'une fonte trop agressive parmi les fontes Noto CJK dans les navigateurs web modernes avec une « locale » chinoise">
<correction freetype "Gestion correcte des points fantômes pour les fontes variables lissées">
<correction gdb "Reconstruction avec la nouvelle version de libbabeltrace avec un numéro de version plus élevé pour éviter un conflit avec l'envoi précédent">
<correction glib2.0 "Possibilité assurée d'authentification des clients libdbus avec un GDBusServer comme celui dans ibus">
<correction gnome-shell "Nouvelle version amont stable ; correction de troncature des messages longs dans les boîtes de dialogue Shell modales ; plantage évité lors de la réallocation d'acteurs morts">
<correction gnome-sound-recorder "Correction de plantage lors de la sélection d'un enregistrement">
<correction gnustep-base "Désactivation du démon gdomap qui avait été accidentellement activé lors des mises à niveau à partir de Stretch">
<correction graphite-web "Suppression de la fonction <q>send_email</q> inutilisée [CVE-2017-18638] ; erreur toutes les heures évitée dans cron quand il n'y pas de base de données Whisper">
<correction inn2 "Correction de négociation des suites de chiffrement DHE">
<correction libapache-mod-auth-kerb "Correction d'un bogue d'utilisation de mémoire après libération menant à un plantage">
<correction libdate-holidays-de-perl "Marquage de la journée internationale de l'enfance (20 septembre) comme fériée à partir de 2019 en Thuringe">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libofx "Correction d'un problème de déréférencement de pointeur NULL [CVE-2019-9656]">
<correction libreoffice "Correction du pilote de postgresql avec PostgreSQL 12">
<correction libsixel "Correction de plusieurs problèmes de sécurité [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Correction de pointeur bancal dans xsltCopyText [CVE-2019-18197]">
<correction lucene-solr "Désactivation de l'appel obsolète à ContextHandler dans solr-jetty9.xml ; correction des permissions de Jetty sur l'index SOLR">
<correction mariadb-10.3 "Nouvelle version amont stable">
<correction modsecurity-crs "Correction des règles de téléchargement de script PHP [CVE-2019-13464]">
<correction mutter "Nouvelle version amont stable">
<correction ncurses "Correction de plusieurs problèmes de sécurité [CVE-2019-17594 CVE-2019-17595] et d'autres problèmes dans tic">
<correction ndppd "Fichier PID accessible en écriture à tous évité, ce qui cassait les scripts de démarrage du démon">
<correction network-manager "Correction des permissions de fichier pour <q>/var/lib/NetworkManager/secret_key</q> et <q>/var/lib/NetworkManager</q>">
<correction node-fstream "Correction d'un problème d'écrasement de fichier arbitraire [CVE-2019-13173]">
<correction node-set-value "Correction de pollution de prototype [CVE-2019-10747]">
<correction node-yarnpkg "Utilisation imposée de HTTPS pour les registres normaux">
<correction nx-libs "Correction de régressions introduites dans l'envoi précédent, affectant x2go">
<correction open-vm-tools "Correction de fuites de mémoire et de gestion d'erreur">
<correction openvswitch "Mise à jour de debian/ifupdown.sh pour permettre la mise en place du MTU ; correction des dépendances à Python pour l'utilisation de Python 3">
<correction picard "Mise à jour des traductions pour corriger un plantage avec la « locale » espagnole">
<correction plasma-applet-redshift-control "Correction du mode manuel lors de son utilisation avec les versions de redshift supérieures à 1.12">
<correction postfix "Nouvelle version amont stable ; contournement des mauvaises performances de la boucle locale TCP">
<correction python-cryptography "Correction d'échec de la suite de tests lors de la construction avec les nouvelles versions d'OpenSSL ; correction d'une fuite de mémoire déclenchable lors de l'analyse d'extensions de certificat x509 comme AIA">
<correction python-flask-rdf "Ajout de Depends à python{3,}-rdflib">
<correction python-oslo.messaging "Nouvelle version amont stable ; correction de basculement de destination de connexion quand un nœud de grappe rabbitmq disparaît">
<correction python-werkzeug "Assurance que les conteneurs Docker ont des codes PIN de débogage uniques [CVE-2019-14806]">
<correction python2.7 "Correction de plusieurs problèmes de sécurité [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Correction de rpc.rquotad accaparant le processeur à 100 %">
<correction rpcbind "Activation permise d’appels distants au moment de l'exécution">
<correction shelldap "Réparation des authentifications SASL, ajout d'une option « sasluser »">
<correction sogo "Correction d'affichage de courriels avec signature PGP">
<correction spf-engine "Nouvelle version amont stable ; correction de la prise en charge de sysvinit">
<correction standardskriver "Correction de l'avertissement d'obsolescence par config.RawConfigParser ; utilisation de la commande externe <q>ip</q> au lieu de la commande <q>ifconfig</q> obsolète">
<correction swi-prolog "Utilisation de HTTPS lors du contact avec les serveurs de pack amont">
<correction systemd "core : aucune propagation d'un échec de rechargement au résultat du service ; correction des échecs de sync_file_range dans les conteneurs nspawn sur arm et ppc ; correction de RootDirectory ne fonctionnant pas lors de son utilisation en association avec User ; assurance que les contrôles d'accès sur l'interface D-Bus de systemd-resolved sont appliqués correctement [CVE-2019-15718] ; correction de StopWhenUnneeded=true pour les unités (« units ») de montage ; MountFlags=shared remis en fonction">
<correction tmpreaper "Casse évitée des services de systemd qui utilisent PrivateTmp=true">
<correction trapperkeeper-webserver-jetty9-clojure "Rétablissement de la compatibilité de SSL avec les nouvelles versions de Jetty">
<correction tzdata "Nouvelle version amont">
<correction ublock-origin "Nouvelle version amont, compatible avec Firefox ESR68">
<correction uim "Relance de libuim-data comme paquet de transition, corrigeant certains problèmes après les mises à niveau vers Buster">
<correction vanguards "Nouvelle version amont stable ; rechargement évité de la configuration de tor au moyen de SIGHUP provoquant un déni de service pour les protections de vanguards">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction firefox-esr "[armel] prise en charge impossible à cause d'une dépendance de construction à nodejs">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

