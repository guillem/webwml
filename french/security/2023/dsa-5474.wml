#use wml::debian::translation-check translation="1e2e8f73c9a0918a76ace7b9844fe4ba20b68e48" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour fournit le microcode mis à jour du processeur pour
certains types de processeurs Intel ainsi que des mitigations pour des
vulnérabilités de sécurité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40982">CVE-2022-40982</a>

<p>Daniel Moghimi a découvert la faille « Gather Data Sampling » (GDS),
une vulnérabilité matérielle qui permettait un accès spéculatif non
privilégié à des données qui étaient précédemment stockées dans les
registres de vecteurs.</p>

<p>Pour plus de détails, veuillez vous référer aux pages
<a href="https://downfall.page">/https://downfall.page</a> et
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html">\
https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html></a></p>.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41804">CVE-2022-41804</a>

<p>Une injection d'erreur non autorisée dans Intel SGX ou Intel TDX pour
certains processeurs Xeon d'Intel pouvait permettre éventuellement une
élévation de privilèges à un utilisateur local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23908">CVE-2023-23908</a>

<p>Un contrôle d'accès incorrect dans certains processeurs Intel Xeon
Scalable de troisième génération pouvait avoir pour conséquence une fuite
d'informations.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 3.20230808.1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 3.20230808.1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5474.data"
# $Id: $
