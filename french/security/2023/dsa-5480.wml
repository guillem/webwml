#use wml::debian::translation-check translation="8656fedad22d0e3d8296e401539d6cb985ee6285" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4269">CVE-2022-4269</a>

<p>William Zhao a découvert qu'un défaut dans le sous-système Traffic
Control (TC) sous-système lors de l'utilisation d'une configuration réseau
particulière (redirection des paquets « egress » vers « ingress » avec
l'action <q>mirred</q>) de TC, pouvait permettre à un utilisateur local non
privilégié de provoquer un déni de service (déclenchant un verrouillage
logiciel du processeur).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39189">CVE-2022-39189</a>

<p>Jann Horn a découvert que les opérations de vidage étaient mal gérées
dans le sous-système KVM dans certaines situations KVM_VCPU_PREEMPTED, ce
qui pouvait permettre à un utilisateur client non privilégié de
compromettre le noyau client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1206">CVE-2023-1206</a>

<p>La pile réseau permettait à des attaquants d'imposer des collisions de
hachage dans la table de recherche de connexions IPv6. Cela pouvait avoir
pour conséquence un déni de service (augmentation significative du coût des
recherches, utilisation accrue du processeur).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1380">CVE-2023-1380</a>

<p>Jisoo Jang a signalé une lecture de tas hors limites dans le pilote
Wi-Fi brcmfmac. Sur les systèmes qui utilisent ce pilote, un utilisateur
local pouvait exploiter cela pour lire des informations sensibles ou
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2002">CVE-2023-2002</a>

<p>Ruiahn Li a signalé une vérification incorrecte des permissions dans le
sous-système Bluetooth. Un utilisateur local pouvait exploiter cela pour
reconfigurer les interfaces locales Bluetooth, avec pour conséquences des
fuites d'informations, une usurpation ou un déni de service (perte de
connexion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2007">CVE-2023-2007</a>

<p>Lucas Leong et Reno Robert ont découvert un défaut
time-of-check-to-time-of-use dans le pilote de contrôleur SCSI dpt_i2o. Un
utilisateur local doté d'un accès à un périphérique SCSI utilisant ce
pilote pouvait exploiter cela pour une élévation de privilèges.</p>

<p>Ce défaut a été atténué en retirant la prise en charge de l'opération
I2OUSRCMD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2124">CVE-2023-2124</a>

<p>Kyle Zeng, Akshay Ajayan et Fish Wang ont découvert que l'absence de
validation de métadonnées pouvait avoir pour conséquences un déni de
service ou une potentielle élévation de privilèges lors du montage d'une
image disque XFS corrompue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2269">CVE-2023-2269</a>

<p>Zheng Zhang a signalé qu'un traitement incorrect du verrouillage dans
l'implémentation du mappage de périphérique pouvait avoir pour conséquence
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2898">CVE-2023-2898</a>

<p>Le manque de vérifications dans le système de fichiers f2fs pouvait
avoir pour conséquence un déni de service lors de l'accès à un système de
fichiers mal formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3090">CVE-2023-3090</a>

<p>L'absence d'initialisation dans le réseau ipvlan pouvait conduire à une
vulnérabilité d'écriture hors limites, avec pour conséquences un déni de
service ou éventuellement l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3111">CVE-2023-3111</a>

<p>L'outil TOTE Robot a découvert un défaut dans le pilote du système de
fichiers Btrfs qui pouvait conduire à une utilisation de mémoire après
libération. Il n'est pas certain qu'un utilisateur non privilégié puisse
exploiter cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3212">CVE-2023-3212</a>

<p>Yang Lan a découvert que l'absence de validation dans le système de
fichiers GFS2 pouvait avoir pour conséquence un déni de service à l'aide
d'un déréférencement de pointeur NULL lors du montage d'un système de
fichiers GFS2 mal formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3268">CVE-2023-3268</a>

<p>Un accès mémoire hors limites dans relayfs pouvait avoir pour
conséquences un déni de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3338">CVE-2023-3338</a>

<p>Davide Ornaghi a découvert un défaut dans l'implémentation du protocole
DECnet qui pouvait conduire à une déréférencement de pointeur NULL ou à
l'utilisation de mémoire après libération. Un utilisateur local pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) et probablement une élévation de privilèges.</p>

<p>Ce défaut a été atténué par le retrait de l'implémentation du protocole
DECnet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3389">CVE-2023-3389</a>

<p>Querijn Voet a découvert une utilisation de mémoire après libération
dans le sous-système io_uring. Cela pouvait avoir pour conséquence un déni
de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3611">CVE-2023-3611</a>

<p>Une écriture hors limites dans le sous-système de contrôle de trafic
pour l'ordonnanceur « Quick Fair Queueing » (QFQ) pouvait avoir pour
conséquences un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3609">CVE-2023-3609</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2023-3776">CVE-2023-3776</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2023-4128">CVE-2023-4128</a>

<p>Une utilisation de mémoire après libération dans les classificateurs
cls_fw, cls_u32, cls_route et réseau pouvait avoir pour conséquences un
déni de service ou une potentielle élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3863">CVE-2023-3863</a>

<p>Une utilisation de mémoire après libération dans l'implémentation de NFC
pouvait avoir pour conséquences un déni de service, une fuite
d'informations ou une potentielle élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4004">CVE-2023-4004</a>

<p>Une utilisation de mémoire après libération dans l'implémentation de
Netfilter de PIPAPO (PIle PAcket POlicies) pouvait avoir pour conséquence
un déni de service ou une potentielle élévation locale de privilèges pour
un utilisateur doté de la capacité CAP_NET_ADMIN dans n'importe quel espace
de noms utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4132">CVE-2023-4132</a>

<p>Une utilisation de mémoire après libération dans le pilote pour les
receveurs MDTV de Siano basés sur SMS1xxx pouvait avoir pour conséquence un
déni de service local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4147">CVE-2023-4147</a>

<p>Kevin Rich a découvert une utilisation de mémoire après libération dans
Netfilter lors de l'ajout d'une règle avec NFTA_RULE_CHAIN_ID, qui pouvait
avoir pour conséquence une élévation locale de privilèges pour un
utilisateur doté de la capacité CAP_NET_ADMIN dans n'importe quel espace de
noms utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4194">CVE-2023-4194</a>

<p>Une confusion de type dans l'implémentation des périphériques réseau
TUN/TAP pouvait permettre à un utilisateur local de contourner les filtres
de réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4273">CVE-2023-4273</a>

<p>Maxim Suhanov a découvert un dépassement de pile dans le pilote exFAT,
qui pouvait avoir pour conséquence un déni de service local à l'aide d'un
système de fichiers mal formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20588">CVE-2023-20588</a>

<p>Jana Hofmann, Emanuele Vannacci, Cedric Fournet, Boris Koepf et Oleksii
Oleksenko ont découvert que sur certains processeurs AMD avec la
microarchitecture Zen1, une division d'entier par zéro pouvait laisser des
données de quotient périmées d'une division précédente, avec pour
conséquence éventuelle une fuite de données sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-21255">CVE-2023-21255</a>

<p>Une utilisation de mémoire après libération a été découverte dans le
pilote de création de lien d'Android, qui pouvait avoir pour conséquence
une élévation locale de privilèges sur les systèmes où le pilote de
création de liens est chargé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-21400">CVE-2023-21400</a>

<p>Ye Zhang et Nicolas Wu ont découvert une double libération de zone de
mémoire dans le sous-système io_uring. Cela pouvait avoir pour conséquence
un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31084">CVE-2023-31084</a>

<p>Le pilote DVB Core ne gérait pas correctement le verrouillage de
certains événements, permettant à un utilisateur local de provoquer un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34319">CVE-2023-34319</a>

<p>Ross Lagerwall a découvert un dépassement de tampon dans le pilote
netback de Xen qui pouvait permettre à un client Xen de provoquer un déni
de service de l'hôte de la virtualisation en envoyant des paquets mal
formés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35788">CVE-2023-35788</a>

<p>Hangyu Hua a découvert qu'une vulnérabilité due à un décalage d'entier
dans le classificateur de trafic Flower pouvait avoir pour conséquences un
déni de service local ou l'exécution d'une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40283">CVE-2023-40283</a>

<p>Une utilisation de mémoire après libération a été découverte dans le
traitement du socket L2CAP de Bluetooth.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 5.10.191-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5480.data"
# $Id: $
