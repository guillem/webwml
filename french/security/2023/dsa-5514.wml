#use wml::debian::translation-check translation="d18c1e0d69a1eaf6149002ae4e03d264efff5d48" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Qualys Research Labs a découvert un dépassement de tampon dans le
traitement du chargeur dynamique de la variable d'environnement
GLIBC_TUNABLES. Un attaquant peut exploiter ce défaut pour une élévation de
privilèges.</p>

<p>Vous trouverez plus de détails dans l'annonce de Qualys à l'adresse
<a href="https://www.qualys.com/2023/10/03/cve-2023-4911/looney-tunables-local-privilege-escalation-glibc-ld-so.txt">\
https://www.qualys.com/2023/10/03/cve-2023-4911/looney-tunables-local-privilege-escalation-glibc-ld-so.txt</a>.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 2.31-13+deb11u7.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 2.36-9+deb12u3. Cette mise à jour inclut des corrections pour
le <a href="https://security-tracker.debian.org/tracker/CVE-2023-4527">CVE-2023-4527</a> et
le <a href="https://security-tracker.debian.org/tracker/CVE-2023-4806">CVE-2023-4806</a>
prévues à l'origine pour la prochaine version intermédiaire de Bookworm .</p>

<p>Nous vous recommandons de mettre à jour vos paquets glibc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de glibc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/glibc">\
https://security-tracker.debian.org/tracker/glibc</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5514.data"
# $Id: $
