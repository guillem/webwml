#use wml::debian::translation-check translation="db3c16a066355af630de96e011541fd523a7b343" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Subversion, un système
de gestion de versions.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28544">CVE-2021-28544</a>

<p>Evgeny Kotkov a signalé que les serveurs Subversion révèlent des chemins
<q>copyfrom</q> qui devraient être dissimulés selon les règles
d'autorisation basées sur le chemin (authz) configurées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24070">CVE-2022-24070</a>

<p>Thomas Weissschuh a signalé que mod_dav_svn de Subversion est prédisposé
à une vulnérabilité d'utilisation de mémoire après libération lors de la
vérification de règles d'autorisation basées sur le chemin. Cela peut avoir
pour conséquence un déni de service (plantage du « worker » HTTPD traitant
la requête).</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 1.10.4-1+deb10u3.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.14.1-3+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets subversion.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de subversion, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/subversion">\
https://security-tracker.debian.org/tracker/subversion</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5119.data"
# $Id: $
