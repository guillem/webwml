#use wml::debian::translation-check translation="57142714136b016072b02516b529006538983a39" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36310">CVE-2020-36310</a>

<p>Un défaut a été découvert dans l'implémentation de KVM pour les
processeurs AMD, qui pouvait conduire à une boucle infinie. Un client de VM
malveillant pouvait exploiter cela pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a> (INTEL-SA-00598)

<p>Des chercheurs de VUSec ont découvert que le tampon « Branch History »
des processeurs Intel pouvait être exploité pour créer des attaques par canal
auxiliaire d'information avec une exécution spéculative. Ce problème est
semblable à Spectre variante 2, mais demande des palliatifs supplémentaires
sur certains processeurs.</p>

<p>Cela pouvait être exploité pour obtenir des informations sensibles à
partir d'un contexte de sécurité différent, comme à partir de l'espace
utilisateur vers le noyau ou à partir d'un client KVM vers le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a> (INTEL-SA-00598)

<p>Il s'agit d'un problème similaire à <a
href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>,
mais recouvre une exploitation à l'intérieur d'un contexte de sécurité,
comme à partir de code compilé avec JIT dans un bac à sable vers du code de
l'hôte dans le même processus.</p>

<p>Ce problème est partiellement pallié en désactivant eBPF pour les
utilisateurs non privilégiés avec l'option sysctl :
kernel.unprivileged_bpf_disabled=2. C'est déjà l'option par défaut dans
Debian 11 <q>Bullseye</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

<p>Une utilisation de mémoire après libération a été découverte dans le
pilote de prise en charge du contrôleur hôte MOXART SD/MMC. Ce défaut
n'impactait pas les paquets binaires de Debian dans la mesure où
CONFIG_MMC_MOXART n'est pas configuré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

<p>Yiqi Sun et Kevin Wang ont signalé que le sous-système cgroup-v1 ne
restreignait pas correctement l'accès à la fonction « release-agent ». Un
utilisateur local pouvait tirer avantage de ce défaut pour une élévation de
privilèges et le contournement de l'isolation d'espace de noms.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

<p>butt3rflyh4ck a découvert un déréférencement de pointeur NULL dans le
système de fichiers UDF. Un utilisateur local qui peut monter une image UDF
contrefaite pour l'occasion peut utiliser ce défaut pour planter le
système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25636">CVE-2022-25636</a>

<p>Nick Gregory a signalé un défaut d'écriture de tas hors limites dans le
sous-système netfilter. Un utilisateur doté de la capacité CAP_NET_ADMIN
pouvait utiliser cela pour un déni de service ou éventuellement pour une
élévation de privilèges.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.103-1. Cette mise à jour comprend en plus beaucoup
plus de corrections de bogue issues des mises à jour de stable, de la
version 5.10.93 à la version 5.10.103 incluse.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5095.data"
# $Id: $
