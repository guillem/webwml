#use wml::debian::template title="How to use the Debian Search Engine" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>
The Debian project offers its own search engine at <a
href="https://search.debian.org/">https://search.debian.org/</a>.
Here are some tips on how to use it and start simple searches as well as
more complex searches with Boolean operators.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Simple Search</h3>
      <p>The simplest way to use the engine is to enter a single word in the search field and hit [Enter]. Alternatively, you can click the <em>Search</em> button. The search engine will then return all pages on our website which contain that word. For most searches, this should give you good results.</p>
      <p>Alternatively, you can search for more than one word. Again, you should see all pages of the Debian website which contain all words you entered. To search for phrases, put them in quotation marks ("). Please note that the search engine is not case-sensitive, so searching for <code>gcc</code> matches "gcc" as well as "GCC".</p> 
      <p>Below the search field you can decide how many results per page you'd like to see. It's also possible to choose a different language; the Debian website search supports almost 40 different languages.</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Boolean Search</h3>
      <p>If a simple search is not sufficient, you can use Boolean search operators. You can choose between <em>AND</em>, <em>OR</em>, and <em>NOT</em> or combine all three. Please make sure to use capital letters for all operators, so that the search engine recognizes them.</p>

      <ul>
        <li><b>AND</b> combines two expressions and returns pages which contain both words. For example, <code>gcc AND patch</code> finds all pages that contain both, "gcc" and "patch". In this case you will get the same results as when searching for <code>gcc patch</code>, but an explicit <code>AND</code> can be useful in combination with other operators.</li>
        <li><b>OR</b> returns results if either word is in the page. <code>gcc OR patch</code> finds any page which contains either "gcc" or "patch".</li>
        <li><b>NOT</b> is used to exclude search terms from the results. For example, <code>gcc NOT patch</code> finds all which contain "gcc", but not "patch". <code>gcc AND NOT patch</code> gives you the same results, but searching for <code>NOT patch</code> is not supported.</li>
        <li><b>(...)</b> can be used to group expressions. For example, <code>(gcc OR make) NOT patch</code> will find all pages which contain either "gcc" or "make", but do not contain "patch".</li>
      </ul>
    </div>
  </div>
</div>
