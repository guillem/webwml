#use wml::debian::projectnews::header PUBDATE="2023-08-05" SUMMARY="Welcome to the inaugural issue of Debian Project Bits!, Debian Day, DebConf23, News on Debian releases, Popular packages, Calls for help"
#use wml::debian::acronyms

# Status: [frozen]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<h2>Welcome to the inaugural issue of Debian Project Bits!</h2>

<shortintro issue="first"/>

<p>Those remembering the Debian Weekly News (DwN) will recognize some of the
sections here which served as our inspiration.</p>

<p>Debian Project Bits posts will allow for a faster turnaround of some project
news on a monthly basis. The <a href="https://micronews.debian.org/">Debian Micronews</a>
service will continue to share shorter news items, the
<a href="https://www.debian.org/News/weekly/">Debian Project News</a>
remains as our official newsletter which may move to a biannual archive format.</p>

<h2>News</h2>

<p><b>Debian Day</b></p>

<p>The Debian Project was <a href="https://wiki.debian.org/DebianHistory">officially founded</a>
by Ian Murdock on August 16, 1993. Since then we have celebrated our Anniversary
of that date each year with events around the world. We would love it if you
could join our revels this very special year as we have the honor of turning
<b>30</b>!</p>

<p>Attend or organize a local <a href="https://wiki.debian.org/DebianDay">Debian Day</a>
celebration. You are invited to plan your own event: from Bug Squashing parties
to Key Signing parties, Meet-Ups, or any type of social event whether large or
small. And be sure to check our
<a href="https://wiki.debian.org/Teams/DPL/Reimbursement">Debian reimbursement How To</a>
if you need such resources.</p>

<p>You can share your days, events, thoughts, or notes with us and the rest of
the community with the <b>#debianday</b> tag that will be used across most
<a href="https://wiki.debian.org/Teams/DebianSocial">social media platforms</a>.
See you then!</p>

<h2>Events: Upcoming and Reports</h2>

<p><b>Upcoming</b></p>

<p><b>Debian 30 anos</b></p>

<p>The <a href="https://debianbrasil.org.br/">Debian Brasil Community</a> is
organizing the event
<a href="https://debianbrasil.gitlab.io/debian30anos/">Debian 30 anos</a> to
celebrate the 30th anniversary of the Debian Project.</p>

<p>From August 14 to 18, between 7pm and 22pm (UTC-3) contributors will talk
online in Portuguese and we will live stream on
<a href="https://www.youtube.com/DebianBrasilOficial">Debian Brasil YouTube channel</a>.</p>

<p><b>DebConf23: Debian Developers Camp and Conference</b></p>

<p>The 2023 Debian Developers Camp \(DebCamp) and Conference
(<a href="https://debconf23.debconf.org/">DebConf23</a>) will be hosted this
year in Infopark, <a href="https://debconf23.debconf.org/about/kochi/">Kochi, India</a>.
DebCamp is slated to run from September 3 through 9, immediately followed by
the larger DebConf, September 10 through 17.</p>

<p>If you are planning on attending the conference this year, now is the time to
ensure your travel documentation,
<a href="https://lists.debian.org/debconf-announce/2023/07/msg00001.html">visa information</a>,
bursary submissions, papers and relevant equipment are prepared. For more
information contact: &lt;debconf@debconf.org&gt;.</p>

<p><b>MiniDebConf Cambridge 2023</b></p>

<p>There will be a 
<a href="https://lists.debian.org/debian-devel-announce/2023/07/msg00002.html">MiniDebConf</a>
held in Cambridge, UK, hosted by ARM for 4 days in November: 2 days for a
mini-DebCamp (Thu 23 - Fri 24), with space for dedicated development / sprint /
team meetings, then two days for a more regular MiniDebConf (Sat 25 - Sun 26)
with space for more general talks, up to 80 people.</p>

<p><b>Reports</b></p>

<p>During the last months, the Debian Community has organized some
<a href="https://wiki.debian.org/BSP">Bug Squashing Parties</a>:</p>
<ul>
<li><a href="https://wiki.debian.org/BSP/2022/11/nl/Tilburg">Tilburg</a>, Netherlands. October 2022.</li>
<li><a href="https://wiki.debian.org/BSP/2023/01/ch/St-Cergue">St-Cergue</a>, Switzerland. January 2023</li>
<li><a href="https://wiki.debian.org/BSP/2023/02/ca/Montreal">Montreal</a>, Canada. February 2023</li>
</ul>

<p>In January, Debian India hosted the
<a href="https://wiki.debian.org/DebianIndia/MiniDebConfTamilNadu2023">MiniDebConf Tamil Nadu</a>
in Viluppuram, Tamil Nadu, India (Sat 28 - Sun 26). The following month, the
<a href="https://wiki.debian.org/DebianEvents/pt/2023/MiniDebConfLisbon">MiniDebConf Portugal 2023</a>
was held in Lisbon (12 - 16 February 2023).</p>

<p>These events, seen as a <b>stunning success</b> by some of their attendees,
demonstrate the vitality of our community.</p>

<p><b>Debian Brasil Community at Campus Party Brazil 2023</b></p>

<p>Another edition of <a href="https://brasil.campus-party.org/cpbr15/">Campus Party Brazil</a>
took place in the city of São Paulo between July 25th and 30th. And one more
time the Debian Brazil Community was present. During the days in the available
space, we carry out some activities such as:</p>
<ul>
<li>Gifts for attendees (stickers, cups, lanyards);</li>
<li>Workshop on how to contribute to the translation team;</li>
<li>Workshop on packaging;</li>
<li>Key signing party;</li>
<li>Information about the project.</li>
</ul>

<p>For more info and a few photos, check out the
<a href="https://debianbrasil.org.br/blog/debian-brasil-campusparty-sp-2023-report/">organizers report</a>.</p>

<p><b>MiniDebConf Brasília 2023</b></p>

<p>From May 25 to 27, Brasília hosted the
<a href="https://brasilia.mini.debconf.org">MiniDebConf Brasília 2023</a>.
This gathering was composed of various activities such as talks, workshops,
sprints, BSPs (Bug Squashing Party), key signings, social events, and hacking,
aimed to bring the community together and celebrate the world's largest Free
Software project: Debian.</p>

<p>For more information please see the
<a href="https://debianbrasil.org.br/blog/minidebconf-brasilia-2023-a-brief-report/">full report</a>
written by the organizers.</p>

<p><b>Debian Reunion Hamburg 2023</b></p>

<p>This year the annual
<a href="https://wiki.debian.org/DebianEvents/de/2023/DebianReunionHamburg">Debian Reunion Hamburg</a>
was held from Tuesday 23 to 30 May starting with four days of
hacking followed by two days of talks, and then two more days of hacking. As
usual, people - more than forty-five attendees from Germany, Czechia, France,
Slovakia, and Switzerland - were happy to meet in person, to hack and chat
together, and much more. If you missed the live streams, the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/Debian-Reunion-Hamburg/">video recordings</a>
are available.</p>

<p><b>Translation workshops from the pt_BR team</b></p>

<p>The Brazilian translation team, debian-l10n-portuguese, had their first workshop
of 2023 in February with great results. The workshop was aimed at beginners,
working in <a href="https://ddtp.debian.org/ddtss">DDTP/DDTSS</a>.</p>

<p>For more information please see the
<a href="https://debianbrasil.org.br/blog/first-2023-translation-workshop-from-the-pt-BR-team/">full report</a>
written by the organizers.</p>

<p>And on June 13 another workshop took place to translate
<a href="https://debian-handbook.info">The Debian Administrator's Handbook</a>. The main
goal was to show beginners how to collaborate in the translation of this
important material, which has existed since 2004. The manual's translations
are hosted on
<a href="https://hosted.weblate.org/projects/debian-handbook/#languages">Weblate</a>.</p>


<h2>Releases</h2>

<p><b>Stable Release</b></p>

<p>Debian 12 <a href="https://wiki.debian.org/DebianBookworm">bookworm</a> was released on
<a href="https://www.debian.org/News/2023/20230610">June 10, 2023</a>. This new version
becomes the stable release of Debian and moves the prior Debian 11
<a href="https://wiki.debian.org/DebianBullseye">bullseye</a> release to
<a href="https://wiki.debian.org/DebianOldStable">oldstable status</a>. The Debian
community celebrated the release with 23
<a href="https://wiki.debian.org/ReleasePartyBookworm">Release Parties</a> all around the
world.</p>

<p>Bookworm's first point release <a href="https://wiki.debian.org/DebianReleases">12.1</a>
address miscellaneous bug fixes affecting 88 packages, documentation, and
installer updates was made available on
<a href="https://www.debian.org/News/2023/20230722">July 22, 2023</a>.</p>

<p><b>RISC-V support</b></p>
<p><a href="https://wiki.debian.org/RISC-V">riscv64</a> has recently been added to the
official Debian architectures for support of 64-bit little-endian
<a href="https://riscv.org">RISC-V</a> hardware running the Linux kernel. We expect
to have full riscv64 support in Debian 13 trixie. Updates on bootstrap, build
daemon, porterbox, and development progress were recently shared by the team in a 
<a href="https://lists.debian.org/debian-devel-announce/2023/07/msg00003.html">Bits from the Debian riscv64 porters</a>
post.</p>

<p><b>non-free-firmware</b></p>
<p>The Debian 12 bookworm archive now includes non-free-firmware; please be
sure to update your apt sources.list if your systems requires such components
for operation. If your previous sources.list included non-free for this
purpose it may safely be removed.</p>

<p><b>apt sources.list</b></p>
<p>The Debian archive holds several components:</p>
<ul>
<li><a href="http://www.debian.org/doc/debian-policy/ch-archive#s-main">main</a>: Contains
<a href="https://www.debian.org/social_contract#guidelines">DFSG</a>-compliant packages,
which do not rely on software outside this area to operate.</li>
<li><a href="http://www.debian.org/doc/debian-policy/ch-archive#s-contrib">contrib</a>:
Contains packages that contain DFSG-compliant software, but have dependencies
not in main.</li>
<li><a href="http://www.debian.org/doc/debian-policy/ch-archive#s-non-free">non-free</a>:
Contains software that does not comply with the DFSG.</li>
<li>non-free-firmware: Firmware that is otherwise not part of the Debian system
to enable use of Debian with hardware that requires such firmware.</li>
</ul>

<p>Example of the sources.list file</p>

<code>deb http://deb.debian.org/debian bookworm main</br>
deb-src http://deb.debian.org/debian bookworm main</code>

<code>deb http://deb.debian.org/debian-security/ bookworm-security main</br>
deb-src http://deb.debian.org/debian-security/ bookworm-security main</code>

<code>deb http://deb.debian.org/debian bookworm-updates main</br>
deb-src http://deb.debian.org/debian bookworm-updates main</code>

<p>Example using the components:</p>

<code>deb http://deb.debian.org/debian bookworm main non-free-firmware</br>
deb-src http://deb.debian.org/debian bookworm main non-free-firmware</code>

<code>deb http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware</br>
deb-src http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware</code>
    
<code>deb http://deb.debian.org/debian bookworm-updates main non-free-firmware</br >
deb-src http://deb.debian.org/debian bookworm-updates main non-free-firmware</code>

<p>For more information and guidelines on proper configuration of the apt
source.list file please see the
<a href="https://wiki.debian.org/SourcesList">Configuring Apt Sources - Wiki</a> page.</p>


<h2>Inside Debian</h2>

<p><b>New Debian Members</b></p>


<p>Please welcome the following newest Debian Project Members:</p>

<ul>
<li>Marius Gripsgard \(mariogrip)</li>
<li>Mohammed Bilal \(rmb)</li>
<li>Emmanuel Arias \(amanu)</li>
<li>Robin Gustafsson \(rgson)</li>
<li>Lukas Märdian \(slyon)</li>
<li>David da Silva Polverari \(polverari)</li>
</ul>

<p>To find out more about our newest members or any Debian Developer, look
for them on the <a href="https://nm.debian.org/public/people/">Debian People list</a>.</p>


<toc-add-entry name="security">Security Advisories</toc-add-entry>


<p>Debian's Security Team releases current advisories on a daily basis.
Some recently released advisories concern these packages:</p>

<ul>
<li><a href="https://www.debian.org/security/2023/dsa-5435-2">trafficserver</a>
Several vulnerabilities were discovered in Apache Traffic Server, a
reverse and forward proxy server, which could result in information
disclosure or denial of service.</li>

<li><a href="https://www.debian.org/security/2023/dsa-5438">asterisk</a>
A flaw was found in Asterisk, an Open Source Private Branch Exchange. A
buffer overflow vulnerability affects users that use PJSIP DNS resolver.
This vulnerability is related to CVE-2022-24793. The difference is that
this issue is in parsing the query record `parse_query()`, while the issue
in CVE-2022-24793 is in `parse_rr()`. A workaround is to disable DNS
resolution in PJSIP config (by setting `nameserver_count` to zero) or use
an external resolver implementation instead.</li>

<li><a href="https://www.debian.org/security/2023/dsa-5442">flask</a>
It was discovered that in some conditions the Flask web framework may
disclose a session cookie.</li>

<li><a href="https://www.debian.org/security/2023/dsa-5456">chromium</a>
Multiple security issues were discovered in Chromium, which could result
in the execution of arbitrary code, denial of service or information
disclosure.</li>
</ul>

<h2>Other</h2>

<p><b>Popular packages</b></p>

<ul>
<li><a href="https://packages.debian.org/stable/gpgv">gpgv - GNU privacy guard
signature verification tool</a>. 99,053 installations. gpgv is actually a
stripped-down version of gpg which is only able to check signatures. It is
somewhat smaller than the fully-blown gpg and uses a different (and simpler)
way to check that the public keys used to make the signature are valid. There
are no configuration files and only a few options are implemented.</li>

<li><a href="https://packages.debian.org/stable/dmsetup">dmsetup - Linux Kernel
Device Mapper userspace library</a>. 77,769 installations. The Linux Kernel
Device Mapper is the LVM (Linux Logical Volume Management) Team's implementation
of a minimalistic kernel-space driver that handles volume management, while
keeping knowledge of the underlying device layout in user-space. This makes it
useful for not only LVM, but software raid, and other drivers that create
<q>virtual</q> block devices.</li>

<li><a href="https://packages.debian.org/stable/sensible-utils">sensible-utils - Utilities
for sensible alternative selection</a>. 96,001 daily users. This package
provides a number of small utilities which are used by programs to sensibly
select and spawn an appropriate browser, editor, or pager. The specific
utilities included are: sensible-browser sensible-editor sensible-pager.</li>

<li><a href="https://packages.debian.org/stable/popularity-contest">popularity-contest -
The popularity-contest package</a>. 90,758 daily users. The popularity-contest
package sets up a cron job that will periodically anonymously submit to the
Debian developers statistics about the most used Debian packages on the system.
This information helps Debian make decisions such as which packages should go on
the first CD. It also lets Debian improve future versions of the distribution so
that the most popular packages are the ones which are installed automatically
for new users.</li>
</ul>


<p><b>New and noteworthy packages in unstable</b></p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/libsimgrid3.34">Toolkit
for scalable simulation of distributed applications</a> SimGrid is a toolkit
that provides core functionalities for the simulation of distributed
applications in heterogeneous distributed environments. SimGrid can be used as
a Grid simulator, a P2P simulator, a Cloud simulator, a MPI simulator, or a
mix of all of them. The typical use-cases of SimGrid include heuristic
evaluation, application prototyping, and real application development and
tuning. This package contains the dynamic libraries and runtime.</li>

<li><a href="https://packages.debian.org/unstable/main/ldraw-mklist">LDraw mklist program</a>
3D CAD programs and rendering programs using the LDraw parts library of LEGO
parts rely on a file called parts.lst containing a list of all available parts.
The program ldraw-mklist is used to generate this list from a directory of
LDraw parts.</li>

<li><a href="https://packages.debian.org/unstable/main/ola-rdm-tests">Open
Lighting Architecture - RDM Responder Tests</a> The DMX512 standard for Digital
MultipleX is used for digital communication networks commonly used to control
stage lighting and effects. The Remote Device Management protocol is an
extension to DMX512, allowing bi-directional communication between RDM-compliant
devices without disturbing other devices on the same connection. The Open
Lighting Architecture (OLA) provides a plugin framework for distributing DMX512
control signals. The ola-rdm-tests package provides an automated way to check
protocol compliance in RDM devices.</li>

<li><a href="https://packages.debian.org/unstable/main/parsec-service">parsec-service</a>
Parsec is an abstraction layer that can be used to interact with hardware-backed
security facilities such as the Hardware Security Module (HSM), the Trusted
Platform Module (TPM), as well as firmware-backed and isolated software
services. The core component of Parsec is the security service, provided by this
package. The service is a background process that runs on the host platform and
provides connectivity with the secure facilities of that host, exposing a
platform-neutral API that can be consumed into different programming languages
using a client library. For a client library implemented in Rust see the package
librust-parsec-interface-dev.</li>

<li><a href="https://packages.debian.org/unstable/main/ripcalc">Simple network calculator and lookup tool</a>
Process and lookup network addresses from the command line or CSV with ripalc.
Output has a variety of customisable formats.</li>

<li><a href="https://packages.debian.org/unstable/main/xmrig">High performance,
open source CPU/GPU miner and RandomX benchmark</a>
XMRig is a high performance, open source, cross platform RandomX, KawPow,
CryptoNight, and GhostRider unified CPU/GPU miner and RandomX benchmark.</li>

<li><a href="https://packages.debian.org/unstable/main/librust-gping-dev">Ping,
but with a graph - Rust source code</a> This package contains the source for the
Rust gping crate, packaged by debcargo for use with cargo and dh-cargo.</li>
</ul>


<p><b>Once upon a time in Debian:</b></p>

<ul>
<li>2014-07-31 The Technical committee choose <a href="https://lists.debian.org/debian-devel-announce/2014/08/msg00000.html">libjpeg-turbo as the default JPEG decoder.</a></li>

<li>2010-08-01 <a href="https://debconf10.debconf.org/">DebConf10 starts à New York City, USA.</a></li>

<li>2007-08-05 
<a href="https://www.debian.org/vote/2007/vote_003/">Debian Maintainers approved by vote.</a></li>

<li>2009-08-05 Jeff Chimene files 
<a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=540000/">bug #540000 against live-initramfs.</a></li>
</ul>


<h2>Calls for help</h2>

<p><b>The Publicity team calls for volunteers and help!</b></p>

<p>Your Publicity team is asking for help from you our readers, developers, and
interested parties to contribute to the Debian news effort. We implore you to
submit items that may be of interest to our community and also ask for your
assistance with translations of the news into (your!) other languages along
with the needed second or third set of eyes to assist in editing our work
before publishing. If you can share a small amount of your time to aid our
team which strives to keep all of us informed, we need you. Please see the
<a href="https://wiki.debian.org/ProjectNews/HowToContribute">contributing page</a>
to find out how to help and reach out to us via IRC on
<a href="irc://irc.debian.org/debian-publicity/">#debian-publicity</a>
on <a href="https://oftc.net/">OFTC.net</a>, or our
<a href="mailto:debian-publicity@lists.debian.org">public mailing list</a>,
or via email at <a href="mailto:press@debian.org">press@debian.org</a> for
sensitive or private inquiries.</p>

<toc-add-entry name="continuedpb">Want to continue reading DPB?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Subscribe or Unsubscribe</a> from the Debian News mailing list.</p>

#use wml::debian::projectnews::footer editor="The Publicity Team with contributions from Jean-Pierre Giraud, Joost van Baal-Ilić, Carlos Henrique Lima Melara, Donald Norwood, Paulo Henrique de Lima Santana"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
