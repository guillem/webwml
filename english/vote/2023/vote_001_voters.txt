-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3           aboll	Andreas Boll
    4          absurd	Stephan Suerken
    5           adamm	Adam Majer
    6       adrianorg	Adriano Rafael Gomes
    7            adsb	Adam D. Barratt
    8       aferraris	Arnaud Ferraris
    9             agx	Guido Guenther
   10          akumar	Kumar Appaiah
   11           alexm	Alex Muntada
   12           alexp	Alex Pennace
   13            alfs	Stefan Alfredsson
   14        alteholz	Thorsten Alteholz
   15        amacater	Andrew Martin Adrian Cater
   16        ametzler	Andreas Metzler
   17             ana	Ana Beatriz Guerrero López
   18         anarcat	Antoine Beaupré
   19            anbe	Andreas Beckmann
   20            andi	Andreas B. Mundt
   21           angel	Angel Abad
   22          ansgar	Ansgar
   23           anupa	Anupa Ann Joseph
   24        anuradha	Anuradha Weeraman
   25         aurel32	Aurelien Jarno
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26        azekulic	Alen Zekulic
   27        ballombe	Bill Allombert
   28           bartm	Bart Martens
   29           bayle	Christian Bayle
   30          bbaren	Benjamin Barenblat
   31           bdale	Bdale Garbee
   32            benh	Ben Hutchings
   33          bernat	Vincent Bernat
   34            beuc	Sylvain Beucler
   35           biebl	Michael Biebl
   36         bigeasy	Sebastian Andrzej Siewior
   37           bluca	Luca Boccassi
   38           bootc	Chris Boot
   39          boutil	Cédric Boutillier
   40            bzed	Bernd Zeimetz
   41        calculus	Jerome Georges Benoit
   42          carnil	Salvatore Bonaccorso
   43           cavok	Domenico Andreoli
   44          chrism	Christoph Martin
   45        cjwatson	Colin Watson
   46             ckk	Christian Kastner
   47           clint	Clint Adams
   48             cts	Christian T. Steigies
   49           cwryu	Changwoo Ryu
   50          czchen	ChangZhuo Chen
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51             dai	Daisuke Higuchi
   52            dank	Nick Loren Black
   53            dave	Dave Holland
   54         debacle	Wolfgang Borgert
   55       debalance	Philipp Huebner
   56        deltaone	Patrick Franz
   57          dererk	Ulises Vitulli
   58          dkogan	Dima Kogan
   59       dktrkranz	Luca Falavigna
   60          dlange	Daniel Lange
   61           dlehn	David I. Lehn
   62             dmn	Damyan Ivanov
   63             dod	Dominique Dumont
   64         dogsleg	Lev Lamberov
   65         donkult	David Kalnischkies
   66       dtorrance	Douglas Andrew Torrance
   67          ebourg	Emmanuel Bourg
   68          eevans	Eric Evans
   69          elbrus	Paul Mathijs Gevers
   70        emollier	Étienne Mollier
   71          enrico	Enrico Zini
   72        eriberto	Joao Eriberto Mota Filho
   73            eric	Eric Dorland
   74           eriks	Erik Schanze
   75           fabbe	Fabian Fagerholm
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76           felix	Félix Sipma
   77          fgeyer	Felix Geyer
   78         florian	Florian Ernst
   79         fpeters	Frederic Peters
   80        francois	Francois Marier
   81         frankie	Francesco Lovergine
   82           fuddl	Bruno Kleinert
   83         gabriel	Gabriel F. T. Gomes
   84        georgesk	Georges Khaznadar
   85          gibmat	Mathias Arthur Gibbens
   86             gio	Giovanni Mascellani
   87        glaubitz	John Paul Adrian Glaubitz
   88          glondu	Stéphane Glondu
   89          gniibe	NIIBE Yutaka
   90           gotom	Masanori Goto
   91          gregoa	Gregor Herrmann
   92            gspr	Gard Spreemann
   93         guilhem	Guilhem Moulin
   94         guillem	Guillem Jover
   95          gusnan	Andreas Rönnquist
   96            guus	Guus Sliepen
   97           gwolf	Gunnar Wolf
   98            haas	Christoph Haas
   99           halls	Chris Halls
  100        hartmans	Sam Hartman
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101           hefee	Sandro Knauß
  102         helmutg	Helmut Grohne
  103         hertzog	Raphaël Hertzog
  104      hlieberman	Harlan Lieberman-Berg
  105             hmc	Hugh McMaster
  106         hoexter	Sven Hoexter
  107          holger	Holger Levsen
  108      hvhaugwitz	Hannes von Haugwitz
  109            ianw	Ian Wienand
  110             ijc	Ian James Campbell
  111        iliastsi	Ilias Tsitsimpis
  112          iustin	Iustin Pop
  113        iwamatsu	Nobuhiro Iwamatsu
  114         jaldhar	Jaldhar H. Vyas
  115        jamessan	James McCoy
  116           jandd	Jan Dittberner
  117          jaqque	John Robinson
  118          jbicha	Jeremy Bicha
  119            jcfp	Jeroen Ploemen
  120          jlines	John Lines
  121             jmm	Moritz Muehlenhoff
  122         joostvb	Joost van Baal
  123           jordi	Jordi Mallach
  124           josue	Josué Ortega
  125          jpuydt	Julien Puydt
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126          jrtc27	Jessica Clarke
  127        jspricke	Jochen Sprickerhof
  128       jvalleroy	James Valleroy
  129          kaliko	Geoffroy Berret
  130          kartik	Kartik Mistry
  131          keithp	Keith Packard
  132          kenhys	HAYASHI Kentaro
  133            kibi	Cyril Brulebois
  134        kilobyte	Adam Borowski
  135            knok	Takatsugu Nokubi
  136           kobla	Ondřej Kobližek
  137      kritzefitz	Sven Bartscher
  138            kula	Marcin Kulisz
  139           lamby	Chris Lamb
  140           laney	Iain Lane
  141           lange	Thomas Lange
  142         larjona	Laura Arjona Reina
  143        lavamind	Jerome Charaoui
  144     leatherface	Julien Patriarca
  145         lechner	Felix Lechner
  146         lenharo	Daniel Lenharo de Souza
  147             leo	Carsten Leonhardt
  148        lfaraone	Luke Faraone
  149        lightsey	John Lightsey
  150        lisandro	Lisandro Damián Nicanor Pérez Meyer
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151          lkajan	Laszlo Kajan
  152            lool	Loïc Minier
  153           lucas	Lucas Nussbaum
  154         lyknode	Baptiste Beauplat
  155           malat	Mathieu Malaterre
  156         matthew	Matthew Vernon
  157     matthieucan	Matthieu Caneill
  158          mattia	Mattia Rizzolo
  159            maxy	Maximiliano Curia
  160         mbehrle	Mathias Behrle
  161              md	Marco d'Itri
  162            mejo	Jonas Meurer
  163          merker	Karsten Merker
  164          merkys	Andrius Merkys
  165          meskes	Michael Meskes
  166           metal	Marcelo Jorge Vieira
  167             mfv	Matteo F. Vescovi
  168             mhy	Mark Hymers
  169           micha	Micha Lenk
  170             mih	Michael Hanke
  171            mika	Michael Prokop
  172           milan	Milan Kupcevic
  173        mjeanson	Michael Jeanson
  174           mones	Ricardo Mones Lastra
  175           mpitt	Martin Pitt
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176           murat	Murat Demirten
  177            myon	Christoph Berg
  178          nbreen	Nicholas Breen
  179           neilm	Neil McGovern
  180         nicolas	Nicolas Boulenguez
  181          nilesh	Nilesh Patra
  182          nodens	Clément Hermann
  183         noodles	Jonathan McDowell
  184        nthykier	Niels Thykier
  185           ntyni	Niko Tyni
  186            odyx	Didier Raboud
  187           ohura	Makoto OHURA
  188           olasd	Nicolas Dandrimont
  189         olebole	Ole Streicher
  190            pabs	Paul Wise
  191    paddatrapper	Kyle Robbertze
  192          paride	Paride Legovini
  193             peb	Pierre-Elliott Bécue
  194             pgt	Pierre Gruet
  195           philh	Philip Hands
  196            phls	Paulo Henrique de Lima Santana
  197            pini	Gilles Filippini
  198             pjb	Phil Brooke
  199           pkern	Philipp Kern
  200          plessy	Charles Plessy
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201       pmatthaei	Patrick Matthäi
  202          pmhahn	Philipp Matthias Hahn
  203           pollo	Louis-Philippe Véronneau
  204         praveen	Praveen Arimbrathodiyil
  205             qjb	Jay Berkenbilt
  206             rak	Ryan Kavanagh
  207             ras	Russell Stuart
  208         reichel	Joachim Reichel
  209           rinni	Philip Rinn
  210         rlaager	Richard Laager
  211         roberto	Roberto C. Sanchez
  212        roehling	Timo Röhling
  213          roland	Roland Rosenfeld
  214             ron	Ron Lee
  215        rousseau	Ludovic Rousseau
  216             rra	Russ Allbery
  217             rul	Raúl Benencia
  218     rvandegrift	Ross Vandegrift
  219        santiago	Santiago Ruano Rincón
  220          sbadia	Sebastien Badia
  221             seb	Sebastien Delafond
  222       sebastien	Sébastien Villemot
  223              sf	Stefan Fritsch
  224        siretart	Reinhard Tartler
  225             sjr	Simon Richter
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226           skitt	Stephen Kitt
  227            smcv	Simon McVittie
  228             smr	Steven Michael Robbins
  229             snd	Dennis Braun
  230       spwhitton	Sean Whitton
  231       sramacher	Sebastian Ramacher
  232            srud	Sruthi Chandran
  233          ssgelm	Stephen Gelman
  234        stappers	Geert Stappers
  235        stefanor	Stefano Rivera
  236  stephanlachnit	Stephan Lachnit
  237       sthibault	Samuel Thibault
  238          stuart	Stuart Prescott
  239       sunweaver	Mike Gabriel
  240           sur5r	Jakob Haufe
  241            tach	Taku Yasui
  242          taffit	David Prévot
  243          takaki	Takaki Taniguchi
  244           talau	Marcos Talau
  245             tbm	Martin Michlmayr
  246        terceiro	Antonio Terceiro
  247           tiago	Tiago Bortoletto Vaz
  248          tianon	Tianon Gravi
  249          tijuca	Carsten Schoenert
  250        tjhukkan	Teemu Hukkanen
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251        tmancill	Tony Mancill
  252            tobi	Tobias Frost
  253           toddy	Tobias Quathamer
  254            toni	Toni Mueller
  255         treinen	Ralf Treinen
  256           troyh	Troy Heber
  257        tvainika	Tommi Vainikainen
  258        tvincent	Thomas Vincent
  259            ucko	Aaron M. Ucko
  260          uhoreg	Hubert Chathi
  261        ukleinek	Uwe Kleine-König
  262           urbec	Judit Foglszinger
  263         vagrant	Vagrant Cascadian
  264          vcheng	Vincent Cheng
  265           viiru	Arto Jantunen
  266            vivi	Vincent Prat
  267          vorlon	Steve Langasek
  268          vvidic	Valentin Vidic
  269          weasel	Peter Palfrader
  270        weinholt	Göran Weinholt
  271           wferi	Ferenc Wágner
  272          wijnen	Bas Wijnen
  273          wouter	Wouter Verhelst
  274             xam	Max Vozeler
  275          xluthi	Xavier Lüthi
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276            yadd	Xavier Guimard
  277            zack	Stefano Zacchiroli
  278            zeha	Christian Hofstaedtler
  279            zigo	Thomas Goirand
