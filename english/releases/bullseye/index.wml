#use wml::debian::template title="Debian &ldquo;bullseye&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"

<p>Debian <current_release_bullseye> was
released on <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 was initially released on <:=spokendate('2021-08-14'):>."
/>
The release included many major
changes, described in
our <a href="$(HOME)/News/2021/20210814">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian 11 has been superseded by
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('2023-06-10'):>.
</strong></p>

# <p><strong>However, bullseye benefits from Long Term Support (LTS) until
# the end of August 2026. The LTS is limited to i386, amd64, armel, armhf and arm64.
# All other architectures are no longer supported in bullseye.
# For more information, please refer to the <a
# href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
# </strong></p>

<p>To obtain and install Debian, see
the <a href="debian-installer/">installation information</a> page and the
<a href="installmanual">Installation Guide</a>. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Computer architectures supported at initial release of bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="../reportingbugs">report other issues</a> to us.</p>

<p>Last but not least, we have a list of <a href="credits">people who take
credit</a> for making this release happen.</p>
