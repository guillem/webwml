#! /usr/bin/perl

# Copyright (C) 2022-2023  Thomas Lange <lange@debian.org>

# This program is free software; you can redistribute it and/or modify it
# under the terms of version 2 of the GNU General Public License as published
# by the Free Software Foundation.

# create list of recent DSA/DLA items with links to sec-tracker and to the mail

# how it works:
# get the last n DSA entries
# get mapping for DSA to URL on the mailing list archive
# check if there are manual fixes of the URL in data/*.fixes
# print list in text or HTML
#
# -s prints a short mapping list. By default the script prints HTML code


use warnings;
use Time::Piece;
use HTTP::Tiny;
use Getopt::Std;

our ($opt_s);

$dsafile  = "data/dsa.data";
$dsafixes = "data/dsa.fixes";
$mldsa    = "https://lists.debian.org/debian-security-announce";

$dlafile  = "data/dla.data";
$dlafixes = "data/dla.fixes";
$mldla    = "https://lists.debian.org/debian-lts-announce";

# security tracker URL
$trurl  = "https://security-tracker.debian.org/tracker";

# global variables
# %years which years in the mail list archive must be parsed
# %msglink  map DSA/DLA to URL of announcement mail in the mailing list archive
# @all list of recent reports

sub usage {

  print << "EOM";

  mk-das-dla-list type [count]

  -s short list, no HTML
  type can be DSA or DLA
  count   list the last count reports
EOM
  exit 0;
}



sub create_map {

  # map DSA 1234-1 (or DLA) to URL in mailing list archive

  # args: hash to store data, URL
  my $href = shift;
  my $url  = shift;

  my ($msg, $nr);

  my $response = HTTP::Tiny->new->get("$url");
  warn "Failed fetching $url!\n" unless $response->{success};
  my $cont = $response->{content} if length $response->{content};
  my @lines = split(/\cJ/,$cont);

  foreach (@lines) {
    next until /^<li>/;

    # regex include some special handling of DLA lines including
    # a second string in brackets line this: [SECURITY] [UPDATE] [DLA 2948-1] ...
    ($msg,$nr) = $_ =~ m#href="(msg\d+.html)">\[SECURITY\]\s+(?:\[[A-Z]+\]\s+)?\[(D[SL]A[\s-]+[^\]]+)#;
    warn "DEBUG: NR $url $_\n" unless defined($nr);
    warn "DEBUG: MSG $url $_\n" unless defined($msg);

    next unless defined $nr;
    # some DLA contain a - instead of a space. Fix that
    # some DSA do not end in -\d, e.g. 1170. TODO, handle this
    $nr =~ s/(D[SL]A)[\s-]+/$1 /; # nr is always DSA 1234-1, space instead of -
    $href->{"$nr"} = "$url$msg";
  }
}

sub get_entries {

  # read $nentries entries and determine the years needed
  # save entries into array ref given, and set %years

  my $file = shift;
  my $aref = shift;
  my $i;

  open (F,"< $file") || die "Cannot open $file";
  foreach (<F>) {
    next until /^\[/;
    chomp;
    push @$aref,$_; # save line

    # match year only
    /^\[\d+\s+\w+\s+(\d+)\]/;
    $years{$1} = 1;
    $i++;
    last if ($i > $nentries);
  }
  close("F");
}

sub mk_list {

  # create html or text output of recent security announcements

  my @list = @_;
  my ($odsa,$date,$dsa,$pkg,$text);

  foreach (@list) {

    ($date,$dsa,$pkg,$text) = m#\[(\d+\s+\w+\s+\d+)\]\s+(D[SL]A-\d+(?:-\d)?)\s+([\w.+-]+)\s*(?:-\s+)?(.+)?#;
    $odsa = $dsa;
    $dsa =~ s/A-/A /;  # index is DSA 1234-1, not DSA-1234-1

    # no link was found, maybe due to typo on mailing list
    # then use a generic link, but still list this DSA/DLA
    warn "WARN: No link to mail for $dsa\n" unless $msglink{$dsa};
    $msglink{$dsa} = $msglink{'none'} unless $msglink{$dsa};

    if ($opt_s) {
      # print short list
      $msglink{$dsa} =~ s#http\S+announce/## if $msglink{$dsa};
      print "$dsa $msglink{$dsa}\n";
    } else {
      print "<tt>[$date]</tt> <strong><a href=$trurl/$odsa> T</a> &ensp;<a href=\"$msglink{$dsa}\">$dsa $pkg</a></strong> $text<br>\n";
    }
  }
}

sub check_fixes {

  my $href = shift;
  my $fixfile = shift;
  my $url = shift;

  # read in all fixes
  open (F,"< $fixfile") || die "Cannot open $fixfile";
  while (<F>) {
    next if /^#/;
    ($name,$link) = m/(^D[SL]A\s+\S+)\s+(\S+)/;
    # replace entry if a fix is available
    warn "Fixing $name with $link\n";
    $href->{"$name"} = "$url/$link";
  }
}

# main program

getopts('s');
$type = shift;
usage unless $type =~/^D[SL]A$/;

# read how much entries to show from cmdline or use default
$nentries = shift || 49;

if ($type eq 'DSA') {
  get_entries($dsafile,\@all);
  foreach (reverse sort keys %years) {
    create_map(\%msglink, "$mldsa/$_/");
  }
  # create a generic link for non-existing entries
  $msglink{'none'} = "$mldsa";
  check_fixes(\%msglink,$dsafixes,$mldsa);
  mk_list(@all);
}

if ($type eq 'DLA') {
  get_entries($dlafile,\@all);

  # DLA have a monthly index in the mailing list
  $t = localtime;
  foreach $y (keys %years) {
    for ("01".."12") {
      # skip the future
      next if ( $t->year == $y && $_ > $t->mon );
      create_map(\%msglink, "$mldla/$y/$_/");
    }
  }
  # create a generic link for non-existing entries
  $msglink{'none'} = "$mldla";
  check_fixes(\%msglink,$dlafixes,$mldla);
  mk_list(@all);
}

exit 0;
