<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>An out-of-bounds write vulnerability due to an integer overflow was
reported in libexif, a library to parse EXIF files, which could result
in denial of service, or potentially the execution of arbitrary code if
specially crafted image files are processed.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 0.6.21-2+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 0.6.21-5.1+deb10u1.</p>

<p>We recommend that you upgrade your libexif packages.</p>

<p>For the detailed security status of libexif please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libexif">https://security-tracker.debian.org/tracker/libexif</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4618.data"
# $Id: $
