<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3862">CVE-2020-3862</a>

    <p>Srikanth Gatta discovered that a malicious website may be able to
    cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3864">CVE-2020-3864</a>

    <p>Ryan Pickren discovered that a DOM object context may not have had
    a unique security origin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3865">CVE-2020-3865</a>

    <p>Ryan Pickren discovered that a top-level DOM object context may
    have incorrectly been considered secure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3867">CVE-2020-3867</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to universal cross site scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3868">CVE-2020-3868</a>

    <p>Marcin Towalski discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.26.4-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4627.data"
# $Id: $
