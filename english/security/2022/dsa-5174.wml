<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Demi Marie Obenour discovered a flaw in GnuPG, allowing for signature
spoofing via arbitrary injection into the status line. An attacker who
controls the secret part of any signing-capable key or subkey in the
victim's keyring, can take advantage of this flaw to provide a
correctly-formed signature that some software, including gpgme, will
accept to have validity and signer fingerprint chosen from the attacker.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.2.12-1+deb10u2.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.2.27-2+deb11u2.</p>

<p>We recommend that you upgrade your gnupg2 packages.</p>

<p>For the detailed security status of gnupg2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/gnupg2">\
https://security-tracker.debian.org/tracker/gnupg2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5174.data"
# $Id: $
