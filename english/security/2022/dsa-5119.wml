<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Subversion, a version control
system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28544">CVE-2021-28544</a>

    <p>Evgeny Kotkov reported that Subversion servers reveal <q>copyfrom</q>
    paths that should be hidden according to configured path-based
    authorization (authz) rules.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24070">CVE-2022-24070</a>

    <p>Thomas Weissschuh reported that Subversion's mod_dav_svn is prone to
    a use-after-free vulnerability when looking up path-based
    authorization rules, which can result in denial of service (crash of
    HTTPD worker handling the request).</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 1.10.4-1+deb10u3.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.14.1-3+deb11u1.</p>

<p>We recommend that you upgrade your subversion packages.</p>

<p>For the detailed security status of subversion please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/subversion">https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5119.data"
# $Id: $
