<define-tag pagetitle>Debian Installer Bookworm RC 1 release</define-tag>
<define-tag release_date>2023-04-03</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the first release candidate of the installer for Debian 12
<q>Bookworm</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>debian-cd:
  <ul>
    <li>Use hard links instead of symlinks for firmware packages
    (<a href="https://bugs.debian.org/1031696">#1031696</a>).</li>
    <li>Delete loadlin support entirely.</li>
  </ul>
  </li>
  <li>debian-installer:
  <ul>
    <li>Refresh help screen with minimum disk size for installation
    and better documentation links (<a href="https://bugs.debian.org/1033193">#1033193</a>).</li>
  </ul>
  </li>
  <li>debian-installer-utils:
  <ul>
    <li>Include xterm terminfo entry on all architectures, fixing nano
    issues in the graphical installer (<a href="https://bugs.debian.org/1026027">#1026027</a>).</li>
  </ul>
  </li>
  <li>grub-installer:
  <ul>
    <li>Basic heuristic for force-efi-extra-removable question.</li>
    <li>Add question for running grub-install with --no-nvram, using
    similar logic.</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li>Deduplicate the list of requested firmware files, not just the
    list of requesting modules (<a href="https://bugs.debian.org/1031631">#1031631</a>).</li>
    <li>Implement microcode support when /proc/cpuinfo contains a
    vendor_id field, with one of the following values (<a href="https://bugs.debian.org/1029804">#1029804</a>):
    <ul>
      <li>Install amd64-microcode on AuthenticAMD.</li>
      <li>Install intel-microcode on GenuineIntel.</li>
      <li>Enable non-free-firmware accordingly.</li>
      <li>Perform installation via finish-install, making sure
      apt-setup has been configured, and using apt-install for
      dependency resolution.</li>
    </ul>
    </li>
    <li>Optimize firmware package installation: process dpkg triggers
    once, after all packages have been installed, leading to a single
    update-initramfs call.</li>
    <li>Fix condition around mountmedia calls (<a href="https://bugs.debian.org/1032377">#1032377</a>).</li>
    <li>Fix files removal for non-accepted firmware packages
    (<a href="https://bugs.debian.org/1032377">#1032377</a>).</li>
    <li>Add a special case for the mhi module (<a href="https://bugs.debian.org/1032140">#1032140</a>), using
    holders information (e.g. ath11k_pci and qrtr_mhi for some Atheros
    wireless cards).</li>
    <li>Fix dmesg timestamp management.</li>
    <li>Fix package name extraction when removing a firmware package
    (e.g. it failed to install because it was corrupted).</li>
    <li>Build /var/log/firmware-summary as a 3-column summary of
    firmware (and microcode) packages getting installed
    (<a href="https://bugs.debian.org/1029849">#1029849</a>).</li>
    <li>Fix removal of temporary files in /target after installing
    firmware packages (<a href="https://bugs.debian.org/1033035">#1033035</a>).</li>
    <li>Determine the package name by using the Package field instead
    of trusting the filename when installing firmware packages
    (<a href="https://bugs.debian.org/1033035">#1033035</a>).</li>
    <li>Make sure not to include the possible -n option when setting
    the IFACES variables in check-missing-firmware (<a href="https://bugs.debian.org/1033035">#1033035</a>).</li>
  </ul>
  </li>
  <li>installation-report:
  <ul>
    <li>Store firmware information in /var/log/installer/firmware-summary
    (<a href="https://bugs.debian.org/1033325">#1033325</a>). See the 2.87
    changelog entry for details.</li>
    <li>Include firmware-summary in installation reports.</li>
  </ul>
  </li>
  <li>localechooser:
  <ul>
    <li>Remove AN from country list; move CW to Caribbean region.</li>
  </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>Enable sound modules and speakup on arm64 gtk images.</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Fix reloading mt7921e module after deploying firmware files
    (<a href="https://bugs.debian.org/1029116">#1029116</a>).</li>
    <li>Add michael_mic to crypto-modules, for ath11k and others
    (<a href="https://bugs.debian.org/1032140">#1032140</a>).</li>
    <li>Add qrtr_mhi to nic-wireless, for ath11k (<a href="https://bugs.debian.org/1032140">#1032140</a>).</li>
    <li>Add DRM ast driver to fb-modules on ppc64el (<a href="https://bugs.debian.org/990016">#990016</a>).</li>
    <li>Add sound and speakup udebs on arm64 and armhf (<a href="https://bugs.debian.org/1031289">#1031289</a>).</li>
  </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 41 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
