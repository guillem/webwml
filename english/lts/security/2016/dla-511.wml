<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A specially crafted HTTP response from a tracker (or potentially a UPnP
broadcast) can crash libtorrent in the parse_chunk_header() function.
Although this function is not present in this version, upstream's
additional sanity checks were added to abort the program if necessary
instead of crashing it.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.15.10-1+deb7u1.</p>

<p>We recommend that you upgrade your libtorrent-rasterbar packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-511.data"
# $Id: $
