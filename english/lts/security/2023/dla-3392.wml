<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two ruby-rack issues have been addressed:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27530">CVE-2023-27530</a>

    <p>Description: Limit all multipart parts, not just files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27539">CVE-2023-27539</a>

    <p>Description: Split headers on commas, then strip the strings in
    order to avoid ReDoS issues.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.0.6-3+deb10u3.</p>

<p>We recommend that you upgrade your ruby-rack packages.</p>

<p>For the detailed security status of ruby-rack please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-rack">https://security-tracker.debian.org/tracker/ruby-rack</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3392.data"
# $Id: $
