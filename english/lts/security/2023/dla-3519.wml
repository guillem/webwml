<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow in devn_pcx_write_rle() has been fixed in Ghostsciprt,
an interpreter for the PostScript language and PDF files.</p>

<p>For Debian 10 buster, this problem has been fixed in version
9.27~dfsg-2+deb10u8.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>For the detailed security status of ghostscript please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ghostscript">https://security-tracker.debian.org/tracker/ghostscript</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3519.data"
# $Id: $
