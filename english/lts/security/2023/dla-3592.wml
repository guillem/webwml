<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in Jetty, a Java based web server
and servlet engine.</p>

<p>The org.eclipse.jetty.servlets.CGI class has been deprecated. It is potentially
unsafe to use it. The upstream developers of Jetty recommend to use Fast CGI
instead. See also <a href="https://security-tracker.debian.org/tracker/CVE-2023-36479">CVE-2023-36479</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26048">CVE-2023-26048</a>

    <p>In affected versions servlets with multipart support (e.g. annotated with
    `@MultipartConfig`) that call `HttpServletRequest.getParameter()` or
    `HttpServletRequest.getParts()` may cause `OutOfMemoryError` when the
    client sends a multipart request with a part that has a name but no
    filename and very large content. This happens even with the default
    settings of `fileSizeThreshold=0` which should stream the whole part
    content to disk.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26049">CVE-2023-26049</a>

    <p>Nonstandard cookie parsing in Jetty may allow an attacker to smuggle
    cookies within other cookies, or otherwise perform unintended behavior by
    tampering with the cookie parsing mechanism.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40167">CVE-2023-40167</a>

    <p>Prior to this version Jetty accepted the `+` character proceeding the
    content-length value in a HTTP/1 header field. This is more permissive than
    allowed by the RFC and other servers routinely reject such requests with
    400 responses. There is no known exploit scenario, but it is conceivable
    that request smuggling could result if jetty is used in combination with a
    server that does not close the connection after sending such a 400
    response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-36479">CVE-2023-36479</a>

    <p>Users of the CgiServlet with a very specific command structure may have the
    wrong command executed. If a user sends a request to a
    org.eclipse.jetty.servlets.CGI Servlet for a binary with a space in its
    name, the servlet will escape the command by wrapping it in quotation
    marks. This wrapped command, plus an optional command prefix, will then be
    executed through a call to Runtime.exec. If the original binary name
    provided by the user contains a quotation mark followed by a space, the
    resulting command line will contain multiple tokens instead of one.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
9.4.16-0+deb10u3.</p>

<p>We recommend that you upgrade your jetty9 packages.</p>

<p>For the detailed security status of jetty9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jetty9">https://security-tracker.debian.org/tracker/jetty9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3592.data"
# $Id: $
