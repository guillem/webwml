<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Issues were discovered in Lemonldap::NG, an OpenID-Connect, CAS and SAML
compatible Web-SSO system, which could lead to impersonation of users
with a second factor authentication.</p>

<p>Weak session ID generation in the AuthBasic handler and incorrect
failure handling during a password check allow attackers to bypass 2FA
verification.  Any plugin that tries to deny session creation after the
store step does not deny an AuthBasic session.</p>

<p>Using the AuthBasic handler is now refused for users with a second
factor.  Admins who are *absolutely sure* that such accounts should be
able to use AuthBasic handlers (which are password only) can append
<code>and not $ENV{AuthBasic}</code> to the 2FA activation rules.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.0.2+ds-7+deb10u9.</p>

<p>We recommend that you upgrade your lemonldap-ng packages.</p>

<p>For the detailed security status of lemonldap-ng please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lemonldap-ng">https://security-tracker.debian.org/tracker/lemonldap-ng</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3496.data"
# $Id: $
