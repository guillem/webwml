<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An out-of-bounds read problem was found in the postprocess_terminfo
function of ncurses, a text-based user interface toolkit, which could
potentially lead to an exposure of sensitive information or denial of
service.</p>

<p>For Debian 10 buster, these problems have been fixed in version
6.1+20181013-2+deb10u4.</p>

<p>We recommend that you upgrade your ncurses packages.</p>

<p>For the detailed security status of ncurses please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ncurses">https://security-tracker.debian.org/tracker/ncurses</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3586.data"
# $Id: $
