<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in libde265, an open source implementation
of the H.265 video codec, which may result in denial of or have unspecified other
impact.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21596">CVE-2020-21596</a>

    <p>libde265 v1.0.4 contains a global buffer overflow in the
    decode_CABAC_bit function, which can be exploited via a crafted a
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21597">CVE-2020-21597</a>

    <p>libde265 v1.0.4 contains a heap buffer overflow in the mc_chroma
    function, which can be exploited via a crafted a file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21598">CVE-2020-21598</a>

    <p>libde265 v1.0.4 contains a heap buffer overflow in the
    ff_hevc_put_unweighted_pred_8_sse function, which can be exploited
    via a crafted a file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43235">CVE-2022-43235</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via ff_hevc_put_hevc_epel_pixels_8_sse in
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43236">CVE-2022-43236</a>

    <p>Libde265 v1.0.8 was discovered to contain a stack-buffer-overflow
    vulnerability via put_qpel_fallback<unsigned short> in
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43237">CVE-2022-43237</a>

    <p>Libde265 v1.0.8 was discovered to contain a stack-buffer-overflow
    vulnerability via void put_epel_hv_fallback<unsigned short> in
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43238">CVE-2022-43238</a>

    <p>Libde265 v1.0.8 was discovered to contain an unknown crash via
    ff_hevc_put_hevc_qpel_h_3_v_3_sse in sse-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS)
    via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43239">CVE-2022-43239</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via mc_chroma<unsigned short> in motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS)
    via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43240">CVE-2022-43240</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via ff_hevc_put_hevc_qpel_h_2_v_1_sse in
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43241">CVE-2022-43241</a>

    <p>Libde265 v1.0.8 was discovered to contain an unknown crash via
    ff_hevc_put_hevc_qpel_v_3_8_sse in sse-motion.cc. This vulnerability
    allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43242">CVE-2022-43242</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via mc_luma<unsigned char> in motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS)
    via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43243">CVE-2022-43243</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via ff_hevc_put_weighted_pred_avg_8_sse in
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43244">CVE-2022-43244</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_qpel_fallback<unsigned short> in
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43245">CVE-2022-43245</a>

    <p>Libde265 v1.0.8 was discovered to contain a segmentation violation
    via apply_sao_internal<unsigned short> in sao.cc. This vulnerability
    allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43248">CVE-2022-43248</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_weighted_pred_avg_16_fallback in
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43249">CVE-2022-43249</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_epel_hv_fallback<unsigned short> in
    fallback-motion.cc.  This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43250">CVE-2022-43250</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_qpel_0_0_fallback_16 in fallback-motion.cc.
    This vulnerability allows attackers to cause a Denial of Service
    (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43252">CVE-2022-43252</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_epel_16_fallback in fallback-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS)
    via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43253">CVE-2022-43253</a>

    <p>Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_unweighted_pred_16_fallback in
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47655">CVE-2022-47655</a>

    <p>Libde265 1.0.9 is vulnerable to Buffer Overflow in function void
    put_qpel_fallback<unsigned short></p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.0.3-1+deb10u2.</p>

<p>We recommend that you upgrade your libde265 packages.</p>

<p>For the detailed security status of libde265 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libde265">https://security-tracker.debian.org/tracker/libde265</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3280.data"
# $Id: $
