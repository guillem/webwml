<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Erik Krogh Kristensen discovered that sqlparse, a non-validating SQL
parser, contained a regular expression that is vulnerable to ReDoS
(Regular Expression Denial of Service).</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.2.4-1+deb10u1.</p>

<p>We recommend that you upgrade your sqlparse packages.</p>

<p>For the detailed security status of sqlparse please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sqlparse">https://security-tracker.debian.org/tracker/sqlparse</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3425.data"
# $Id: $
