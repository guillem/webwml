<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in svgpp: a C++ library for parsing and
rendering Scalable Vector Graphics (SVG) files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44960">CVE-2021-44960</a>

   <p>The XMLDocument::getRoot function in the renderDocument function handled the
   XMLDocument object improperly. Specifically, it returned a null pointer
   prematurely at the second if statement, resulting in a null pointer
   reference behind the renderDocument function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6245">CVE-2019-6245</a>

<p>and <a href="https://security-tracker.debian.org/tracker/CVE-2019-6247">CVE-2019-6247</a>:
   issues were discovered in Anti-Grain Geometry (AGG) within the function
   agg::cell_aa::not_equal. Since svgpp is a header-only library, the issue is
   only transitive in theory. As a result, only a dependency version hardening
   has been added to the control file.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.2.3+dfsg1-6+deb10u1.</p>

<p>We recommend that you upgrade your svgpp packages.</p>

<p>For the detailed security status of svgpp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/svgpp">https://security-tracker.debian.org/tracker/svgpp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3376.data"
# $Id: $
