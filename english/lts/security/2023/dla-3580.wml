<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The mod_jk component of Apache Tomcat Connectors, an Apache 2 module to
forward requests from Apache to Tomcat, in some circumstances, such as when
a configuration included "JkOptions +ForwardDirectories" but the
configuration did not provide explicit mounts for all possible proxied
requests, mod_jk would use an implicit mapping and map the request to the
first defined worker. Such an implicit mapping could result in the
unintended exposure of the status worker and/or bypass security constraints
configured in httpd. As of this security update, the implicit mapping
functionality has been removed and all mappings must now be via explicit
configuration. This issue affects Apache Tomcat Connectors (mod_jk only).</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:1.2.46-1+deb10u2.</p>

<p>We recommend that you upgrade your libapache-mod-jk packages.</p>

<p>For the detailed security status of libapache-mod-jk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libapache-mod-jk">https://security-tracker.debian.org/tracker/libapache-mod-jk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3580.data"
# $Id: $
