<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in git, a fast, scalable
and distributed revision control system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22490">CVE-2023-22490</a>

    <p>yvvdwf found a data exfiltration vulnerability while performing a local
    clone from a malicious repository even using a non-local transport.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23946">CVE-2023-23946</a>

    <p>Joern Schneeweisz found a path traversal vulnerbility in git-apply
    that a path outside the working tree can be overwritten as the acting
    user.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:2.20.1-2+deb10u8.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3338.data"
# $Id: $
