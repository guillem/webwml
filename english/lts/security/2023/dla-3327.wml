<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in nss, the Network
Security Service libraries.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6829">CVE-2020-6829</a>

    <p>When performing EC scalar point multiplication, the wNAF point
    multiplication algorithm was used; which leaked partial information about
    the nonce used during signature generation. Given an electro-magnetic trace
    of a few signature generations, the private key could have been computed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12400">CVE-2020-12400</a>

    <p>When converting coordinates from projective to affine, the modular
    inversion was not performed in constant time, resulting in a possible
    timing-based side channel attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12401">CVE-2020-12401</a>

    <p>During ECDSA signature generation, padding applied in the nonce designed to
    ensure constant-time scalar multiplication was removed, resulting in
    variable-time execution dependent on secret data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12403">CVE-2020-12403</a>

    <p>A flaw was found in the way CHACHA20-POLY1305 was implemented in NSS.
    When using multi-part Chacha20, it could cause out-of-bounds reads.
    This issue was fixed by explicitly disabling multi-part ChaCha20&nbsp;
    (which was not functioning correctly) and strictly enforcing tag length.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0767">CVE-2023-0767</a>

    <p>Christian Holler discovered that incorrect handling of PKCS 12 Safe Bag
    attributes may result in execution of arbitrary code if a specially crafted
    PKCS 12 certificate bundle is processed.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:3.42.1-1+deb10u6.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>For the detailed security status of nss please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nss">https://security-tracker.debian.org/tracker/nss</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3327.data"
# $Id: $
