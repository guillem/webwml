<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Triggering arbitrary code execution was possible due to .desktop files
registered as application/x-ms-dos-executable MIME handlers in the open
source .NET framework Mono.</p>

<p>For Debian 10 buster, this problem has been fixed in version
5.18.0.240+dfsg-3+deb10u1.</p>

<p>We recommend that you upgrade your mono packages.</p>

<p>For the detailed security status of mono please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mono">https://security-tracker.debian.org/tracker/mono</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3343.data"
# $Id: $
