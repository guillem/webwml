<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>
<p>sssd 1.16.3-3.2+deb10u1 (<a href="./dla-3436">DLA 3436-1</a>) had a broken upgrade path from
version 1.16.3-3.2.</p>

<p>One could upgrade sssd-common to 1.16.3-3.2+deb10u1 while leaving
libsss-certmap0 at 1.16.3-3.2; the version mismatch broke SSSD as the
the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2022-4254">CVE-2022-4254</a>
introduces new symbols which are used in sssd-common's <code>sssd_pam</code>.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.16.3-3.2+deb10u2.  This version differs from 1.16.3-3.2+deb10u1 only
in package metadata.  (Bumping the minimum version for libsss-certmap0
in sssd-common's Depends: field ensures a safe upgrade path.)</p>

<p>We recommend that you upgrade your sssd packages.</p>

<p>For the detailed security status of sssd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sssd">https://security-tracker.debian.org/tracker/sssd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3436-2.data"
# $Id: $
