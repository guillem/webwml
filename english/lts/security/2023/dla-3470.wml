<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In OWSLib, a Python client library for Open Geospatial web services,
the XML parser did not disable entity resolution which could lead to
arbitrary file reads from an attacker-controlled XML payload.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.17.1-1+deb10u1.</p>

<p>We recommend that you upgrade your owslib packages.</p>

<p>For the detailed security status of owslib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/owslib">https://security-tracker.debian.org/tracker/owslib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3470.data"
# $Id: $
