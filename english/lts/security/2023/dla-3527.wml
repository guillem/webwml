<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>SoX is a command line utility that can convert various formats
of computer audio files in to other formats. It can also apply
various effects to these sound files during the conversion.</p>

<p>Sox was vulnerable to divide by zero vulnerability by reading an
specialy crafted Creative Voice File (.voc) file, in the read_samples
function. This flaw can lead to a denial of service.</p>

<p>For Debian 10 buster, this problem has been fixed in version
14.4.2+git20190427-1+deb10u3.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>For the detailed security status of sox please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sox">https://security-tracker.debian.org/tracker/sox</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3527.data"
# $Id: $
