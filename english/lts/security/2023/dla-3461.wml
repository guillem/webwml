<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libfastjson, a fast json library for C.
Due to missing checks, out-of-bounds write might happen when parsing
large JSON files.</p>


<p>For Debian 10 buster, this problem has been fixed in version
0.99.8-2+deb10u1.</p>

<p>We recommend that you upgrade your libfastjson packages.</p>

<p>For the detailed security status of libfastjson please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libfastjson">https://security-tracker.debian.org/tracker/libfastjson</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3461.data"
# $Id: $
