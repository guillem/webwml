<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in apache2,
a webserver that may be used as front-end proxy for other applications.
These vulnerabilities may lead to HTTP request smuggling, and thus
to front-end security controls being bypassed.</p>

<p>Unfortunately, fixing these security vulnerabilities may require
changes to configuration files. Some out-of-specification
RewriteRule directives that were previously silently accepted,
are now rejected with error AH10409. For instance, some RewriteRules
that included a back-reference and the flags "[L,NC]" will need to
be written with extra escaping flags such as "[B= ?,BNP,QSA]".</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25690">CVE-2023-25690</a>

  <p>Some mod_proxy configurations allow an HTTP request Smuggling
  attack. Configurations are affected when mod_proxy is
  enabled along with some form of RewriteRule
  or ProxyPassMatch in which a non-specific pattern matches
  some portion of the user-supplied request-target (URL)
  data and is then re-inserted into the proxied request-target
  using variable substitution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27522">CVE-2023-27522</a>

    <p>HTTP Response Smuggling in mod_proxy_uwsgi</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.4.38-3+deb10u10.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3401.data"
# $Id: $
