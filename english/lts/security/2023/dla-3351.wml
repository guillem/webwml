<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Apache HTTP
server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2006-20001">CVE-2006-20001</a>

<p>A carefully crafted If: request header can cause a memory read, or write
of a single zero byte, in a pool (heap) memory location beyond the header
value sent. This could cause the process to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33193">CVE-2021-33193</a>

<p>A crafted method sent through HTTP/2 will bypass validation and be
forwarded by mod_proxy, which can lead to request splitting or cache
poisoning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36760">CVE-2022-36760</a>

<p>Inconsistent Interpretation of HTTP Requests ('HTTP Request Smuggling')
vulnerability in mod_proxy_ajp of Apache HTTP Server allows an attacker to
smuggle requests to the AJP server it forwards requests to.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37436">CVE-2022-37436</a>

<p>A malicious backend can cause the response headers to be truncated early,
resulting in some headers being incorporated into the response body. If
the later headers have any security purpose, they will not be interpreted
by the client.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.4.38-3+deb10u9.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3351.data"
# $Id: $
