<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in zabbix, a network
monitoring solution, potentially allowing to crash the server, information
disclosure or Cross-Site-Scripting attacks.</p>

<p>Important Notices:
To mitigate <a href="https://security-tracker.debian.org/tracker/CVE-2019-17382">CVE-2019-17382</a>, on existing installations, the guest account
needs to be manually disabled, for example by disabling the the <q>Guest
group</q> in the UI:
   Administration -> User groups -> Guests -> Untick Enabled</p>

<p>This update also fixes a regression with <a href="https://security-tracker.debian.org/tracker/CVE-2022-35229">CVE-2022-35229</a>, which broke the
possiblity to edit and add discovery rules in the UI.</p>


<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7484">CVE-2013-7484</a>

    <p>Zabbix before version 4.4.0alpha2 stores credentials in the <q>users</q>
    table with the password hash stored as a MD5 hash, which is a known
    insecure hashing method. Furthermore, no salt is used with the hash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17382">CVE-2019-17382</a>

<p>(Disputed, not seen by upstream as not a security issue)</p>

    <p>An issue was discovered in
    zabbix.php?action�shboard.view&dashboardid=1 in Zabbix through
    4.4. An attacker can bypass the login page and access the dashboard
    page, and then create a Dashboard, Report, Screen, or Map without
    any Username/Password (i.e., anonymously). All created elements
    (Dashboard/Report/Screen/Map) are accessible by other users and by
    an admin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-35229">CVE-2022-35229</a>

    <p>An authenticated user can create a link with reflected
    Javascript code inside it for the discovery page and send it to
    other users. The payload can be executed only with a known CSRF
    token value of the victim, which is changed periodically and is
    difficult to predict.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43515">CVE-2022-43515</a>

    <p>Zabbix Frontend provides a feature that allows admins to
    maintain the installation and ensure that only certain IP addresses
    can access it. In this way, any user will not be able to access the
    Zabbix Frontend while it is being maintained and possible sensitive
    data will be prevented from being disclosed. An attacker can bypass
    this protection and access the instance using IP address not listed
    in the defined range.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29450">CVE-2023-29450</a>

    <p>JavaScript pre-processing can be used by the attacker to gain
    access to the file system (read-only access on behalf of user
    <q>zabbix</q>) on the Zabbix Server or Zabbix Proxy, potentially leading
    to unauthorized access to sensitive data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29451">CVE-2023-29451</a>

    <p>Specially crafted string can cause a buffer overrun in the JSON
    parser library leading to a crash of the Zabbix Server or a Zabbix
    Proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29454">CVE-2023-29454</a>

    <p>A Stored or persistent cross-site scripting (XSS) vulnerability
    was found on “Users” section in “Media” tab in “Send to” form field.
    When new media is created with malicious code included into field
    “Send to” then it will execute when editing the same media.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29455">CVE-2023-29455</a>

    <p>A Reflected XSS attacks, also known as non-persistent attacks, was
    found where an attacker can pass malicious code as GET request to
    graph.php and system will save it and will execute when current
    graph page is opened.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29456">CVE-2023-29456</a>

    <p>URL validation scheme receives input from a user and then parses
    it to identify its various components. The validation scheme can
    ensure that all URL components comply with internet standards.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29457">CVE-2023-29457</a>

    <p>A Reflected XSS attacks, also known as non-persistent attacks, was
    found where XSS session cookies could be revealed, enabling a
    perpetrator to impersonate valid users and abuse their private
    accounts.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:4.0.4+dfsg-1+deb10u2.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3538.data"
# $Id: $
