<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in zabbix,
a network monitoring solution, potentially allowing User Enumeration,
Cross-Site-Scripting or Cross-Site Request Forgery.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15132">CVE-2019-15132</a>

<p>Zabbix through 4.4.0alpha1 allows User Enumeration. With login requests, it is
possible to enumerate application usernames based on the variability of server
responses (e.g., the <q>Login name or password is incorrect</q> and <q>No permissions
for system access</q> messages, or just blocking for a number of seconds). This
affects both api_jsonrpc.php and index.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15803">CVE-2020-15803</a>

<p>Zabbix before 3.0.32rc1, 4.x before 4.0.22rc1, 4.1.x through 4.4.x
before 4.4.10rc1, and 5.x before 5.0.2rc1 allows stored XSS in the URL
Widget.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27927">CVE-2021-27927</a>

<p>In Zabbix from 4.0.x before 4.0.28rc1, 5.0.0alpha1 before 5.0.10rc1,
5.2.x before 5.2.6rc1, and 5.4.0alpha1 before 5.4.0beta2, the
CControllerAuthenticationUpdate controller lacks a CSRF protection
mechanism. The code inside this controller calls diableSIDValidation
inside the init() method. An attacker doesn't have to know Zabbix user
login credentials, but has to know the correct Zabbix URL and contact
information of an existing user with sufficient privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24349">CVE-2022-24349</a>

<p>An authenticated user can create a link with reflected XSS payload for
actions’ pages, and send it to other users. Malicious code has access to
all the same objects as the rest of the web page and can make arbitrary
modifications to the contents of the page being displayed to a victim.
This attack can be implemented with the help of social engineering and
expiration of a number of factors - an attacker should have authorized
access to the Zabbix Frontend and allowed network connection between a
malicious server and victim’s computer, understand attacked
infrastructure, be recognized by the victim as a trustee and use trusted
communication channel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24917">CVE-2022-24917</a>

<p>An authenticated user can create a link with reflected Javascript code
inside it for services’ page and send it to other users. The payload can
be executed only with a known CSRF token value of the victim, which is
changed periodically and is difficult to predict. Malicious code has
access to all the same objects as the rest of the web page and can make
arbitrary modifications to the contents of the page being displayed to a
victim during social engineering attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24919">CVE-2022-24919</a>

<p>An authenticated user can create a link with reflected Javascript code
inside it for graphs’ page and send it to other users. The payload can
be executed only with a known CSRF token value of the victim, which is
changed periodically and is difficult to predict. Malicious code has
access to all the same objects as the rest of the web page and can make
arbitrary modifications to the contents of the page being displayed to a
victim during social engineering attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-35229">CVE-2022-35229</a>

<p>An authenticated user can create a link with reflected Javascript code
inside it for the discovery page and send it to other users. The payload
can be executed only with a known CSRF token value of the victim, which
is changed periodically and is difficult to predict.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-35230">CVE-2022-35230</a>

<p>An authenticated user can create a link with reflected Javascript code
inside it for the graphs page and send it to other users. The payload
can be executed only with a known CSRF token value of the victim, which
is changed periodically and is difficult to predict.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:4.0.4+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3390.data"
# $Id: $
