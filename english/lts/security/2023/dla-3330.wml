<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential privilege escalation
vulnerability in the "amanda" backup utility.</p>

<p>The SUID binary located at <code>/lib/amanda/rundump</code> executed
<code>/usr/sbin/dump</code> as root with arguments controlled by the
attacker, which may have led to an escalation of privileges, denial of
service (DoS) or information disclosure.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37704">CVE-2022-37704</a></li>
</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:3.5.1-2+deb10u1.</p>

<p>We recommend that you upgrade your amanda packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3330.data"
# $Id: $
