<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A couple of security issues were discovered in ruby2.5, the Ruby
interpreter, and are as follows </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33621">CVE-2021-33621</a>

    <p>Hiroshi Tokumaru discovered that Ruby did not properly handle
    certain user input for applications the generate HTTP responses
    using cgi gem. An attacker could possibly use this issue to
    maliciously modify the response a user would receive from a
    vulnerable application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28739">CVE-2022-28739</a>

    <p>It was discovered that Ruby incorrectly handled certain inputs.
    An attacker could possibly use this issue to expose sensitive
    information.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed, along with the
regressions caused by the last Ruby update, in version 2.5.5-3+deb10u6.</p>

<p>We recommend that you upgrade your ruby2.5 packages.</p>

<p>For the detailed security status of ruby2.5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby2.5">https://security-tracker.debian.org/tracker/ruby2.5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3450.data"
# $Id: $
