<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The firmware-nonfree package has been updated to include addtional firmware
that may be requested by some drivers in Linux 5.10, availble for Debian LTS as
backported kernel.</p>

<p>Some of the updated firmware files adresses security vulnerabilities, which may
allow Escalation of Privileges, Denial of Services and Information Disclosures.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24586">CVE-2020-24586</a>

<p>(INTEL-SA-00473)</p>

    <p>The 802.11 standard that underpins Wi-Fi Protected Access (WPA,
    WPA2, and WPA3) and Wired Equivalent Privacy (WEP) doesn't require
    that received fragments be cleared from memory after (re)connecting
    to a network. Under the right circumstances, when another device
    sends fragmented frames encrypted using WEP, CCMP, or GCMP, this can
    be abused to inject arbitrary network packets and/or exfiltrate user
    data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24587">CVE-2020-24587</a>

 <p>(INTEL-SA-00473)</p>

    <p>The 802.11 standard that underpins Wi-Fi Protected Access (WPA,
    WPA2, and WPA3) and Wired Equivalent Privacy (WEP) doesn't require
    that all fragments of a frame are encrypted under the same key. An
    adversary can abuse this to decrypt selected fragments when another
    device sends fragmented frames and the WEP, CCMP, or GCMP encryption
    key is periodically renewed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24588">CVE-2020-24588</a>

 <p>(INTEL-SA-00473)</p>

    <p>The 802.11 standard that underpins Wi-Fi Protected Access (WPA,
    WPA2, and WPA3) and Wired Equivalent Privacy (WEP) doesn't require
    that the A-MSDU flag in the plaintext QoS header field is
    authenticated. Against devices that support receiving non-SSP A-MSDU
    frames (which is mandatory as part of 802.11n), an adversary can
    abuse this to inject arbitrary network packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23168">CVE-2021-23168</a>

 <p>(INTEL-SA-00621)</p>

    <p>Out of bounds read for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow an unauthenticated user to
    potentially enable denial of service via adjacent access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23223">CVE-2021-23223</a>

<p>(INTEL-SA-00621)</p>

    <p>Improper initialization for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow a privileged user to potentially
    enable escalation of privilege via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37409">CVE-2021-37409</a>

<p>(INTEL-SA-00621)</p>

    <p>Improper access control for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow a privileged user to potentially
    enable escalation of privilege via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44545">CVE-2021-44545</a>

<p>(INTEL-SA-00621)</p>

    <p>Improper input validation for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow an unauthenticated user to
    potentially enable denial of service via adjacent access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21181">CVE-2022-21181</a>

<p>(INTEL-SA-00621)</p>

    <p>Improper input validation for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow a privileged user to potentially
    enable escalation of privilege via local access.</p>

<p>The following advisories are also fixed by this upload, but needs an
updated Linux kernel to load the updated firmware:</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12362">CVE-2020-12362</a>

<p>(INTEL-SA-00438)</p>

    <p>Integer overflow in the firmware for some Intel(R) Graphics Drivers
    for Windows * before version 26.20.100.7212 and before Linux kernel
    version 5.5 may allow a privileged user to potentially enable an
    escalation of privilege via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12363">CVE-2020-12363</a>

<p>(INTEL-SA-00438)</p>

    <p>Improper input validation in some Intel(R) Graphics Drivers for
    Windows* before version 26.20.100.7212 and before Linux kernel
    version 5.5 may allow a privileged user to potentially enable a
    denial of service via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12364">CVE-2020-12364</a>

<p>(INTEL-SA-00438)</p>

    <p>Null pointer reference in some Intel(R) Graphics Drivers for
    Windows* before version 26.20.100.7212 and before version Linux
    kernel version 5.5 may allow a privileged user to potentially enable
    a denial of service via local access.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
20190114+really20220913-0+deb10u1.</p>

<p>We recommend that you upgrade your firmware-nonfree packages.</p>

<p>For the detailed security status of firmware-nonfree please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firmware-nonfree">https://security-tracker.debian.org/tracker/firmware-nonfree</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3380.data"
# $Id: $
