<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Python, an interactive
high-level object-oriented language. An attacker may cause command
injection, denial of service (DoS), request smuggling and port
scanning.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-20107">CVE-2015-20107</a>

    <p>The mailcap module does not add escape characters into commands
    discovered in the system mailcap file. This may allow attackers to
    inject shell commands into applications that call
    mailcap.findmatch with untrusted input (if they lack validation of
    user-provided filenames or arguments).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20907">CVE-2019-20907</a>

    <p>In Lib/tarfile.py, an attacker is able to craft a TAR archive
    leading to an infinite loop when opened by tarfile.open, because
    _proc_pax lacks header validation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8492">CVE-2020-8492</a>

    <p>Python allows an HTTP server to conduct Regular Expression Denial
    of Service (ReDoS) attacks against a client because of
    urllib.request.AbstractBasicAuthHandler catastrophic backtracking.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26116">CVE-2020-26116</a>

    <p>http.client allows CRLF injection if the attacker controls the
    HTTP request method, as demonstrated by inserting CR and LF
    control characters in the first argument of
    HTTPConnection.request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3177">CVE-2021-3177</a>

    <p>Python has a buffer overflow in PyCArg_repr in _ctypes/callproc.c,
    which may lead to remote code execution in certain Python
    applications that accept floating-point numbers as untrusted
    input, as demonstrated by a 1e300 argument to
    c_double.from_param. This occurs because sprintf is used unsafely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3733">CVE-2021-3733</a>

    <p>There's a flaw in urllib's AbstractBasicAuthHandler class. An
    attacker who controls a malicious HTTP server that an HTTP client
    (such as web browser) connects to, could trigger a Regular
    Expression Denial of Service (ReDOS) during an authentication
    request with a specially crafted payload that is sent by the
    server to the client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3737">CVE-2021-3737</a>

    <p>An improperly handled HTTP response in the HTTP client code of
    python may allow a remote attacker, who controls the HTTP server,
    to make the client script enter an infinite loop, consuming CPU
    time.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4189">CVE-2021-4189</a>

    <p>The FTP (File Transfer Protocol) client library in PASV (passive)
    mode trusts the host from the PASV response by default. This flaw
    allows an attacker to set up a malicious FTP server that can trick
    FTP clients into connecting back to a given IP address and
    port. This vulnerability could lead to FTP client scanning
    ports. For the rare user who wants the previous behavior, set a
    `trust_server_pasv_ipv4_address` attribute on your `ftplib.FTP`
    instance to True.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45061">CVE-2022-45061</a>

    <p>An unnecessary quadratic algorithm exists in one path when
    processing some inputs to the IDNA (RFC 3490) decoder, such that a
    crafted, unreasonably long name being presented to the decoder
    could lead to a CPU denial of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.7.16-2+deb10u2.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>For the detailed security status of python2.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python2.7">https://security-tracker.debian.org/tracker/python2.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3432.data"
# $Id: $
