<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security issues were found in PHP, a widely-used open source general
purpose scripting language, which could result in information
disclosure, denial of service or potentially remote code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3823">CVE-2023-3823</a>

     <p>Various XML functions rely on libxml global state to track
     configuration variables, like whether external entities are loaded.
     This state is assumed to be unchanged unless the user explicitly
     changes it by calling appropriate function.  Joas Schilling and
     Baptista Katapi discovered that, since the state is process-global,
     other modules — such as ImageMagick — may also use this library
     within the same process and change that global state for their
     internal purposes, and leave it in a state where external entities
     loading is enabled.  This can lead to the situation where external
     XML is parsed with external entities loaded, which can lead to
     disclosure of any local files accessible to PHP.  This vulnerable
     state may persist in the same process across many requests, until
     the process is shut down.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3824">CVE-2023-3824</a>

    <p>Niels Dossche discovered that when loading a Phar file, while
    reading PHAR directory entries, insufficient length checking may
    lead to a stack buffer overflow, leading potentially to memory
    corruption or RCE.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
7.3.31-1~deb10u5.</p>

<p>We recommend that you upgrade your php7.3 packages.</p>

<p>For the detailed security status of php7.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.3">https://security-tracker.debian.org/tracker/php7.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3555.data"
# $Id: $
