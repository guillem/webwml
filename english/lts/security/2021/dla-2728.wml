<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of issues in VideoLAN (aka 'vlc',
a popular video and multimedia player:</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25801">CVE-2021-25801</a>

    <p>A buffer overflow vulnerability in the __Parse_indx component allowed
    attackers to cause an out-of-bounds read via a crafted .avi file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25802">CVE-2021-25802</a>

    <p>A buffer overflow vulnerability in the AVI_ExtractSubtitle component
    could have allowed attackers to cause an out-of-bounds read via a crafted
    .avi file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25803">CVE-2021-25803</a>

    <p>A buffer overflow vulnerability in the vlc_input_attachment_New
    component allowed attackers to cause an out-of-bounds read via a
    specially-crafted .avi file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25804">CVE-2021-25804</a>

    <p>A NULL-pointer dereference in "Open" in avi.c can result in a denial of
    service (DoS) vulnerability.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
3.0.11-0+deb9u2.</p>

<p>We recommend that you upgrade your vlc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2728.data"
# $Id: $
