<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Researchers at the United States of America National Security Agency (NSA)
identified a denial of services vulnerability in strongSwan, an
IKE/IPsec suite.</p>

<p>Once the in-memory certificate cache is full it tries to randomly replace
lesser used entries. Depending on the generated random value, this could
lead to an integer overflow that results in a double-dereference and a
call using out-of-bounds memory that most likely leads to a segmentation
fault.</p>

<p>Remote code execution can't be ruled out completely, but attackers have
no control over the dereferenced memory, so it seems unlikely at this
point.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
5.5.1-4+deb9u5.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>For the detailed security status of strongswan please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/strongswan">https://security-tracker.debian.org/tracker/strongswan</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2788.data"
# $Id: $
