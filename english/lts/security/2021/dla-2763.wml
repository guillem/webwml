<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability has been found in Kaminari, a pagination engine plugin
for Rails 3+ and other modern frameworks, that would allow an attacker to
inject arbitrary code into pages with pagination links.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.17.0-3+deb9u1.</p>

<p>We recommend that you upgrade your ruby-kaminari packages.</p>

<p>For the detailed security status of ruby-kaminari please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ruby-kaminari">https://security-tracker.debian.org/tracker/ruby-kaminari</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2763.data"
# $Id: $
