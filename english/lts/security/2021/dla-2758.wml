<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in sssd.</p>

<p>The sssctl command was vulnerable to shell command injection via the logs-fetch
and cache-expire subcommands. This flaw allows an attacker to trick the root
user into running a specially crafted sssctl command, such as via sudo, to gain
root access. The highest threat from this vulnerability is to confidentiality,
integrity, as well as system availability.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.15.0-3+deb9u2.</p>

<p>We recommend that you upgrade your sssd packages.</p>

<p>For the detailed security status of sssd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sssd">https://security-tracker.debian.org/tracker/sssd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2758.data"
# $Id: $
