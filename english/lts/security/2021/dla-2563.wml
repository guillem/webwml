<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two issues in the openssl cryptographic
system:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23840">CVE-2021-23840</a>

    <p>Calls to EVP_CipherUpdate, EVP_EncryptUpdate and EVP_DecryptUpdate may
    overflow the output length argument in some cases where the input length is
    close to the maximum permissable length for an integer on the platform. In
    such cases the return value from the function call will be 1 (indicating
    success), but the output length value will be negative. This could cause
    applications to behave incorrectly or crash. OpenSSL versions 1.1.1i and
    below are affected by this issue. Users of these versions should upgrade to
    OpenSSL 1.1.1j. OpenSSL versions 1.0.2x and below are affected by this
    issue. However OpenSSL 1.0.2 is out of support and no longer receiving
    public updates. Premium support customers of OpenSSL 1.0.2 should upgrade
    to 1.0.2y. Other users should upgrade to 1.1.1j. Fixed in OpenSSL 1.1.1j
    (Affected 1.1.1-1.1.1i). Fixed in OpenSSL 1.0.2y (Affected
    1.0.2-1.0.2x).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23841">CVE-2021-23841</a>

    <p>The OpenSSL public API function X509_issuer_and_serial_hash() attempts
    to create a unique hash value based on the issuer and serial number data
    contained within an X509 certificate. However it fails to correctly handle
    any errors that may occur while parsing the issuer field (which might occur
    if the issuer field is maliciously constructed). This may subsequently
    result in a NULL pointer deref and a crash leading to a potential
    denial of service attack.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.1.0l-1~deb9u3.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2563.data"
# $Id: $
