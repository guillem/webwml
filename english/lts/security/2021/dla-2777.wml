<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were found in TIFF, a widely used format for
storing image data, as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19131">CVE-2020-19131</a>

    <p>Buffer Overflow in LibTiff allows attackers to cause
    a denial of service via the "invertImage()" function
    in the component <q>tiffcrop</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19144">CVE-2020-19144</a>

    <p>Buffer Overflow in LibTiff allows attackers to cause
    a denial of service via the <q>in _TIFFmemcpy</q> funtion
    in the component <q>tif_unix.c</q>.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.0.8-2+deb9u7.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>For the detailed security status of tiff please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tiff">https://security-tracker.debian.org/tracker/tiff</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2777.data"
# $Id: $
