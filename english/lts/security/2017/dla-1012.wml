<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Versions of Puppet prior to 4.10.1 will deserialize data off the wire
(from the agent to the server, in this case) with a attacker-specified
format. This could be used to force YAML deserialization in an unsafe
manner, which would lead to remote code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.7.23-1~deb7u4, by enabling PSON serialization on clients and refusing
non-PSON formats on the server.</p>

<p>We recommend that you upgrade your puppet packages. Make sure you
update all your clients before you update the server otherwise older
clients won't be able to connect to the server.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1012.data"
# $Id: $
