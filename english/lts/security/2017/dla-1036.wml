<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in gsoap, a library for the development
of SOAP web services and clients, that may be exposed with a large and
specific XML message over 2 GB in size. After receiving this 2 GB
message, a buffer overflow can cause an open unsecured server to crash.
Clients communicating with HTTPS with trusted servers are not affected.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.8.7-2+deb7u1.</p>

<p>We recommend that you upgrade your gsoap packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1036.data"
# $Id: $
