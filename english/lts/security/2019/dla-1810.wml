<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Nightwatch Cybersecurity Research team identified a XSS vulnerability
in tomcat7. The SSI printenv command echoes user provided data without
escaping. SSI is disabled by default. The printenv command is intended
for debugging and is unlikely to be present in a production website.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7.0.56-3+really7.0.94-1.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1810.data"
# $Id: $
