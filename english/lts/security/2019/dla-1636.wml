<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that aria2 (the lightweight command-line download
utility) can store passed user credentials in a log file when using
the --log option. This might allow local users to obtain sensitive
information by reading this file.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.18.8-1+deb8u1.</p>

<p>We recommend that you upgrade your aria2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1636.data"
# $Id: $
