<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A stack overflow vulnerability was discovered in AGG,
the AntiGrain Geometry graphical toolkit, that may lead to code
execution if a malformed file is processed. Since AGG only provides a
static library, the desmume and exactimage packages were rebuilt
against the latest security update.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.5+dfsg1-9+deb8u1.</p>

<p>We recommend that you upgrade your agg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1656.data"
# $Id: $
