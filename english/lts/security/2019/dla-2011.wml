<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue in xmlrpc-epi, an XML-RPC request serialisation/deserialisation
library, has been found.</p>

<p>An integer signedness error in the simplestring_addn function in
simplestring.c in xmlrpc-epi could be used for a heap based buffer
overflow and possibly execution of arbitrary code.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.54.2-1.1+deb8u1.</p>

<p>We recommend that you upgrade your xmlrpc-epi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2011.data"
# $Id: $
