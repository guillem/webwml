<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial of service vulnerability in
<tt>ruby-websocket-extensions</tt>, a library for managing long-lived HTTP
"WebSocket" connections.</p>

<p>The parser took quadratic time when parsing a header containing an unclosed
string parameter value whose content is a repeating two-byte sequence. This
could be abused by an attacker to conduct a Regex Denial Of Service (ReDoS) on
a single-threaded server by providing a malicious payload in the
<tt>Sec-WebSocket-Extensions</tt> HTTP header.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7663">CVE-2020-7663</a>

    <p>websocket-extensions ruby module prior to 0.1.5 allows Denial of Service
    (DoS) via Regex Backtracking. The extension parser may take quadratic time
    when parsing a header containing an unclosed string parameter value whose
    content is a repeating two-byte sequence of a backslash and some other
    character. This could be abused by an attacker to conduct Regex Denial Of
    Service (ReDoS) on a single-threaded server by providing a malicious
    payload with the Sec-WebSocket-Extensions header.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.1.2-1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-websocket-extensions packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2334.data"
# $Id: $
