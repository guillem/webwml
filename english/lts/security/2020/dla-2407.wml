<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in Apache Tomcat 8, the Java
application server. An excessive number of concurrent streams could have
resulted in users seeing responses for unexpected resources.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13943">CVE-2020-13943</a>

    <p>If an HTTP/2 client connecting to Apache Tomcat 10.0.0-M1 to 10.0.0-M7,
    9.0.0.M1 to 9.0.37 or 8.5.0 to 8.5.57 exceeded the agreed maximum number of
    concurrent streams for a connection (in violation of the HTTP/2 protocol),
    it was possible that a subsequent request made on that connection could
    contain HTTP headers - including HTTP/2 pseudo headers - from a previous
    request rather than the intended headers. This could lead to users seeing
    responses for unexpected resources.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
8.5.54-0+deb9u4.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2407.data"
# $Id: $
