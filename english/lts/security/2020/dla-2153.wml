<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVE(s) were reported against jackson-databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10672">CVE-2020-10672</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.aries.transaction.jms.internal.XaPooledConnectionFactory
    (aka aries.transaction.jms).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10673">CVE-2020-10673</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    com.caucho.config.types.ResourceRef (aka caucho-quercus).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u13.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2153.data"
# $Id: $
