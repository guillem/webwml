<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple security issues have been found in the Mozilla Firefox web
browser, which could potentially result in the execution of arbitrary
code or a timing attack on cryptographic keys.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
68.9.0esr-1~deb8u2.
<p><b>Note</b>: 68.9.0esr-1~deb8u2 fixes an i386 build error in the otherwise
      identical 68.9.0esr-1~deb8u1 that was uploaded but not announced.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2243.data"
# $Id: $
