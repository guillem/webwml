<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in dompurify.js, an XSS sanitizer for HTML,
MathML and SVG.
Both issues are related to mXSS issues in SVG- or MATH-elements.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.8.2~dfsg1-1+deb9u1.</p>

<p>We recommend that you upgrade your dompurify.js packages.</p>

<p>For the detailed security status of dompurify.js please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dompurify.js">https://security-tracker.debian.org/tracker/dompurify.js</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2419.data"
# $Id: $
