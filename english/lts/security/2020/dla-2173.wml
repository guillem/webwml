<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in graphicsmagick, a collection of image
processing tools, that results in a heap overflow in 32-bit applications
because of a signed overflow on range check in the HuffmanDecodeImage
function.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.20-3+deb8u10.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2173.data"
# $Id: $
