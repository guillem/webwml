<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following CVEs were reported against src:ruby-rack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8161">CVE-2020-8161</a>

    <p>A directory traversal vulnerability exists in rack < 2.2.0 that
    allows an attacker perform directory traversal vulnerability in
    the Rack::Directory app that is bundled with Rack which could
    result in information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8184">CVE-2020-8184</a>

    <p>A reliance on cookies without validation/integrity check security
    vulnerability exists in rack < 2.2.3, rack < 2.1.4 that makes it
    is possible for an attacker to forge a secure or host-only cookie
    prefix.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.6.4-4+deb9u2.</p>

<p>We recommend that you upgrade your ruby-rack packages.</p>

<p>For the detailed security status of ruby-rack please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-rack">https://security-tracker.debian.org/tracker/ruby-rack</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2275.data"
# $Id: $
