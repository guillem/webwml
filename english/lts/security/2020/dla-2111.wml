<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that jackson-databind, a Java library used to parse JSON and
other data formats, could deserialize data without proper validation,
allowing a maliciously client to perform remote code execution on a
service with the required characteristics.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u11.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2111.data"
# $Id: $
