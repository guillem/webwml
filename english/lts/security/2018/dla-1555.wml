<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18584">CVE-2018-18584</a>
      <p>Fixing the size of the CAB block input buffer, which is too small
      for the maximal Quantum block, prevents an out-of-bounds write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18585">CVE-2018-18585</a>

      <p>Blank filenames (having length zero or their 1st or 2nd byte is
      null) should be rejected.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.5-1+deb8u3.</p>

<p>We recommend that you upgrade your libmspack packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1555.data"
# $Id: $
