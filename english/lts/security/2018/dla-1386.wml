<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Ming:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7866">CVE-2018-7866</a>

    <p>NULL pointer dereference in the newVar3 function (util/decompile.c).
    Remote attackers might leverage this vulnerability to cause a denial
    of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7873">CVE-2018-7873</a>

    <p>Heap-based buffer overflow vulnerability in the getString function
    (util/decompile.c). Remote attackers might leverage this vulnerability
    to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7876">CVE-2018-7876</a>

    <p>Integer overflow and resulting memory exhaustion in the
    parseSWF_ACTIONRECORD function (util/parser.c). Remote attackers might
    leverage this vulnerability to cause a denial of service via a crafted
    swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9009">CVE-2018-9009</a>

    <p>Various heap-based buffer overflow vulnerabilites in util/decompiler.c.
    Remote attackers might leverage this vulnerability to cause a denial of
    service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9132">CVE-2018-9132</a>

    <p>NULL pointer dereference in the getInt function (util/decompile.c).
    Remote attackers might leverage this vulnerability to cause a denial
    of service via a crafted swf file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.4.4-1.1+deb7u9.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1386.data"
# $Id: $
