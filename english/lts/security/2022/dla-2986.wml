<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Go programming
language. An attacker could trigger a denial-of-service (DoS) or
invalid cryptographic computation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23772">CVE-2022-23772</a>

    <p>Rat.SetString in math/big has an overflow that can lead to
    Uncontrolled Memory Consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23806">CVE-2022-23806</a>

    <p>Curve.IsOnCurve in crypto/elliptic can incorrectly return true in
    situations with a big.Int value that is not a valid field element.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24921">CVE-2022-24921</a>

    <p>regexp.Compile allows stack exhaustion via a deeply nested
    expression.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.8.1-1+deb9u5.</p>

<p>We recommend that you upgrade your golang-1.8 packages.</p>

<p>For the detailed security status of golang-1.8 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.8">https://security-tracker.debian.org/tracker/golang-1.8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2986.data"
# $Id: $
