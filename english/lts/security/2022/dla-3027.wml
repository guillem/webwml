<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the previous upload to neutron to Debian 9 "Stretch"
(ie. version 2:9.1.1-3+deb9u2) was incomplete and did not actually apply the
fix for CVE-2021-40085.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40085">CVE-2021-40085</a>

	<p>An issue was discovered in OpenStack Neutron before 16.4.1, 17.x
	before 17.2.1, and 18.x before 18.1.1. Authenticated attackers can
	reconfigure dnsmasq via a crafted extra_dhcp_opts value.</p></li>    

</ul>

<p>For Debian 9 <q>Stretch</q>, this has now been fixed in version
2:9.1.1-3+deb9u3.</p>

<p>We recommend that you upgrade your neutron packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3027.data"
# $Id: $
