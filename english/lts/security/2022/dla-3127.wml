<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libhttp-daemon-perl, a simple http server
class.
Due to insufficient Content-Length: handling in HTTP-header an attacker
could gain privileged access to APIs or poison intermediate caches.</p>


<p>For Debian 10 buster, this problem has been fixed in version
6.01-3+deb10u1.</p>

<p>We recommend that you upgrade your libhttp-daemon-perl packages.</p>

<p>For the detailed security status of libhttp-daemon-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libhttp-daemon-perl">https://security-tracker.debian.org/tracker/libhttp-daemon-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3127.data"
# $Id: $
