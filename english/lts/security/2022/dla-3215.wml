<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Team discovered a race condition in the snapd-confine
binary which could result in local privilege escalation.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.37.4-1+deb10u2.</p>

<p>We recommend that you upgrade your snapd packages.</p>

<p>For the detailed security status of snapd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/snapd">https://security-tracker.debian.org/tracker/snapd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3215.data"
# $Id: $
