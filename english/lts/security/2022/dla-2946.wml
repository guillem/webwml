<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jeremy Mousset discovered two XML parsing vulnerabilities in the Tryton
application platform, which may result in information disclosure or
denial of service.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
4.2.0-1+deb9u1.</p>

<p>We recommend that you upgrade your tryton-proteus packages.</p>

<p>For the detailed security status of tryton-proteus please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tryton-proteus">https://security-tracker.debian.org/tracker/tryton-proteus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2946.data"
# $Id: $
