#use wml::debian::template title="Debian &ldquo;trixie&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"

<if-stable-release release="trixie">

<p>Debian <current_release_trixie> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_trixie/>"><current_release_date_trixie></a>.
<ifneq "13.0" "<current_release>"
  "Debian 13.0 werd oorspronkelijk uitgebracht op <:=spokendate('XXXXXXXX'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen,
beschreven in ons
our <a href="$(HOME)/News/XXXX/XXXXXXXX">persbericht</a> en de
the <a href="releasenotes">Notities bij de release</a>.</p>

#<p><strong>Debian 13 werd vervangen door
#<a href="../forky/">Debian 14 (<q>forky</q>)</a>.
#Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### Deze paragraaf is indicatief; kijk ze na voor publicatie!
#<p><strong>Trixie geniet evenwel van langetermijnondersteuning (Long Term Support - LTS) tot
#eind xxxxx 20xx. De LTS is beperkt tot i386, amd64, armel, armhf en arm64.
#Alle andere architecturen worden niet langer ondersteund in trixie.
#Raadpleeg voor meer informatie de <a
#href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
#</strong></p>

<p>Raadpleeg de <a href="debian-installer/">installatie-informatie</a>-pagina
en de <a href="installmanual">Installatiehandleiding</a> over het verkrijgen
en installeren van Debian. Zie de instructies in de
<a href="releasenotes">Notities bij de release</a> om van een oudere Debian
release op te waarderen.</p>

### Wat volgt activeren wanneer de LTS-periode begint.
#<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Ondersteunde computerarchitecturen bij de initiële release van trixie:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a>
gemaakt en u kunt ons altijd
<a href="../reportingbugs">andere problemen rapporteren</a>.</p>

<p>Tot slot, maar niet onbelangrijk, een overzicht van de
<a href="credits">mensen</a> die deze release mogelijk maakten.</p>
</if-stable-release>

<if-stable-release release="bookworm">

<p>De codenaam voor de volgende hoofdrelease van Debian na <a
href="../bookworm/">bookworm</a> is <q>trixie</q>.</p>

<p>Deze release startte als een kopie van bookworm en is momenteel in een
toestand genaamd
<q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>.
Dit betekent dat problemen niet zo erg zouden mogen zijn als die in de
onstabiele of de experimentele distributie, omdat pakketten enkel toegelaten
worden tot deze distributie na een bepaalde periode en wanneer er geen
release-kritieke bugs voor gerapporteerd zijn.</p>

<p>Merk op dat beveiligingsupdates voor de <q>testing</q>-distributie nog
<strong>niet</strong> beheerd worden door het beveiligingsteam. Dus,
<q>testing</q> krijgt <strong>niet</strong> snel beveiligingsupdates.
# Zie voor meer informatie de
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">aankondiging</a>
# van het beveiligingsteam voor Testing.
We raden u aan om voorlopig uw sources.list-regels te veranderen van testing
naar bookworm als u beveilingsondersteuning nodig heeft. Zie ook de vermelding in de
<a href="$(HOME)/security/faq#testing">FAQ van het Beveiligingsteam</a>
over de <q>testing</q>-distributie.</p>

<p>Mogelijk is een <a href="releasenotes">ontwerp van de Notities bij
de release beschikbaar</a>.
Raadpleeg ook <a href="https://bugs.debian.org/release-notes">de voorgestelde
aanvullingen voor de Notities bij de release</a>.</p>

<p>Zie <a href="$(HOME)/devel/debian-installer/">de pagina van het Debian
installatiesysteem</a> voor installatie-images en documentatie over hoe
<q>testing</q> te installeren.</p>

<p>>Om meer te weten over hoe de <q>testing</q>-distributie werkt, kunt u
<a href="$(HOME)/devel/testing">de ontwikkelaarsinformatie daarover</a>
raadplegen.</p>

<p>Mensen vragen dikwijls of er één enkele release-<q>voortgangsmeter</q> is.
Spijtig genoeg is die er niet, maar we kunnen u naar verschillende plaatsen
verwijzen waar beschreven staat wat er moet gebeuren vóór een release kan
worden uitgebracht:</p>

<ul>
  <li><a href="https://release.debian.org/">Algemene status van de release</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Release-kritieke bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bugs in het basissysteem</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs in standaard- en taakpakketten</a></li>
</ul>

<p>>Daarnaast rapporteert de releasemanager regelmatig over de algemene status
op de <a href="https://lists.debian.org/debian-devel-announce/">\
mailinglijst debian-devel-announce</a>.</p>

</if-stable-release>

<if-stable-release release="bullseye">

<p>De codenaam voor de volgende hoofdrelease van Debian na <a
href="../bookworm/">bookworm</a> is <q>trixie</q>. Momenteel werd
<q>bookworm</q> nog niet uitgebracht. Dus is <q>trixie</q> nog ver weg.</p>

</if-stable-release>
