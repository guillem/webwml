#use wml::debian::cdimage title="Debian cd/dvd-images downloaden via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="c64bdf15d82617c46942470ac0846ed7ec8b0f74"

# Last Translation Update by: $Author$
# Last Translation Update at: $Date$


<div class="tip">
<p><strong>Download cd/dvd-images alstublieft niet met uw browser op de
manier waarop u andere bestanden downloadt!</strong> De reden
daarvoor is dat als de download misgaat en halverwege stopt, de meeste
browsers u niet de mogelijkheid bieden verder te gaan met de download
vanaf het punt waar u gebleven was.</p>
</div>

<p>Gebruik in plaats van uw browser dus liever een programma dat
hervatting van afgebroken downloads toestaat - meestal omschreven als een
<q>downloadmanager</q>. Er zijn veel browserplug-ins die dit doen, maar
misschien wilt u liever een apart programma installeren. Onder Linux/Unix kunt u
gebruik maken van <a href="https://aria2.github.io/">aria2</a>,
<a href="https://sourceforge.net/projects/dfast/">wxDownload Fast</a> of
(vanaf the commandoregel)
<q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> of
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>.
Veel meer mogelijkheden worden vermeld in een
<a href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">
vergelijking van downloadprogramma's</a>.</p>

<p>De volgende Debian-images zijn beschikbaar om te
downloaden:</p>

<ul>

  <li><a href="#stable">Officiële cd/dvd-images voor de <q>stable</q>
  (stabiele) release</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Officiële cd/dvd-images voor de distributie <q>testing</q> distribution (<em>wekelijks opnieuw aangemaakt</em>)</a></li>

</ul>

<p>Zie ook:</p>
<ul>

  <li>Een volledige <a href="#mirrors">lijst met
  <tt>debian-cd/</tt>-spiegelservers</a></li>

  <li>Voor <q>netwerkinstallatie</q>-images,
  zie de pagina
  <a href="../netinst/">netwerkinstallatie</a>.</li>

  <li>Voor images van de release <q>testing</q>,
  zie de pagina over
  het <a href="$(DEVEL)/debian-installer/">Debian-Installatiesysteem</a>.</li>

</ul>

<hr />

<h2><a name="stable">Officiële cd/dvd-images voor de <q>stable</q> (stabiele)
    release</a></h2>

<p>Voor de installatie van Debian op een machine zonder (snelle)
Internetverbinding kunt u gebruik maken van cd-images (elk 700&nbsp;MB) of
dvd-images (elk 4.7&nbsp;GB). Download het eerste cd- of dvd-imagebestand,
brandt het met behulp van een cd/dvd-schrijver (of zet het eventueel op een
USB-stick voor de architecturen i386 en amd64) en herstart daarna uw systeem
vanaf dat medium.</p>

<p>Het <strong>eerste</strong> cd/dvd-image bevat alle bestanden die nodig
zijn voor de installatie van een standaard Debian systeem.<br />
Download om onnodige belasting van onze servers te voorkomen alstublieft
<strong>geen</strong> andere cd- of dvd-imagebestanden, tenzij u weet dat u de
pakketten daarop nodig heeft.</p>

<div class="line">
<div class="item col50">
<p><strong>cd</strong></p>

<p>Via onderstaande links vindt u imagebestanden tot een grootte
van 700&nbsp;MB geschikt voor het beschrijven van normale cd-r(w)-media:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>dvd</strong></p>

<p>Via onderstaande links vindt u imagebestanden tot een grootte
van 4.7&nbsp;GB geschikt voor het beschrijven van normale dvd-r/dvd+r en
vergelijkbare media:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Bekijk ook de beschikbare documentatie voor u een installatie begint.
<strong>Als u slechts één document wilt lezen</strong> voor u met de
installatie begint, lees dan onze
<a href="$(HOME)/releases/stable/i386/apa">Installatie Howto</a>, een kort
overzicht van het installatieproces. Andere nuttige documentatie:
</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installatiehandleiding</a>,
    bevat gedetailleerde installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentatie over het
    Debian Installatiesysteem</a>, waaronder antwoorden op
    veel gestelde vragen (FAQ)</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata bij het
    Debian Installatiesysteem</a>, een overzicht van bekende problemen
    in het installatiesysteem</li>
</ul>

<hr />

<h2><a name="mirrors">Geregistreerde spiegelservers voor het
    <q>debian-cd</q>-archief</a></h2>

<p>Merk op dat <strong>sommige spiegelservers niet up-to-date zijn</strong>
&mdash; controleer daarom voor het downloaden of het versienummer van de
images hetzelfde is als het versienummer zoals vermeld <a
href="../#latest">op deze pagina</a>.
Merk ook op dat vele sites niet alle images dupliceren (vooral de dvd-images niet)
omwille van hun grootte.</p>

<p><strong>Gebruik bij twijfel de <a href="https://cdimage.debian.org/debian-cd">
primaire cd-imageserver</a> in Zweden.</strong> of probeer het met
<a href="https://debian-cd.debian.net/">het experimentele systeem van
automatische spiegelserverselectie</a>, dat u automatisch zal doorverwijzen
naar een spiegelserver in de buurt waarvan bekend is dat hij de huidige
versie bevat.</p>

<p>Bent u geïnteresseerd in het aanbieden van Debian cd-images op
uw spiegelserver? Zo ja, bekijk dan <a href="../mirroring/">de instructies voor
het opzetten van een cd-imagespiegelserver</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
