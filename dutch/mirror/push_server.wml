#use wml::debian::template title="Een push-server opzetten"
#use wml::debian::toc
#use wml::debian::translation-check translation="8af469f41d65d1e2618d91408e57a4313cf98f8b"

<p>Het opzetten van een push-server bestaat uit twee basistaken: het instellen
van rsync-toegang (voor het normale <q>pull</q>-spiegelen) en het instellen van
het ssh-triggermechanisme (voor het <q>pushen</q> van het pull-spiegelen).
</p>

<p><small>(Lees voor meer informatie over wat een push-server is,
<a href="push_mirroring">de uitleg over push-spiegelen</a>.)</small>
</p>

<toc-display />

<toc-add-entry name="rsync">rsync opzetten</toc-add-entry>

<p>Installeer <code>rsync</code>. Indien uw site Debian gebruikt, moet u gewoon
het pakket <a href="https://packages.debian.org/stable/rsync">rsync</a>
installeren.
</p>

<p>Maak het bestand <code>rsyncd.conf</code> aan en zet er iets als dit in:
</p>

<pre>
uid = nobody
gid = nogroup
max connections = 50
socket options = SO_KEEPALIVE

[debian]
  path = /srv/debian/mirror
  comment = Het Debian-archief (https://www.debian.org/mirror/size)
  auth users = *
  read only = true
  secrets file = /etc/rsyncd/debian.secrets
</pre>

<p>Voeg in het bestand <code>/etc/rsyncd/debian.secrets</code> een item toe
voor elke site waarnaar u pusht:
</p>

<pre>
site1.example.com:een_wachtwoord
site2.example.com:nog_een_wachtwoord
site3.example.net:wachtwoord
</pre>

<p>U hebt nu de benedenstroomse spiegelservers toegang gegeven tot het archief
op uw machine. Indien u iedereen rsync-toegang wilt verlenen, sla dan het
instellen van <code>auth users</code> en <code>secrets file</code> over in
<code>rsyncd.conf</code>. U heeft dan ook geen secrets-bestand nodig.
</p>

<p>U zult waarschijnlijk de rsync-achtergronddienst willen starten vanuit inetd.
Om de achtergronddienst in te schakelen, voegt u het volgende toe aan uw
bestand <code>/etc/inetd.conf</code>:
</p>

<pre>
rsync      stream      tcp         nowait      root /usr/bin/rsync rsyncd --daemon
</pre>

<p>
(Vergeet niet om inetd een HUP-signaal te sturen om het te vertellen dat het
zijn configuratiebestand opnieuw moet lezen nadat het bestand is gewijzigd.)
</p>

<toc-add-entry name="sshtrigger">Het ssh-triggermechanisme opzetten</toc-add-entry>

<p>Maak met behulp van <code>ssh-keygen</code> een nieuwe ssh-sleutel aan voor
het account dat u gebruikt om Debian te spiegelen. Als u met uw account al een
sleutel heeft voor andere doeleinden, wilt u misschien een nieuwe maken en deze
opslaan in een ander bestand met behulp van
<code>ssh-keygen -f ~/.ssh/identiteit.mijnsite</code></p>

<p>
Uw benedenstroomse spiegelservers moeten dan aan hun bestand
<code>~/.ssh/authorized_keys</code> het volgende toevoegen:
</p>
<pre>
command="~/bin/ftpsync",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty &lt;inhoud van uw bestand ~/.ssh/&lt;key&gt;.pub&gt;
</pre>

<p>U moet uw spiegelingsproces zo instellen dat het contact opneemt met uw
benedenstroomse spiegelservers wanneer het klaar is met zijn spiegelingstaak.
De suite ftpsync bevat het script <code>runmirrors</code> dat alle benodigde
taken voor u afhandelt. U dient gewoon uw bestand ftpsync.conf aan te passen,
zodat het de instelling <code>HUB=true</code> bevat, het bestand
<code>runmirrors.conf.sample</code> te kopiëren
naar <code>runmirrors.conf</code>
en <code>runmirrors.mirror.sample</code> te kopiëren naar
<code>runmirrors.mirror</code>, en het configuratiebestand zo te configureren
dat het aangepast is aan uw systeem. Daarna moet u al uw benedenstroomse
spiegelservers vermelden in runmirrors.mirror, en het duo ftpsync/runmirror zal
al het zware werk voor u doen.
</p>

<p>Het effect zal zijn dat uw systeem zal proberen om naar uw benedenstroomse
spiegelservers te ssh-en, nadat uw eigen spiegelserver bijgewerkt is, zodat zij
hun eigen updates kunnen starten. Dit veronderstelt dat u de operatoren van uw
benedenstroomse spiegelservers de ssh-sleutel gaf die u door runmirrors laat
gebruiken en dat ze die aan hun eigen ~/.ssh/authorized_keys toevoegden zoals
hierboven beschreven.
</p>

<p>Mocht u hiermee problemen ervaren, <a href="mailto:mirrors@debian.org">\
neem dan contact op met ons</a>.
</p>
