#use wml::debian::template title="Mentorprogramma van het vrouwenproject van Debian"
#use wml::debian::translation-check translation="8a7389809695b9d1808cb8a306cede14fe193209"

## updated as for 11/2012

<h2>Welkom bij het mentorprogramma!</h2>

<p>
Het doel van het mentorprogramma is om iedereen die wil bijdragen aan Debian (maar niet zeker weet waar of hoe te beginnen) te ondersteunen en te begeleiden bij hun eerste stappen.
</p>

<h3>Wat bedoelen we met <q>mentorschap</q>?</h3>

<p>
De mentoren die betrokken zijn bij het programma, zijn mensen die ervaring hebben in bepaalde met Debian verband houdende vaardigheden, en die graag andere mensen willen helpen om hun vaardigheden in die gebieden te verbeteren. We verwachten dat mentorschap voornamelijk zal plaatsvinden in 1:1-relaties die een vrij brede reeks activiteiten kunnen omvatten, zoals de volgende:

<ul>
            <li>
                de mentor geeft de leerling technische assistentie om een geplande taak uit te voeren die bijdraagt aan Debian
            </li>
            <li>
                de mentor helpt de leerling bij de interactie binnen het Debian-project op manieren die consistent zijn met het verwezenlijken van de geplande taken
            </li>
            <li>
                de mentor helpt de leerling bij het vinden van documentatie die relevant is voor de geplande taken
            </li>
            <li>
                de mentor introduceert de leerling in aspecten van de Debian-gemeenschap en -cultuur, in het bijzonder die aspecten die betrekking hebben op de geplande taak
            </li>
        </ul>

<p>
Het is belangrijk om te begrijpen dat mentoren niet bedoeld zijn om het eigenlijke werk te doen dat komt kijken bij het oplossen van het probleem waar de leerling aan werkt. Dat zou indruisen tegen het doel van het programma, namelijk om leerlingen de kans te geven nieuwe vaardigheden te leren en hun interactie met en bijdragen aan Debian te vergroten. Er moet ook worden opgemerkt dat een mentor niet noodzakelijk in staat (of bereid) zal zijn om het pakket van een leerling te sponsoren, zelfs als de mentor een Debian-ontwikkelaar is en het overeengekomen project verpakking betreft.
</p>

<h3>Hoe het programma werkt</h3>


<p>
Potentiële mentoren kunnen een e-mail sturen naar <a
href="mailto:mentoring@women.debian.org">mentoring@women.debian.org</a>.
We hebben grote waardering voor de mensen die bereid zijn tijd vrij te maken om anderen op deze manier te helpen, en we hopen dat u het interessant en de moeite waard zult vinden.
</p>

<p>
Potentiële leerlingen kunnen een e-mail sturen naar
<a href="mailto:mentoring@women.debian.org">mentoring@women.debian.org</a>
om gekoppeld te worden aan een potentiële mentor. Leg in uw e-mail uit in wat voor soort activiteiten u geïnteresseerd bent en wat voor soort hulp u zoekt. We zullen ons best doen om u te koppelen aan een mentor die geïnteresseerd is om u te helpen met deze dingen.
</p>

<p>
Zodra een mentor en een leerling aan elkaar gekoppeld zijn, is het in principe aan u om samen te beslissen wat u gaat doen. Houd er rekening mee dat we verwachten dat zowel de mentor als de leerling ten minste de tijd kunnen besteden die nodig is om contact te leggen en te bespreken wat u zinnens bent te gaan doen. Daarbuiten kunnen de tijdsbestedingen aanzienlijk verschillen. We hopen dat de leerlingen in staat zullen zijn om de hoeveelheid tijd te besteden die nodig is om de taken uit te voeren waarin ze geïnteresseerd zijn, en we hopen dat mentoren genoeg tijd zullen kunnen vrijmaken om hun leerlingen te helpen met hun taken. Maar rekening houdend met het feit dat het leven en de werkdruk van mensen kunnen veranderen, erkennen we dat mensen soms hun mate van betrokkenheid moeten aanpassen.
</p>

<p>
Als u problemen hebt met het mentorprogramma of met uw mentor of mentee, kunt u contact opnemen met <a href="mailto:mentoring@women.debian.org">mentoring@women.debian.org</a> en we zullen ons best doen om de zaken op orde te krijgen.
</p>
