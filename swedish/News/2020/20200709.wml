# Status: [open-for-edit]
# $Rev$
#use wml::debian::translation-check translation="9c0ebe940eaf29e78367427aea8e02f46fb70bcd"
<define-tag pagetitle>Debian 8 långtidsstöd (LTS) når slutet av sin livslängd</define-tag>
<define-tag release_date>2020-07-09</define-tag>
#use wml::debian::news

##
## Translators:
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>Gruppen för Debian långtidsstöd (Long Term Support - LTS) tillkännager
härmed att stödet för Debian 8 <q>Jessie</q> har avslutats den 30 juni
2020, fem år efter dess ursprungliga utgåva den 26 april, 2015.</p>

<p>Debian kommer inte att tillhandahålla ytterligare säkerhetsuppdateringar för
Debian 8. En delmängd av <q>Jessie</q>-paket kommer att stödjas av externa
parter. Detaljerad information kan hittas på sidan
<a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.</p>

<p>LTS-gruppen kommer att förbereda en övergång till Debian 9 <q>Stretch</q>,
som är den aktuella gamla stabila utgåvan. LTS-gruppen har tagit över stödet
från säkerhetsgruppen 6 Juli 2020, medan den slutliga punktutgåvan för
<q>Stretch</q> kommer att ges ut 18 Juli 2020.</p>

<p>Debian 9 kommer också att få långtidsstöd under fem år efter dess
ursprungliga utgåva, med stöd som avslutas 30 Juni 2022. Arkitekturerna
med stöd fortsätter att vara am64, i386, armel och armhf. Utöver detta är
vi stolta att tillkännage att för första gången kommer stöd att utökas med
att inkludera arkitekturen arm64.</p>

<p>För ytterligare information om användning av <q>Stretch</q> LTS och att
uppgradera från <q>Jessie</q> LTS, var vänlig se
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.</p>

<p>Debian och dess LTS-grupp skulle vilja tacka alla bidragande användare,
utvecklare och sponsorer som gör det möjligt att utöka livslängden för
tidigare stabila utgåvor, och som har gjort denna LTS till en framgång.</p>

<p>Om du är beroende av Debian LTS, överväg att 
<a href="https://wiki.debian.org/LTS/Development">gå med i gruppen</a>,
tillhandahåll patchar, testning eller
<a href="https://wiki.debian.org/LTS/Funding">finansiera insatserna</a>.</p>

<h2>Om Debian</h2>

<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella 
	operativsystemet</q>.
</p>

<h2>Ytterligare information</h2>
<p>Ytterligare information om Debians Långtidsstöd kan hittas på 
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, var vänlig besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på
engelska) till &lt;press@debian.org&gt;.</p>
