#use wml::debian::translation-check translation="90833ca5169a5ef4cdeac320dd3d7016a5d5f8d2" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>Данное обновление содержит обновлённый микрокод ЦП для некоторых типов ЦП Intel и
предоставляет возможность снижения риска от следующих уязвимостей обрудования:
Special Register Buffer Data Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>),
Vector Register Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>)
и L1D Eviction Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>).</p>

<p>Обновление микрокода для HEDT и ЦП Xeon с сигнатурой 0x50654, которое
было отменено в DSA 4565-2 теперь снова включено.</p>

<p>Обновление основной ветки для Skylake-U/Y (сигнатура 0x406e3) должно было
быть исключено из данного обновления из-за зависания при загрузке.</p>

<p>За подробностями обращайтесь по адресам
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html</a>,
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html</a></p>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 3.20200609.2~deb9u1.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 3.20200609.2~deb10u1.</p>

<p>Рекомендуется обновить пакеты intel-microcode.</p>

<p>С подробным статусом поддержки безопасности intel-microcode можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4701.data"
