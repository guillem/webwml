#use wml::debian::translation-check translation="28bc87857803972597b697f1aafdfc05773ea8db" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i mailserveren Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

    <p>Når imap-davle er aktiv, kunne en angriber (med gyldige loginoplysninger 
    til at tilgå mailserveren) få Dovecot til at opdage filsystemets 
    mappestrukturer og tilgå andre brugeres mail gennem særligt fremstillede 
    kommandoer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

    <p>Innokentii Sennovskiy rapporterede at mailaflevering og -fortolkning i 
    Dovecot kunne gå ned, når den 10000. MIME-del var en message/rfc822 (eller 
    hvis ophavet var multipart/digest).  Fejlen blev indført af tidligere 
    ændringer, som løste 
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">\
    CVE-2020-12100</a>.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1:2.3.4.1-5+deb10u5.</p>

<p>Vi anbefaler at du opgraderer dine dovecot-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende dovecot, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4825.data"
