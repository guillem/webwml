#use wml::debian::translation-check translation="d3c36081141f159ff0a43ff0c5bf3e0e2f57fde3" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>To sikkerhedsproblemer blev opdaget i Jetty, en Java-servlet-motor og 
-webserver.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2047">CVE-2022-2047</a>

    <p>I Eclipse Jetty, ved fortolkning af autoritetssegementet hørende til en 
    http-scheme-URI, genkendte Jetty HttpURI-klassen fejlagtigt ugyldigt inddata 
    som et værtsnavn.  Det kunne føre til fejl i et proxyscenarie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2048">CVE-2022-2048</a>

    <p>I Eclipse Jettys HTTP/2-serverimplementering, når der blev stødt på en 
    ugyldig HTTP/2-forespørgsel, var der en fejl i fejlhåndteringen, der kunne 
    medføre at de aktive forbindelser og tilhørende ressourcer ikke blev ryddet 
    op på korrekt vis.  Det kunne medføre et lammelsesangreb, hvor der ikke var 
    tilstrækkeligt med ressourcer tilbage til at behandle korrekte 
    forespørgsler.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 9.4.39-3+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine jetty9-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende jetty9, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5198.data"
