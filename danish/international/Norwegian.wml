#use wml::debian::template title="Debians norske hjørne"
#use wml::debian::translation-check translation="ae7f0ab25ebcd74a2e63d1440ce5838d3dc673cb" original="norwegian"

<p>På denne side kan du finde oplysninger for norske brugere af Debian.  Hvis 
du synes noget specielt hører til her, så skriv gerne til en af de norske 
<a href="#translators">oversætterne</a>.</p>


<h2>Postlister</h2>

<p>Debian har for tiden ingen officielle postlister på norsk. Hvis der er 
interesse for det, kan vi starte en eller flere for brugere og/eller 
udviklere.</p>


<h2>Links</h2>

<p>Nogle links som kan være af interesse for norske Debian-brugere:</p>

<ul>
#   <li><a href="http://www.linux.no/">Linux Norge</a><br>
#    	<em>"En frivillig organisation som skal udbrede kendskabet til
#    	styresystemet Linux i Norge."</em></li>
    <li><a href="https://nuug.no/">Norwegian UNIX User Group</a>.  Kig især på 
	deres wikiside med <a href="https://wiki.nuug.no/likesinnede-oversikt">\
	oversigt over tilsvarende foreninger</a>, som opremser mange/alle aktive 
	Linux-brugergrupper.</li>
</ul>


<h2>Norske bidragsydere til Debian-projektet</h2>

<h3>Nuværende aktive norske Debian-udviklere:</h3>

<ul>
    <li>Lars Bahner &lt;<email bahner@debian.org>&gt;</li>
    <li>Morten Werner Forsbring &lt;<email werner@debian.org>&gt;</li>
    <li>Ove Kåven &lt;<email ovek@debian.org>&gt;</li>
    <li>Petter Reinholdtsen &lt;<email pere@debian.org>&gt;</li>
    <li>Ruben Undheim &lt;<email rubund@debian.org>&gt;</li>
    <li>Steinar H. Gunderson &lt;<email sesse@debian.org>&gt;</li>
    <li>Stein Magnus Jodal &lt;<email jodal@debian.org>&gt;</li>
    <li>Stig Sandbeck Mathisen &lt;<email ssm@debian.org>&gt;</li>
    <li>Tollef Fog Heen &lt;<email tfheen@debian.org>&gt;</li>
    <li>Øystein Gisnås &lt;<email shaka@debian.org>&gt;</li>
</ul>

<p>Denne liste opdateres uregelmæssigt, for at se de seneste oplysninger, bør du 
kigge i <a href="https://db.debian.org/">Debian Developers Database</a> - vælg
<q>Norway</q> som <q>country</q>.</p>


<h3>Tidligere norske Debian-udviklere:</h3>

<ul>
    <li>Morten Hustvei</li>
    <li>Ole J. Tetlie</li>
    <li>Peter Krefting</li>
    <li>Tom Cato Amundsen</li>
    <li>Tore Anderson</li>
    <li>Tor Slettnes</li>
</ul>

<a id="translators"></a>
<h3>Oversættere:</h3>

<p>Debians websider oversættes i øjeblikket til norsk af:</p>

<ul>
    <li>Hans F. Nordhaug &lt;<email hansfn@gmail.com>&gt;</li>
</ul>

#<p>Debian søger folk der kan oversætte websiderne til norsk.  
<p>Hvis du er interesseret, bør du begynde med at læse 
<a href="$(DEVEL)/website/translating">oplysningerne til oversættere</a>.</p>

<p>Debians websider blev tidligere oversat af:</p>

<ul>
    <li>Cato Auestad</li>
    <li>Stig Sandbeck Mathisen</li>
    <li>Tollef Fog Heen</li>
    <li>Tor Slettnes</li>
</ul>

<p>Hvis du vil hjælpe til, eller hvis du kender til andre som på en eller anden 
måde er involveret i Debian-projektet, så tag kontakt med en af os.</p>
