#use wml::debian::template title="Configurando um servidor push"
#use wml::debian::toc
#use wml::debian::translation-check translation="8af469f41d65d1e2618d91408e57a4313cf98f8b"

<p>Configurar um servidor push consiste em duas tarefas básicas: configurar o
acesso rsync (para um espelhamento normal <q>pull</q>) e configurar um mecanismo
de ativação ssh (para fazer <q>pushing</q> do espelhamento pull).
</p>

<p><small>(Para mais informações sobre o que é um servidor push, por favor leia
<a href="push_mirroring">a explicação sobre o espelhamento push</a>).</small>
</p>

<toc-display />

<toc-add-entry name="rsync">Configurando rsync</toc-add-entry>

<p>Instale o <code>rsync</code>. Se o seu site está rodando o Debian, somente
instale o pacote <a href="https://packages.debian.org/stable/rsync">rsync</a>.
</p>

<p>Crie o arquivo <code>rsyncd.conf</code> e insira alguma coisa similar a isto:
</p>

<pre>
uid = nobody
gid = nogroup
max connections = 50
socket options = SO_KEEPALIVE

[debian]
  path = /srv/debian/mirror
  comment = O repositório Debian (https://www.debian.org/mirror/size)
  auth users = *
  read only = true
  secrets file = /etc/rsyncd/debian.secrets
</pre>

<p>Adicione uma entrada para cada site que você vai fazer pushing no arquivo
<code>/etc/rsyncd/debian.secrets</code>:
</p>

<pre>
site1.example.com:uma_senha
site2.example.com:outra_senha
site3.example.net:senha
</pre>

<p>Você agora deu aos espelhos de destino o acesso ao repositório de sua
máquina. Se você desejar fornecer acesso rsync para todos, pule as configurações
<code>auth users</code> e <code>secrets file</code> em <code>rsyncd.conf</code>.
Você não precisa de um arquivo secrets.
</p>

<p>Você provavelmente vai querer iniciar o daemon rsync pelo inetd.
Para habilitar o daemon, adicione o seguinte no arquivo
<code>/etc/inetd.conf</code>:
</p>

<pre>
rsync      stream      tcp         nowait      root /usr/bin/rsync rsyncd --daemon
</pre>

<p>
(Lembre-se de enviar para o inetd um sinal HUP para informá-lo da releitura de
seu arquivo de configuração após modificar o arquivo).
</p>

<toc-add-entry name="sshtrigger">Configurando o mecanismo de ativação ssh</toc-add-entry>

<p>Crie uma nova chave ssh para a conta que você usa para espelhar o Debian
usando <code>ssh-keygen</code>. Se sua conta já possui uma chave para outros
propósitos, você pode querer criar uma nova e armazená-la em um arquivo
diferente usando <code>ssh-keygen -f ~/.ssh/identity.mysite</code></p>

<p>
Então seus espelhos de destino precisarão adicionar
</p>
<pre>
command="~/bin/ftpsync",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty &lt;conteúdo do seu arquivo ~/.ssh/&lt;key&gt;.pub&gt;
</pre>
<p>
nos seus arquivos <code>~/.ssh/authorized_keys</code>.
</p>

<p>Você precisa configurar seu processo de espelhamento para contatar seus
espelhos de destino quando sua execução de espelhamento tiver terminado.
A suíte ftpsync contém o script <code>runmirrors</code> que cuidará de todas as
tarefas para você. Simplesmente mude seu ftpsync.conf para incluir a
configuração <code>HUB=true</code>, copie <code>runmirrors.conf.sample</code>
para <code>runmirrors.conf</code> e <code>runmirrors.mirror.sample</code> para
<code>runmirrors.mirror</code> e configure o arquivo de configuração para se
adequar ao seu sistema. Então liste todos os seus espelhos de destino dentro de
runmirrors.mirror e a dupla ftpsync/runmirror fará todo o trabalho pesado para
você.
</p>

<p>O efeito será que seu sistema tentará fazer um ssh para seus espelhos de
destino, depois da atualização do seu próprio espelho, de modo que eles possam
fazer suas próprias atualizações. Assume-se que você tenha dado a chave ssh para
seus(as) operadores(as) de espelhos de destino, chave que você disse ao
runmirror para ser usada, e que eles adicionaram aos seus próprios arquivos
~/.ssh/authorized_keys como descrito acima.
</p>

<p>Se tiver qualquer problema,
<a href="mailto:mirrors@debian.org">entre em contato conosco</a>.
</p>
