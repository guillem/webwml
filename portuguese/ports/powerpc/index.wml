#use wml::debian::template title="Porte para PowerPC" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
#use wml::debian::toc
#use wml::debian::translation-check translation="bd1362b966297f3e701c0512bd20e9497fda943e"

<toc-display/>

<toc-add-entry name="about">Debian para PowerPC</toc-add-entry>

<p>
<img src="pics/ppc750.jpg" alt="PPC-750 picture" class="rightico">
 O PowerPC é uma arquitetura de microprocessador
 <a href="https://en.wikipedia.org/wiki/Reduced_instruction_set_computer">\
 RISC</a>
 desenvolvida pela <a href="https://www.ibm.com/it-infrastructure/power">IBM</a>,
 Motorola (agora <a href="http://www.freescale.com/">Freescale</a>) e <a
 href="https://www.apple.com/">Apple</a>.  A arquitetura PowerPC permite
 tanto implementações 64-bit como 32-bit (a implementação 64-bit inclui
 a implementação 32-bit). O primeiro microprocessador PowerPC foi o
 601, uma implementação 32-bit lançada em 1992. Várias outras implementações
 32-bit foram lançadas desde então, incluindo a 603, 604, 750
 (G3), 7400 (G4) e o processador de comunicação embarcado PowerQUICC.
 Implementações 64-bit incluem a 620, POWER4, POWER5
 e a 970 (G5).
</p>

<p>
 O Linux para o PowerPC foi primeiramente lançado na versão 2.2.x do
 kernel. Um recurso-chave do desenvolvimento do PowerPC Linux é o <a
 href="http://penguinppc.org/">penguinppc</a>, que também inclui
 uma lista de compatibilidade de hardware. O suporte ao PowerPC no
 kernel do Linux é desenvolvido agora como parte do kernel Linux "principal"
 em <a href="https://www.kernel.org/">kernel.org</a>.
</p>

<p>
 O porte Debian PowerPC começou em 1997 no <a href="http://www.linux-kongress.org/">
 German Linux Congress</a> em W&uuml;rzburg.
 <a href="http://www.infodrom.north.de/Infodrom/tervola.html">Uma
 máquina PowerPC</a> (Motorola StarMax 4000, 200 MHz 604e) foi doada para o
 projeto Debian. Informações adicionais sobre este computador estão disponíveis
 na <a href="history">página de histórico</a>.
</p>

<toc-add-entry name="powerpc">Debian no PowerPC 32-bit (powerpc)</toc-add-entry>

<p>
Ela se tornou uma <q>arquitetura lançada</q> oficialmente com o Debian
GNU/Linux2.2 (<q>potato</q>) e manteve este estado até a publicação
do Debian 9 (<q>stretch</q>). A última versão suportada para o PowerPC 32-bit
é o Debian 8 (<q>jessie</q>).

Veja a <a href="$(HOME)/releases/jessie/powerpc/release-notes/">\
nota de lançamento</a> e o <a href="$(HOME)/releases/jessie/powerpc/">\
manual de instalação</a> para mais informações.
</p>

<toc-add-entry name="ppc64el">Debian no PowerPC 64-bit Little Endian (ppc64el)</toc-add-entry>
<p>
Iniciando no lançamento do Debian 8 (<q>jessie</q>), o ppc64el é uma
arquitetura oficialmente suportada no Debian.

Veja a <a href="$(HOME)/releases/stable/ppc64el/release-notes/">\
nota de lançamento</a> e o <a href="$(HOME)/releases/stable/ppc64el/">\
manual de instalação</a>.
</p>

<p>
Aqui você pode encontrar informações sobre a arquitetura
<a href="https://en.wikipedia.org/wiki/Ppc64">PowerPC 64-bit</a>
<a href="https://en.wikipedia.org/wiki/Endianness">Little Endian</a>.
</p>
<p>
Contudo, note que também existem informações na página
<a href="https://wiki.debian.org/ppc64el">wiki do ppc64el</a>, como a
instalação e informação sobre ABI.
</p>

<toc-add-entry name="installation">Instalação</toc-add-entry>

<p>
 Existe uma variedade de sistemas usando o microprocessador
 PowerPC. Verifique nossas páginas de <a href="inst/install">instalação</a>
 para informações específicas sobre a instalação do Debian/PowerPC no seu
 sistema.
</p>


<p>
 Existem algumas peculiaridades que você precisa saber quando instalar no
 iBook, TiBook ou iBook2, especialmente quando em dual boot com o Mac
 OS X. Alguns hardwares iBook2, especialmente os modelos recentemente
 introduzidos, não são bem suportados ainda. Para informações específicas
 sobre potenciais problemas e soluções, estude essas páginas web:
</p>

<ul>
<li>de William R. Sowerbutts, <a
         href="http://www.sowerbutts.com/linux-mac-mini/">Instalando
         o Debian GNU/Linux no Mac Mini</a></li>
<li>de Mij, <a href="http://mij.oltrelinux.com/ibook/ppc_linux.html">\
       Dicas de instalação do Debian em um iBook2</a></li>
<li><a href="http://seb.france.free.fr/linux/ibookG4/iBookG4-howto.html">\
       Instalando o Debian GNU/Linux em um iBook 3.3 (G4)</a> por Sébastien FRANÇOIS</li>
<li><a href="https://lists.debian.org/debian-powerpc/2002/07/msg00858.html">\
      HOWTO de instalação do Debian via rede no IBM RS/6000 44P-170
      (POWER3)</a> por Rolf Brudeseth</li>
<li> Instalando o Debian GNU/Linux em um p630 LPAR (7028-6C4) - <a
    href="https://web.archive.org/web/20080625231946/http://people.debian.org/~fmw/p630-LPAR-Debian-en.txt">em inglês</a>
     e <a
    href="https://web.archive.org/web/20080916213451/http://people.debian.org/~fmw/p630-LPAR-Debian-de.txt">em alemão</a>
    (de Florian M. Weps, link para cópias arquivadas em archive.org)</li>
<li>de Daniel DeVoto,<a href="http://ppcluddite.blogspot.de/2012/03/installing-debian-linux-on-ppc-part-i.html">\
Instalando o Debian Wheezy/teste em um iBook G3</a></li>
</ul>

<p>
 O Debian GNU/Linux não suporta oficialmente máquinas PowerPC NuBus,
 como a 6100, 7100, 8100 e a maioria da série Performa.
 Entretanto, um kernel baseado em MkLinux está disponível, que pode ser
 lançado de um bootloader do Mac OS. Um sistema Debian pode ser instalado
 usando este kernel, que está disponível em
 <url "http://nubus-pmac.sourceforge.net/">.
</p>

<p>
 Quando atualizar do Potato para Woody, ou quando atualizar de kernels
 realmente antigos, existem algumas
 <a href="keycodes">informações importantes</a> que você deveria
 estar ciente sobre uma mudança na codificação de teclado. Isto pode te
 salvar muito tempo e dor de cabeça!
</p>

<toc-add-entry name="docs">Links - Documentação</toc-add-entry>

<p>Informação atualizada sobre o Linux para PowerPC está em
<a href="http://penguinppc.org/">PenguinPPC</a>.
Nós temos uma <a href="docu">coleção mais antiga de links sobre PowerPC</a> e
alguns <a href="devel">apontamentos</a> para desenvolvedores(as) do Debian
PowerPC.</p>

<p>Especificações de hardware para computadores Apple estão disponíveis em
<a href="https://support.apple.com/specs">AppleSpec</a>.</p>
<p>
Mais informações e documentações sobre a arquitetura POWER:
</p>
<ul>
<li><a href="https://www-03.ibm.com/technologyconnect/tgcm/TGCMServlet.wss?alias=OpenPOWER">
Especificação oficial ELFv2 ABI</a> (na seção 'Link')
ou este
<a href="https://www-03.ibm.com/technologyconnect/tgcm/TGCMFileServlet.wss/ABI64BitOpenPOWER_21July2014_pub.pdf?id=B81AEC1A37F5DAF185257C3E004E8845">
PDF</a>. É necessário se registrar. </li>
<li><a href="https://gcc.gnu.org/wiki/cauldron2014#Slides_and_Notes">GNU Tools
Caldron 2014</a> slides &amp; vídeo</li>
</ul>

<toc-add-entry name="availablehw">Hardware disponível para contribuidores(as)</toc-add-entry>

<p> Por favor, leia a <a href="https://db.debian.org/machines.cgi">lista de
máquinas Debian</a> para acessar o porterboxen Debian powerpc/ppc64el.
</p>

<toc-add-entry name="contact">Informação de contato</toc-add-entry>

<p>Se você precisar de ajuda, tente perguntar nos seguintes lugares:</p>

<h3>Listas de discussão</h3>

<p>A lista de discussão do Debian PowerPC é o lugar adequado para perguntas,
sugestões de melhorias ou somente para um bate-papo relativo aos sistemas
PowerPC rodando Debian. A lista debian-user também está disponível para
questões gerais sobre o Debian que não são específicas ao PowerPC.</p>

<p>Para ingressar na lista, mande um e-mail para
<a href="mailto:debian-powerpc-request@lists.debian.org">
&lt;debian-powerpc-request@lists.debian.org&gt;</a> com a palavra
"subscribe" na linha de assunto.
<a href="https://lists.debian.org/debian-powerpc/">Os arquivos da lista estão
disponíveis</a>.</p>

<h3>Newsgroups Usenet</h3>

<p>Linux para sistemas PowerPC</p>
<ul>
  <li><a href="news:comp.os.linux.powerpc">comp.os.linux.powerpc</a></li>
</ul>

<p>Linux em geral</p>
<ul>
  <li><a href="news:comp.os.linux.misc">comp.os.linux.misc</a></li>
  <li><a href="news:comp.os.linux.networking">comp.os.linux.networking</a></li>
  <li><a href="news:comp.os.linux.hardware">comp.os.linux.hardware</a></li>
  <li><a href="news:comp.os.linux.x">comp.os.linux.x</a></li>
</ul>

<h3>IRC</h3>

<p>O canal <code>#debian</code> no <code>irc.debian.org</code> é
para tópicos gerais relativos ao Debian, e existe o canal
<code>#debianppc</code> para tópicos específicos ao Debian em
processadores PowerPC. Você quase sempre vai encontrar alguém on-line que
ficará feliz em compartilhar informações e lhe ajudar com seu problema.</p>

<hr />

#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
