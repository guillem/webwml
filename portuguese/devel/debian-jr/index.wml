#use wml::debian::template title="Projeto Debian Jr."
#use wml::debian::translation-check translation="1df7fb6f20f7d03e33cbc3ede19249bfa322abe2"

<h2>Debian para crianças</h2>

<p>
Este é um <a href="https://www.debian.org/blends/">Debian Pure Blend</a>
(em resumo, Blend). Nosso objetivo é tornar o Debian um sistema operacional que
crianças de todas as idades queiram usar. Nosso foco inicial será produzir algo
para crianças de até 8 anos. Uma vez que tenhamos feito isso, nossa próxima
faixa etária alvo é de 7 a 12 anos. Quando as crianças atingirem a adolescência,
elas devem estar confortáveis em usar o Debian sem nenhuma modificação especial.
</p>

<h3>Lista de e-mails</h3>
<p>
O Debian configurou uma lista de e-mails para este grupo. Você pode
<a href="https://lists.debian.org/debian-jr/">se inscrever ou ler os arquivos da lista</a>.
</p>

<h3>Chat</h3>
<p>
Temos um canal de discussão em tempo real, #debian-jr no irc.debian.org e um
chat multiusuário XMPP
<a href="xmpp:debian-jr@conference.debian.org?join">debian-jr@conference.debian.org</a>.
</p>

<h3>Notícias</h3>
<p>
Temos um <a href="https://debian-jr-team.pages.debian.net/blog/">blog da equipe</a>
para atualizações e notícias do projeto.
</p>

<h3>Website da comunidade</h3>
<p>
Temos um
<a href="https://wiki.debian.org/Teams/DebianJr">website da comunidade Debian Jr.</a>
onde desenvolvedores(as) e usuários(as) são encorajados(as) a contribuir para o
projeto Debian Jr.
</p>

<h3>O que eu posso fazer para ajudar?</h3>
<p>
Estamos interessados em ouvir o que você acha que o Debian Jr. poderia estar
fazendo, especialmente se você gostaria de ajudar a fazer isso acontecer.
</p>

<h3>Instalação</h3>
<p>
O pacote <a href="https://packages.debian.org/junior-doc">junior-doc</a> contém
o guia rápido.
</p>

<h3>Pacotes no Debian Jr.</h3>

<p>
Os Debian Pure Blends estão criando
<a href="https://blends.debian.org/junior/tasks/">descrições dos pacotes</a>
que são interessantes para o grupo de usuários(as) alvo.
</p>


<h3>Objetivos do Debian Jr.</h3>

<h4>Tornando o Debian desejável para crianças</h4>

<p>
O objetivo principal do projeto Debian Jr. é tornar o Debian um sistema
operacional que nossas crianças <i>queiram</i> executar. Isso envolve alguma
sensibilidade às necessidades das crianças, conforme expressas pelas próprias
crianças. Como pais/mães, desenvolvedores(as), irmãos(ãs) mais velhos(as),
administradores(as) de sistema, precisamos manter nossos ouvidos e olhos
abertos e descobrir o que torna os computadores desejáveis para as crianças.
Sem esse foco, podemos facilmente nos desviar tentando alcançar objetivos
abstratos como "facilidade de uso", "simplicidade", "baixa manutenção" ou
"robustez" que, embora sejam objetivos certamente louváveis para o Debian como
um todo, são muito amplos para atender às necessidades e desejos específicos das
crianças.
</p>

<h4>Aplicativos</h4>

<p>
Naturalmente, as crianças têm necessidades e desejos diferentes dos adultos nos
aplicativos que escolhem executar. Alguns deles serão jogos, enquanto outros
serão processadores de palavras, editores de texto, programas de "pintura" e
similares. O objetivo é identificar os aplicativos da mais alta qualidade
disponíveis no Debian que são adequados para crianças, aumentar essa
quantidade empacotando aqueles que ainda não estão no Debian e garantir que os
aplicativos escolhidos sejam mantidos em um estado bem atualizado. Um objetivo
de implementação é fornecer meta pacotes para facilitar para o(a)
administrador(a) do sistema a instalação de grupos de aplicativos "amigáveis
para crianças". Outro objetivo é melhorar nossos pacotes de maneira que sejam
particularmente importantes para as crianças, o que pode ser tão simples quanto
preencher lacunas na documentação ou pode ser mais complexo, envolvendo o
trabalho com os(as) desenvolvedores(as) originais.
</p>

<h4>Proteção para crianças e gerenciamento de contas</h4>

<p>
A ideia aqui não é necessariamente implementar medidas duras de segurança.
Isso está além da nossa delegação. O objetivo é simplesmente fornecer aos(as)
administradores(as) de sistema documentação e ferramentas para configurar seus
sistemas para que suas crianças usuárias naturalmente curiosas não "quebrem"
suas contas, absorvam todos os recursos do sistema ou façam coisas que exijam
intervenção constante do(a) administrador(a) de sistema. Essa é uma preocupação
mais para crianças usuárias do que para adultos, pois elas tendem a explorar e
deliberadamente levar o sistema ao limite apenas para ver o que acontece. As
bagunças resultantes podem ser ao mesmo tempo divertidas e frustrantes. Esse
objetivo é manter sua sanidade (e senso de humor) como administrador(a) de
sistema de uma criança.
</p>

<h4>Aprendendo a usar o computador</h4>

<p>
A meta de "proteção das crianças" precisa ser equilibrada com a meta de
<i>permitir</i> que as crianças experimentem coisas (e sim, quebrem coisas) e
encontrem soluções para seus problemas. Aprender a usar o teclado, GUI, shell e
linguagens de computador são coisas que pais/mães e filhos(as) precisam de
algumas dicas para ajudá-los a seguir na direção certa.
</p>

<h4>Interface de usuário(as)</h4>

<p>
Descubra e implemente tanto interfaces gráficas quanto baseadas em texto que
funcionem bem e sejam atraentes para as crianças. A ideia não é reinventar a
interface de usuário(a), mas agregar valor às ferramentas e pacotes existentes
(gerenciadores de janelas, sistema de menus e assim por diante) fornecendo
algumas configurações convenientemente pré-selecionadas que acreditamos
funcionar melhor para crianças.
</p>

<h4>Orientação familiar</h4>

<p>
Dê aos(as) pais/mães (e, em alguns casos, irmãos(ãs) mais velhos(as)) as
ferramentas para ajudar seus(suas) filhos(as) (ou irmãos(ãs)) a aprender sobre
computadores e a colocar limites razoáveis em seu acesso, orientando-os(as) para
o uso independente do computador à medida que amadurecem. Por exemplo,
muitos(as) pais/mães estarão preocupados em regular o uso da Internet para
proteger seus(suas) filhos(as) até que atinjam uma idade adequada para lidar com
conteúdo adulto. A coisa importante a lembrar é que os(as) pais/mães vão
escolher o que eles(as) acham que é melhor para seus(suas) filhos(as). O grupo
Debian Jr. não faz esse julgamento, mas está lá para ajudar a fornecer as
ferramentas e documentação para ajudar os(as) pais/mães com essas decisões. Dito
isso, acho que esse objetivo deve se concentrar mais no aspecto "orientador" do
que na restrição, pois o primeiro é uma atividade positiva enquanto o segundo é
negativo.
</p>

<h4>Sistema para crianças</h4>

<p>
Embora nosso primeiro objetivo como administradores(as) de sistema para crianças
seja provavelmente configurar contas para elas em nossos próprios sistemas e
preenchê-las com aplicativos que as crianças gostam, chega um momento em
que contemplamos lhes fornecendo o próprio sistema. A realização mais
ambiciosa desse objetivo pode ser um software Debian equivalente aos
computadores "de brinquedo" atualmente no mercado: sistemas cobertos com
decalques de cores vivas pré-carregados com softwares atraentes para crianças
de uma faixa etária específica. É importante manter em perspectiva que este
ainda seria um sistema Debian, não um fork da distribuição Debian. É um
objetivo perfeitamente alcançável através do sistema de gerenciamento de
pacotes do Debian (via meta pacotes, por exemplo) e não deve exigir um fork no
desenvolvimento para produzir uma "edição especial para crianças" do Debian.
</p>

<h4>Internacionalização</h4>

<p>
Embora o inglês possa ser o idioma "universal", nem todas as crianças o falam
como língua materna. E embora a internacionalização deva ser um objetivo do
próprio Debian, os problemas do idioma são amplificados ao lidar com crianças.
As crianças não vão querer usar o Debian se não houver suporte para seu idioma,
e acharão mais confortável usar outros sistemas operacionais onde o suporte a
idiomas é melhor. Portanto, precisamos ter isso em mente.
</p>
